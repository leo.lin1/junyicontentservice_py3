# -*- coding: utf-8 -*-
import urllib.parse
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import logging
import json
import time

import google.auth

from . import env
DEFAULT_SERVICE_SERVER_URL = env.DEFAULT_SERVICE_SERVER_URL
CONTENT_SERVICE_PRIVATE_KEY_FILE = 'api_client/content_private.pem'
CONTENT_SERVICE_NAME = 'contentfrontend'
DEFAULT_SERVICE_NAME = 'default'

SOFT_MAX_PAYLOAD_B = 16 * 1024 * 1024  # 16MB
HARD_MAX_PAYLOAD_B = 32 * 1024 * 1024  # 32MB


def fetch(path):
    """
    取 default service 提供的資料，目前僅支援 GET method 且 payload 須符合以下條件：
        1. 不得超過 HARD_MAX_PAYLOAD_B
        2. json type
    """
    url = urllib.parse.urljoin(DEFAULT_SERVICE_SERVER_URL, path)

    try:
        with open(CONTENT_SERVICE_PRIVATE_KEY_FILE, 'rb') as fd:
            private_key = fd.read()
    except IOError as e:
        logging.error('Open file [%s] failed, Detail: %r' % (CONTENT_SERVICE_PRIVATE_KEY_FILE, e))
        raise RuntimeError('Open file [%s] failed' % CONTENT_SERVICE_PRIVATE_KEY_FILE) from e
    signer = google.auth.crypt.RSASigner.from_string(private_key)
    now = int(time.time())
    payload = {'iat': now,
               'exp': now + 300,
               'iss': CONTENT_SERVICE_NAME,
               'sub': CONTENT_SERVICE_NAME,
               'aud': DEFAULT_SERVICE_NAME}
    jwt = google.auth.jwt.encode(signer, payload)

    value = 'Bearer '.encode('utf-8') + jwt
    request = urllib.request.Request(url, headers={'Authorization': value})
    response_handle = urllib.request.urlopen(request, timeout=30)
    response_string = response_handle.read(HARD_MAX_PAYLOAD_B)
    if len(response_string) > SOFT_MAX_PAYLOAD_B:
        logging.warning("Response size exceed 16M, which is [%d], please implement paging", len(response_string))
    if response_handle.read(1) != b'':
        # 讀了 HARD_MAX_PAYLOAD_B 之後還有資料
        logging.error("Response size exceed 32M, abort process")
        raise ValueError("Payload too large.")
    return json.loads(response_string)


