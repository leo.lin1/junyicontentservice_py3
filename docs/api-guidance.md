# Google SDK
## datastore
python2 的專案用 google.appengine.ext.datastore 這套 sdk 存取資料庫，

python3 則改用 google.cloud.datastore

兩者在級別上不太一樣，google.cloud.datastore 幾乎只是 datastore api 的 python wrapper; 而原本的 ext.datastore 則提供了更多方便的功能

# Policy
## Transaction
過往讀寫資料庫都不會抓 transaction，這個規格在此專案修正為
**所有寫入都要抓 transaction，讀取需要 strong consistancy 的資料也抓 transaction**，其餘參考業務邏輯
相關的業務邏輯：Topic (資料夾概念）為多版本的資料，只有 edit version 的 Topic 會變動，除此之外的版本
（如 default，也就是目前網站線上的版本）都不會修改，所以 edit version 的 Topic 讀寫都要抓 transaction，
而其他版本不允許寫入，讀取不必抓 transaction。

## API test coverage
* 至少需包含一個全程無 mock 的 happy path
* 若有修改到資料庫，盡量在 teardown 把資料庫回覆 (至少 test case pass 時可以完全回復資料庫)
