# Topic
<a href="vocabulary.md#topic">topic</a> 的 child_keys 紀錄下一層的 <a href="vocabulary.mdcontent">content</a> 清單，parent_keys 反向指回去自己的上層主題，ancestor_keys 紀錄從上層主題到跟節點的主題清單，parent_keys & ancestor_keys 都屬於 cache 性質的 redundant information

# Teaching Material / 教材

## 特徵
### 可以屬於多個主題
除了 iframe 以外的教材都可以同時上架到多個主題。
例如 video_A 原本就存在在 topic_A 底下，然後再從 topic_B 下 "create" 一次 video_A (由 readable_id 判定），後端實際上是把修改同一個 Video entity, 也就是把 video_A 也加到 topic_B 下 （by parent_topic.add_child(video)）

所以是使用者角度都是 create, 對我們來說有分 "新影片 create" 跟 "同一部影片 update topic"

### 可以不屬於任一主題
不屬於任一主題代表無法從均一教育平台的課程樹索引到此教材。除此之外，這個狀況下各種教材的行為有點混亂

### access_control

### live / public / hidden
除了 video 和 iframe 以外都有權限控管的屬性，隱藏之後只有 super user 可以看到

## 相關資料表
### <a name="version-content-change">VersionContentChange</a>
教育組編輯<a href="vocabulary.md#teaching-material">教材</a>時產生的異動，也就是<a href="vocabulary.md#teaching-material">教材</a>在 <a href="vocabulary.md#edit-version">edit version</a> 和 <a href="vocabulary.md#default-version">default version</a> 之間的差異，命名為 VersionTeachingMaterialChange 會更精準


## 相關功能

### 推薦

### 搜尋

### 知識地圖