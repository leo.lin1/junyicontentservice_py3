
### <a name="content">content / 內容</a>
泛指<a href="#teaching-material">教材</a>和 <a href="#topic">topic</a>

### <a name="topic">topic / 主題</a>
container 形態的 <a href="#content">content</a>，類似檔案系統的資料夾，擁有 title, description 等描述，
topic 的內容物可以有 topic, section 或<a href="#teaching-material">教材</a>

### <a name="section">section / 小節</a>
container 形態的 <a href="#content">content</a>，類似檔案系統的資料夾，擁有 title, description 等描述，
不同於 topic, section 的內容物只能有<a href="#teaching-material">教材</a>

### <a name="topic-tree">topic tree</a>
由 <a href="#topic">topic</a> 和 <a href="#section">section</a> 組成的樹狀結構

技術細節：topic tree 有多版本的紀錄，同一個 topic 在不同版本中會維持固定的 id

### <a name="teaching-material">teaching material / 教材</a>
video, exercise, exam, article, diagnostic exam, url, iframe 的集合

### <a name="content-tree">content tree / 知識樹</a>
<a href="#topic-tree">topic tree</a> 加上位於 leaves 的<a href="#teaching-material">教材</a>

技術細節：因為<a href="#teaching-material">教材</a>沒有版本控制，意即沒有多版本的紀錄，所以 content tree 僅有 <a href="#default-version">default version</a> 一個版本。<a href="#edit-version">edit version</a> 中的 content 是 content 和 <a href="#version-content-change">VersionContentChange</a> 合併的結果

### <a name="">exercise / 技能 / 知識點 / 習題 / 練習題</a>
為題目的集合，可以是隨機產題的 html exercise (e.g. junyiacademy/khan-exercises/exercises/10000_inside_money_with_5.html) 或一組 perseus question

技術細節：perseus exercise 和 perseus question 透過 cover range 互相參照，一個 perseus exercise 下的題目總數為此 perseus exercise 的每一個 cover range 所對應到的 perseus question 的集合

### <a name="">video / 影片</a>
對應到一支 youtube 影片．可以添加影片插入習題 (SocratesQuestion) 或與技能關聯 (ExerciseVideo) 

### <a name="topic-version">topic version</a>
topic tree 版本，每個版本有一個對應的版本號碼，一次 <a href="#publish">publish</a> 會產生一個新的版本

技術細節: topic tree 和 topic version 為一對一關係

### <a name="edit-version">edit version</a>
教育組編輯中的 <a href="#topic-version">topic version</a>，也是當前最新的 <a href="#topic-version">topic version</a>

### <a name="default-version">default version</a>
當前均一教育平台 “課程” 參考到的 <a href="#topic-version">topic version</a>

### <a name="publish">publish</a>
將均一教育平台 ”課程“ 的參考改到原 <a href="#edit-version">edit version</a> 並建立新的 <a href="#edit-version">edit version</a> 的動作

### <a name="">教育組後台</a>
https://www.junyiacademy.org/devadmin/content

## 特殊 Topic
### <a name="topic-junyiacademy-exam">不想讓使用者看到的內容</a>
Topic('junyiacademy-exam', '不想讓使用者看到的內容')  subtree 包含的所有 Exercise，搜尋、推薦時會跳過

### <a name="cemetery">墳場</a>
title 通常是 “即將下架” 的 Topic，裡面放 Exercise，是為了不讓使用者已經精熟的技能不見．從 root 到 墳場 Topic 一路都是 live 的 Topic（不然技能總表就拉不到）。e.g. https://www.junyiacademy.org/course-compare/math-grade-2-a/m2-history

