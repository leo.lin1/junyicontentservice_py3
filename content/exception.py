# -*- coding: utf-8 -*-


class ContentNotExistsError(Exception):
    pass

class InvalidParent(Exception):
    pass

class TopicExistsError(Exception):
    # 預期 topic 不存在，例如要建立新的，但實際上已經存在
    pass


class TopicKeyExistsError(Exception):
    # 預期 topic key 不存在，例如要建立新的，但實際上已經存在
    pass


class InvalidFormat(Exception):
    def __init__(self, value, message=None):
        self.value = value
        self.message = message

    def __str__(self):
        return self.message or "Invalid Format [%s]." % self.value


class ExerciseCreateError(Exception):
    pass


class ExerciseUpdateError(Exception):
    pass
