# -*- coding: utf-8 -*-
from .internal import topic, iframe, video, exercise, url, exam, article, diagnostic_exam
import instance_cache

class ContentFactory:
    def __call__(self, _treat_topic_as_divider=False, *args, **kwargs):
        if 'entity' not in kwargs.keys():
            raise ValueError("ContentFactory requires Entities to produce contents")

        if kwargs['entity'].kind == 'Topic':
            if 'is_section' in kwargs['entity'] and kwargs['entity']['is_section']:
                return topic.Section(*args, **kwargs)
            if _treat_topic_as_divider:
                return topic.Divider(*args, **kwargs)
            return topic.Topic(*args, **kwargs)
        if kwargs['entity'].kind == 'Iframe':
            return iframe.Iframe(*args, **kwargs)
        if kwargs['entity'].kind == 'Video':
            return video.Video(*args, **kwargs)
        if kwargs['entity'].kind == 'Exercise':
            return exercise.Exercise(*args, **kwargs)
        if kwargs['entity'].kind == 'Url':
            return url.Url(*args, **kwargs)
        if kwargs['entity'].kind == 'Exam':
            return exam.Exam(*args, **kwargs)
        if kwargs['entity'].kind == 'Article':
            return article.Article(*args, **kwargs)
        if kwargs['entity'].kind == 'DiagnosticExam':
            return diagnostic_exam.DiagnosticExam(*args, **kwargs)
        raise ValueError('Not support kind [%s]' % kwargs['entity'].kind)


@instance_cache.cache(available_seconds=0)
def get_content_factory():
    return ContentFactory()

