# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch

from google.cloud import datastore

from . import topic
from content.internal.internal import InvalidContentTree
from content.internal import mock_entity

class TestTopic(unittest.TestCase):
    math_id = 'math'
    update_id = 'test_update_topic'
    new_id = 'new_topic_id'
    new_title = 'New Topic Title'
    new_desc = 'description of new topic'
    edit_ver_id = 'edit'
    wrong_ver_id = 'x'
    empty = ''

    def test_validate_topic_required_info_by_edu_sheet_ok(self):
        errors = topic.validate_topic_required_info_by_edu_sheet(self.new_id, self.new_title, self.edit_ver_id)
        self.assertEqual(len(errors), 0)

    def test_validate_topic_required_info_by_edu_sheet_fail_empty(self):
        # 代號空白
        errors = topic.validate_topic_required_info_by_edu_sheet(self.empty, self.new_title, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '資料夾的代號不能空白')
        # 標題空白
        errors = topic.validate_topic_required_info_by_edu_sheet(self.new_id, self.empty, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '資料夾的標題不能空白')
        # 錯誤版本號
        errors = topic.validate_topic_required_info_by_edu_sheet(self.new_id, self.new_title, self.wrong_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '不允許編輯此版本 [x]')
        # 兩個錯誤
        errors = topic.validate_topic_required_info_by_edu_sheet(self.empty, self.empty, self.edit_ver_id)
        self.assertEqual(len(errors), 2)
        self.assertEqual(errors[0], '資料夾的代號不能空白')
        self.assertEqual(errors[1], '資料夾的標題不能空白')
        # 三個錯誤
        errors = topic.validate_topic_required_info_by_edu_sheet(self.empty, self.empty, self.wrong_ver_id)
        self.assertEqual(len(errors), 3)
        self.assertEqual(errors[0], '資料夾的代號不能空白')
        self.assertEqual(errors[1], '資料夾的標題不能空白')
        self.assertEqual(errors[2], '不允許編輯此版本 [x]')

    @patch('content.internal.internal.get_edit_version')
    def test_validate_topic_no_edit_version(self, get_edit_version_patch):
        get_edit_version_patch.side_effect = InvalidContentTree
        errors = topic.validate_topic_required_info_by_edu_sheet(self.new_id, self.new_title, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '嚴重錯誤：edit version 不存在')

    def test_validate_topic_required_info_by_edu_sheet_fail_long(self):
        # 500 字代號
        topic_id = 'a' * 500
        title = self.new_title
        errors = topic.validate_topic_required_info_by_edu_sheet(topic_id, title, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        # 501 字代號
        topic_id += 'a'
        errors = topic.validate_topic_required_info_by_edu_sheet(topic_id, title, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '代號不能超過 500 個字, %s' % topic_id)
        # 不合法 id
        errors = topic.validate_topic_required_info_by_edu_sheet("id+", title, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '代號 %s 存在不合法字元' % "id+")
        # 100 字標題
        topic_id = 'topic_id'
        title = 'a' * 100
        errors = topic.validate_topic_required_info_by_edu_sheet(topic_id, title, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        # 101 字標題
        title += 'a'
        errors = topic.validate_topic_required_info_by_edu_sheet(topic_id, title, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], '標題不能超過 100 個字, %s' % title)

    @patch('google.cloud.datastore.Client.put')
    def test_update_topic_ok(self, put_patch):
        # validation only
        errors, updated_topic = topic.update_topic(self.update_id, self.new_title, self.new_desc, True, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        self.assertIsNone(updated_topic)
        self.assertFalse(put_patch.called)
        # update
        errors, updated_topic = topic.update_topic(self.update_id, self.new_title, self.new_desc, False, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        self.assertIsNotNone(updated_topic)
        self.assertTrue(put_patch.called)

    def test_update_topic_fail(self):
        # 不存在 topic id
        errors, updated_topic = topic.update_topic(self.new_id, self.new_title, self.new_desc, False, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertIsNone(updated_topic)

    @patch('google.cloud.datastore.Client.put')
    def test_create_topic_ok(self, put_patch):
        # validation only
        errors, created_topic = topic.create_topic(self.new_id, self.new_title, self.new_desc, self.math_id, True, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        self.assertIsNone(created_topic)
        self.assertFalse(put_patch.called)
        # create
        errors, created_topic = topic.create_topic(self.new_id, self.new_title, self.new_desc, self.math_id, False, self.edit_ver_id)
        self.assertEqual(len(errors), 0)
        self.assertIsNotNone(created_topic)
        self.assertTrue(put_patch.called)

    def test_create_topic_fail(self):
        # 錯誤版本號
        errors, created_topic = topic.create_topic(self.new_id, self.new_title, self.new_desc, self.math_id, False, 'x')
        self.assertEqual(len(errors), 1)
        self.assertIsNone(created_topic)
        # 不存在 parent
        errors, created_topic = topic.create_topic(self.new_id, self.new_title, self.new_desc, 'x', False, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertIsNone(created_topic)
        # 已存在 id
        errors, created_topic = topic.create_topic(self.math_id, self.new_title, self.new_desc, self.math_id, False, self.edit_ver_id)
        self.assertEqual(len(errors), 1)
        self.assertIsNone(created_topic)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic.get_all_topics')
    def test_get_exercise_topics_ok(self, get_all_topics, get_children_patch):
        topic_entity, children, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=2, num_child_each_content=2,
                child_is_live=[True, False])
        get_children_patch.return_value = \
            [child for child in children if child.kind == 'Exercise' and child['live']]
        get_all_topics.return_value = [topic_entity]
        result = topic.get_exercise_topics(350)
        self.assertTrue(get_all_topics.called)
        self.assertTrue(get_children_patch.called)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0]['kind'], 'Topic')
        self.assertEqual(result[0]['id'], topic_entity['id'])
        self.assertEqual(len(result[0]['children']), 1)
        self.assertEqual(result[0]['children'][0]['kind'], 'Exercise')

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic.get_all_topics')
    def test_get_exercise_topics_empty_exercises(self, get_all_topics,
                                                 get_children_patch):
        topic_entity, _, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=2, child_is_live=[True, False])
        get_children_patch.return_value = []
        get_all_topics.return_value = [topic_entity]
        result = topic.get_exercise_topics(350)
        self.assertTrue(get_all_topics.called)
        self.assertTrue(get_children_patch.called)
        self.assertEqual(len(result), 0)
