# -*- coding: utf-8 -*-
import content.exception
import instance_cache
from .factory import get_content_factory
from .internal import internal
from .internal import topic


@instance_cache.cache_with_key_fxn(lambda topic_id, include_hidden=False, version_id=None, current_user=None:
                                   "get_topic_page_%s_%r" %
                                   (topic_id, include_hidden),
                                   available_seconds=0, category=internal.get_default_tree_category())
def get_topic_page(topic_id, include_hidden=False, version_id=None, current_user=None):
    leading_topic = topic.Topic.from_id(topic_id=topic_id, version_id=version_id, current_user=current_user)

    visible_content_selector = lambda c: include_hidden or c.is_live
    if not leading_topic or not visible_content_selector(leading_topic):
        raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)

    leading_topic.content_factory = get_content_factory()
    return leading_topic.info_to_topic_page(leading_topic=True, visible_content_selector=visible_content_selector)


@instance_cache.cache_with_key_fxn(lambda version_id: "get_topic_tree_data_v%s" % version_id,
                                   available_seconds=0)
def get_topic_tree_data(version_id):
    root_topic = topic.get_root(version_id)
    return root_topic.subtree()


def validate_topic_required_info_by_edu_sheet(topic_id, title, version_id):
    errors = []

    try:
        topic.validate_id_format(topic_id)
    except content.exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        topic.validate_title(title)
    except content.exception.InvalidFormat as e:
        errors.append(str(e))

    if version_id != 'edit':
        errors.append(u"不允許編輯此版本 [%s]" % version_id)

    try:
        internal.get_edit_version()
    except internal.InvalidContentTree:
        errors.append(u"嚴重錯誤：edit version 不存在")

    return errors


def update_topic(topic_id, title, description, validation_only, version_id):
    errors = validate_topic_required_info_by_edu_sheet(topic_id, title, version_id)

    try:
        updated_topic = topic.update_topic(validation_only=validation_only,
                                           topic_id=topic_id,
                                           title=title,
                                           standalone_title=title,
                                           description=description)
    except content.exception.ContentNotExistsError as e:
        errors.append(str(e))
        updated_topic = None

    return errors, updated_topic


def create_topic(topic_id, title, description, parent_id, validation_only, version_id):
    errors = validate_topic_required_info_by_edu_sheet(topic_id, title, version_id)
    if errors or validation_only:
        return errors, None

    """
    將 topic 和 parent_topic 的驗證搬到 validation_only 之後，
    避免教育組正常的上架流程被「檢查變動」擋住。
    e.g. 想要批次建立以下的樹
         A      一開始 A, B, C 都不存在，如果在 validation_only 就檢查，
         |-B    會發現 B 的 parent A 不存在而失敗。因此改到真的要建立時才
           |-C  檢查，就會先建出 A，再來建立 B 的時候 A 就存在了。
    """
    try:
        created_topic = topic.create_topic(title=title,
                                           parent_id=parent_id,
                                           topic_id=topic_id,
                                           standalone_title=title,
                                           description=description)
    except (content.exception.TopicExistsError,
            content.exception.ContentNotExistsError,
            content.exception.InvalidParent) as e:
        errors.append(str(e))
        created_topic = None

    return errors, created_topic


def get_exercise_topics(version_id):
    version = internal.get_version(version_id)
    topics = topic.get_filled_content_topics(types=['Exercise'],
                                             version=version)
    # Topics in ignored_topics will not show up on the knowledge map,
    # have topic exercise badges created for them, etc.
    ignored_topics = [
        "New and Noteworthy",
    ]

    # Filter out New and Noteworthy special-case topic. It might
    # have exercises, but we don't want it to own a badge.
    topics = [t for t in topics if t.title not in ignored_topics and len(t.children) > 0]

    # Remove non-live exercises
    for t in topics:
        t.children = [exercise for exercise in t.children if exercise['live']]

    # Transform TopicVersion to JSON
    version_json = {
        'entity_key_id': version.id,
        'kind': version.kind,
        'number': version['number'],
        'default': version['default'],
        'edit': version['edit'],
    }

    # Filter down to only topics that have live exercises
    return [t.topic_to_json(version_json) for t in topics if len(t.children) > 0]
