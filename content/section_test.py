# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch

from . import section
from .internal import topic, mock_entity, internal

class TestSection(unittest.TestCase):
    @patch('content.internal.topic.Section.from_id')
    def test_read_section_from_default_version(self, from_id_patch):
        section_entity, _ = mock_entity.get_section_entity(0)
        mock_section = topic.Section(entity=section_entity)
        mock_section.version = internal.get_default_version()
        from_id_patch.return_value = mock_section

        is_published, _ = section.read_section(section_id='xxx', version_id='default')
        self.assertTrue(is_published)
        is_published, _ = section.read_section(section_id='xxx', version_id='300')
        self.assertTrue(is_published)

    @patch('content.internal.topic.Section.from_id')
    def test_read_section_from_edit_version(self, from_id_patch):
        section_entity, _ = mock_entity.get_section_entity(0)
        mock_section = topic.Section(entity=section_entity)
        mock_section.version = internal.get_edit_version()
        from_id_patch.return_value = mock_section

        is_published, _ = section.read_section(section_id='xxx', version_id='edit')
        self.assertFalse(is_published)
        is_published, _ = section.read_section(section_id='xxx', version_id='301')
        self.assertFalse(is_published)
