# -*- coding: utf-8 -*-
from content import exception
from content.internal import exercise, internal


def validate_exercise_required_info_by_edu_sheet(exercise_name, content_title):
    errors = []

    try:
        exercise.validate_name_format(exercise_name)
    except exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        exercise.validate_title_format(content_title)
    except exception.InvalidFormat as e:
        errors.append(str(e))

    try:
        internal.get_edit_version()
    except internal.InvalidContentTree:
        errors.append("嚴重錯誤：edit version 不存在")

    return errors


def create_exercise(exercise_name, content_title, description, parent_id, validation_only):
    errors = validate_exercise_required_info_by_edu_sheet(exercise_name, content_title)
    if errors or validation_only:
        return errors

    try:
        exercise.create_exercise(exercise_name, content_title, description, parent_id)
    except exception.ExerciseCreateError as e:
        errors.append(str(e))

    return errors


def update_exercise(exercise_name, content_title, description, validation_only):
    errors = validate_exercise_required_info_by_edu_sheet(exercise_name, content_title)
    if errors or validation_only:
        return errors

    try:
        exercise.update_exercise(exercise_name, content_title, description)
    except exception.ExerciseUpdateError as e:
        errors.append(str(e))

    return errors
