# -*- coding: utf-8 -*-
import copy
import datetime
import json
import pickle
import random
import re
import unittest
from unittest.mock import patch

from content import exception
from content.internal import exercise
from content.internal import mock_entity


class TestExercise(unittest.TestCase):
    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.exercise.get_content_change')
    @patch('content.internal.exercise.get_by_name')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.topic.get_root_key')
    @patch('content.internal.internal.get_edit_version')
    def test_create_exercise(self, get_version_patch, _, get_topic_patch, get_exercise_patch, get_change_patch, put_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])
        orig_desc = pickle.loads(change['content_changes'])['description']
        parent_topic, _, _ = mock_entity.get_topic_entity()
        get_version_patch.return_value = version

        # Test parent topic not exist.
        get_topic_patch.return_value = None
        get_exercise_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^預計擺放的母資料夾 \[%s\] 不存在$" % re.escape(parent_topic['id']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        put_patch.assert_not_called()

        # Test exercise exists in the parent topic.
        parent_topic['child_keys'].append(content.key)
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^母資料夾中，已存在相同標題的知識點 \[%s\]$" % re.escape(content['pretty_display_name']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        put_patch.assert_not_called()
        parent_topic['child_keys'].pop()

        # Test exercise exists with different title.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^此知識點已存在，且已存在的知識點標題 \[%s\] 和預計上架的標題不一樣$" % re.escape(content['pretty_display_name']),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'] + 'x', content['description'],
                parent_topic['id'])
        put_patch.assert_not_called()

        # Test exercise exists with different cover range.
        orig_cover_range = json.loads(content['cover_range'])
        content['cover_range'] = json.dumps(orig_cover_range + [['test_ex_name']])
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        self.assertRaisesRegex(
                exception.ExerciseCreateError,
                r"^此知識點已存在，且已存在的知識點 cover range \[%s\] 和預計上架的 cover range 不一樣$" % re.escape(str(content['cover_range'])),
                exercise.create_exercise,
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        put_patch.assert_not_called()
        content['cover_range'] = json.dumps(orig_cover_range)

        # Test exercise not exist.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        # Should put a new Exercise and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'Exercise', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'Exercise':
                self.assertEqual(entity.key.flat_path, ('Exercise',))
                required_props = {
                        'cover_range': json.dumps([[content['name']]]),
                        'covers': [],
                        'description': content['description'],
                        'h_position': 0,
                        'is_quiz_exercise': True,
                        'live': True,
                        'name': content['name'],
                        'prerequisites': [],
                        'pretty_display_name': content['pretty_display_name'],
                        'seconds_per_fast_problem': 0.0,
                        'short_display_name': content['pretty_display_name'][:11],
                        'tags': [],
                        'v_position': 0,
                }
                self.assertEqual({k: entity[k] for k in required_props}, required_props)
            elif entity.key.kind == 'Topic':
                exercise_key = entity['child_keys'].pop()
                self.assertEqual(entity, parent_topic)
                # The appended key is not complete since Client.put is mocked.
                self.assertEqual(exercise_key.flat_path, ('Exercise',))
        put_patch.reset_mock()

        # Test exercise not exist and parent topic doesn't have 'child_keys'
        # property.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        del get_topic_patch.return_value['child_keys']
        get_exercise_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        # Should put a new Exercise and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'Exercise', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'Exercise':
                self.assertEqual(entity.key.flat_path, ('Exercise',))
                required_props = {
                        'cover_range': json.dumps([[content['name']]]),
                        'covers': [],
                        'description': content['description'],
                        'h_position': 0,
                        'is_quiz_exercise': True,
                        'live': True,
                        'name': content['name'],
                        'prerequisites': [],
                        'pretty_display_name': content['pretty_display_name'],
                        'seconds_per_fast_problem': 0.0,
                        'short_display_name': content['pretty_display_name'][:11],
                        'tags': [],
                        'v_position': 0,
                }
                self.assertEqual({k: entity[k] for k in required_props}, required_props)
            elif entity.key.kind == 'Topic':
                exercise_key = entity['child_keys'].pop()
                self.assertEqual(entity, parent_topic)
                # The appended key is not complete since Client.put is mocked.
                self.assertEqual(exercise_key.flat_path, ('Exercise',))
        put_patch.reset_mock()

        # Test exercise exists.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        # Should put a changed Topic.
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'].append(content.key)
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise exists and parent topic doesn't have 'child_keys'
        # property.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        del get_topic_patch.return_value['child_keys']
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], content['description'],
                parent_topic['id'])
        # Should put a changed Topic.
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'] = [content.key]
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'] = []
        put_patch.reset_mock()

        # Test exercise/change exists.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], orig_desc,
                parent_topic['id'])
        # Should put a changed Topic.
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        parent_topic['child_keys'].append(content.key)
        self.assertEqual(entity, parent_topic)
        parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise exists and put a change.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        new_desc = orig_desc + ' (new)'
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], new_desc,
                parent_topic['id'])
        # Should put a new VersionContentChange and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'VersionContentChange', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'VersionContentChange':
                self.assertEqual(entity.key.flat_path, version.key.flat_path + ('VersionContentChange',))
                self.assertEqual(entity['version'], version.key)
                self.assertEqual(entity['content'], content.key)
                self.assertEqual(entity['content_changes'], pickle.dumps({'description': new_desc}, protocol=2))
                self.assertEqual(entity['last_edited_by'], None)
                self.assertEqual(type(entity['updated_on']), datetime.datetime)
            elif entity.key.kind == 'Topic':
                parent_topic['child_keys'].append(content.key)
                self.assertEqual(entity, parent_topic)
                parent_topic['child_keys'].pop()
        put_patch.reset_mock()

        # Test exercise/change exists and put a change.
        get_topic_patch.return_value = copy.deepcopy(parent_topic)
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        new_desc = orig_desc + ' (new)'
        exercise.create_exercise(
                content['name'], content['pretty_display_name'], new_desc,
                parent_topic['id'])
        # Should put a changed VersionContentChange and a changed Topic.
        self.assertEqual(put_patch.call_count, 2)
        entities = [e for ((e,), _) in put_patch.call_args_list]
        self.assertEqual({e.key.kind for e in entities}, {'VersionContentChange', 'Topic'})
        for entity in entities:
            if entity.key.kind == 'VersionContentChange':
                self.assertEqual(entity.key, change.key)
                self.assertEqual(entity['version'], version.key)
                self.assertEqual(entity['content'], content.key)
                self.assertEqual(entity['content_changes'], pickle.dumps({'description': new_desc}, protocol=2))
                self.assertEqual(entity['last_edited_by'], None)
                self.assertEqual(type(entity['updated_on']), datetime.datetime)
            elif entity.key.kind == 'Topic':
                parent_topic['child_keys'].append(content.key)
                self.assertEqual(entity, parent_topic)
                parent_topic['child_keys'].pop()
        put_patch.reset_mock()

    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.exercise.get_content_change')
    @patch('content.internal.exercise.get_by_name')
    @patch('content.internal.internal.get_edit_version')
    def test_update_exercise(self, get_version_patch, get_exercise_patch, get_change_patch, put_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])
        orig_desc = pickle.loads(change['content_changes'])['description']
        get_version_patch.return_value = version

        # Test exercise not found.
        get_exercise_patch.return_value = None
        self.assertRaisesRegex(
                exception.ExerciseUpdateError, r"^這個代號的知識點不存在$",
                exercise.update_exercise, content['name'], 'test_title', 'test_desc')
        put_patch.assert_not_called()

        # Test dummy update with existing exercise and no change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], content['description'])
        put_patch.assert_not_called()

        # Test dummy update with existing exercise and existing change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], orig_desc)
        put_patch.assert_not_called()

        # Test update with existing exercise and no change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = None
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], orig_desc)
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        self.assertEqual(entity.key.flat_path, version.key.flat_path + ('VersionContentChange',))
        self.assertEqual(entity['version'], version.key)
        self.assertEqual(entity['content'], content.key)
        self.assertEqual(entity['content_changes'], pickle.dumps({'description': orig_desc}, protocol=2))
        self.assertEqual(entity['last_edited_by'], None)
        self.assertEqual(type(entity['updated_on']), datetime.datetime)
        put_patch.reset_mock()

        # Test update with existing exercise and existing change.
        get_exercise_patch.return_value = copy.deepcopy(content)
        get_change_patch.return_value = copy.deepcopy(change)
        new_desc = orig_desc + ' (new)'
        exercise.update_exercise(
                content['name'], content['pretty_display_name'], new_desc)
        put_patch.assert_called_once()
        (entity,) = put_patch.call_args[0]
        self.assertEqual(entity.key, change.key)
        self.assertEqual(entity['version'], version.key)
        self.assertEqual(entity['content'], content.key)
        self.assertEqual(entity['content_changes'], pickle.dumps({'description': new_desc}, protocol=2))
        self.assertEqual(entity['last_edited_by'], None)
        self.assertEqual(type(entity['updated_on']), datetime.datetime)


class TestExerciseUtil(unittest.TestCase):
    @patch('google.cloud.datastore.Query.fetch')
    def test_get_by_name(self, fetch_patch):
        # Test not found.
        fetch_patch.return_value = []
        ret = exercise.get_by_name('test_name')
        self.assertIsNone(ret)
        fetch_patch.assert_called_once()
        fetch_patch.reset_mock()

        # Test found.
        entity = mock_entity.get_exercise_entity()
        fetch_patch.return_value = [entity]
        ret = exercise.get_by_name(entity['name'])
        self.assertIs(ret, entity)
        fetch_patch.assert_called_once()

    @patch('google.cloud.datastore.Client.put')
    def test_add_new_exercise(self, put_patch):
        data = dict(mock_entity.get_exercise_entity())
        ret = exercise.add_new_exercise(data)
        put_patch.assert_called_once()
        self.assertEqual(put_patch.call_args[0], (ret,))
        self.assertEqual(ret.key.flat_path, ('Exercise',))
        self.assertEqual({k: ret[k] for k in data}, data)

    @patch('google.cloud.datastore.Query.fetch')
    def test_get_content_change(self, fetch_patch):
        version = mock_entity.get_topic_version_entity()
        content = mock_entity.get_exercise_entity()
        change = mock_entity.get_content_change_entity(version.key, content.key, ['description'])

        # Test not found.
        fetch_patch.return_value = []
        ret = exercise.get_content_change(content, version)
        self.assertIsNone(ret)
        fetch_patch.assert_called_once()
        fetch_patch.reset_mock()

        # Test found.
        fetch_patch.return_value = [change]
        ret = exercise.get_content_change(content, version)
        self.assertIs(ret, change)
        fetch_patch.assert_called_once()

    @patch('google.cloud.datastore.Client.put')
    @patch('google.cloud.datastore.Query.fetch')
    def test_add_content_change(self, fetch_patch, put_patch):
        # No existing VersionContentChange will be fetched.
        fetch_patch.return_value = []

        content = mock_entity.get_exercise_entity()
        version = mock_entity.get_topic_version_entity()

        # Test invalid change.
        data = {'description': None}
        exercise.add_content_change(content, version, data)
        put_patch.assert_not_called()

        # Test dummy change.
        data = {'description': content['description']}
        exercise.add_content_change(content, version, data)
        put_patch.assert_not_called()

        # Test add a change.
        data = {'description': content['description'] + 'x'}
        exercise.add_content_change(content, version, data)
        put_patch.assert_called_once()
        (change,) = put_patch.call_args[0]
        self.assertEqual(change.key.flat_path, version.key.flat_path + ('VersionContentChange',))
        self.assertEqual(change['version'], version.key)
        self.assertEqual(change['content'], content.key)
        self.assertEqual(change['content_changes'], pickle.dumps(data, protocol=2))
        self.assertEqual(change['last_edited_by'], None)
        self.assertEqual(type(change['updated_on']), datetime.datetime)

    def test_validate_name_format(self):
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能空白$",
                exercise.validate_name_format, None)

        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能空白$",
                exercise.validate_name_format, '')

        # Expect not raises.
        name = ''.join(random.choices('abc123', k=500))
        exercise.validate_name_format(name)

        name = ''.join(random.choices('abc123', k=501))
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號不能超過 500 個字, %s$" % re.escape(name),
                exercise.validate_name_format, name)

        name = 'test_name_with_' + random.choice('!@#$%^') + '_as_an_invalid_char'
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^代號 %s 存在不合法字元$" % re.escape(name),
                exercise.validate_name_format, name)

    def test_validate_title_format(self):
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能空白$",
                exercise.validate_title_format, None)

        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能空白$",
                exercise.validate_title_format, '')

        # Expect not raises.
        title = ''.join(random.choices('abc123', k=100))
        exercise.validate_title_format(title)

        title = ''.join(random.choices('abc123', k=101))
        self.assertRaisesRegex(
                exception.InvalidFormat, r"^標題不能超過 100 個字, %s$" % re.escape(title),
                exercise.validate_title_format, title)
