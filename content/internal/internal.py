# -*- coding: utf-8 -*-
from concurrent import futures
import math
import os
import threading

import instance_cache
import content.exception

from google.cloud import datastore


READ_CHUNK_SIZE = 1000


class InvalidContentTree(BaseException):
    pass


@instance_cache.cache(available_seconds=0)
def get_client():
    datastore_client = datastore.Client()
    return datastore_client


def get_query(kind, ancestor=None):
    # in local server, ignore ancestor query if out of a txn
    # FIXME: this is a workaround for a bug in local datastore emulator
    # https://issuetracker.google.com/issues/151198233
    datastore_client = get_client()
    if datastore_client.current_transaction or os.getenv('GAE_VERSION'):
        return datastore_client.query(kind=kind, ancestor=ancestor)
    else:
        return datastore_client.query(kind=kind)


def datastore_ordered_get_multi(keys: [datastore.Key]):
    def order(entity):
        return keys.index(entity.key)

    client = get_client()
    entities = client.get_multi(keys)
    entities.sort(key=order)
    return entities


def _chunks(l, n=READ_CHUNK_SIZE):
    for i in range(0, len(l), n):
        yield l[i:i + n]


def datastore_chunked_get_multi(keys: [datastore.Key]):
    client = get_client()
    n_thread = min(8, math.ceil(len(keys) / READ_CHUNK_SIZE))
    results = []
    with futures.ThreadPoolExecutor(max_workers=n_thread) as executor:
      for vals in executor.map(client.get_multi,
                               _chunks(keys, READ_CHUNK_SIZE)):
          results += vals
    return results


def datastore_unordered_get_multi(keys: [datastore.Key]):
    client = get_client()
    if len(keys) <= READ_CHUNK_SIZE:
        return client.get_multi(keys)

    # A single read call can only handle 1000 requests
    return datastore_chunked_get_multi(keys)


def get_setting(key, entity_group='default_settings'):
    client = get_client()
    db_key = client.key('Settings', entity_group, 'Setting', key)
    return client.get(db_key)['value']


def get_topic_version_query():
    return get_client().query(kind='TopicVersion')


_topic_version_lock = threading.RLock()
_default_topic_version_number = None
_default_topic_version = None


@instance_cache.cache(available_seconds=60)
def get_default_version():
    global _default_topic_version_number
    global _default_topic_version
    version_number = int(get_setting('topic_tree_version'))
    with _topic_version_lock:
        if version_number == _default_topic_version_number and _default_topic_version:
            return _default_topic_version

        query = get_topic_version_query()
        query.add_filter('number', '=', version_number)
        fetched = list(query.fetch())
        if len(fetched) == 0:
            raise InvalidContentTree("Default version not exists.")

        _default_topic_version_number = version_number
        _default_topic_version = fetched[0]

    # 強制釋放前一版本的 cache. TODO: size 很大，應改用 service cache 取代
    instance_cache.flush(category=get_default_tree_category(version_number - 1))

    return fetched[0]


@instance_cache.cache(available_seconds=60)
def get_edit_version():
    query = get_topic_version_query()
    query.add_filter('edit', '=', True)
    fetched = list(query.fetch())
    if len(fetched) == 0:
        raise InvalidContentTree("Edit version not exists.")
    return fetched[0]


def get_version(version_id=None):
    if version_id is None or version_id == "default":
        return get_default_version()
    if version_id == "edit":
        return get_edit_version()
    if str(version_id).isdigit():
        number = int(version_id)
        query = get_topic_version_query()
        query.add_filter('number', '=', number)
        fetched = list(query.fetch())
        return fetched[0]
    raise content.exception.InvalidFormat(version_id, 'Invalid version_id. [%s]' % version_id)

def get_default_tree_category(version_number=None):
    if not version_number:
        version_number = get_default_version()['number']
    return 'default_tree_%d' % version_number
