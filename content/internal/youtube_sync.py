import logging
import re
import json
import urllib.error
import urllib.request

from . import video

# owned by support@junyiacademy.org
API_KEY = "AIzaSyDn_nFGJlf0mNuXQ9tys4Zu3_Jy6xGZyiw"

def youtube_get_video_data_dict(youtube_id):
    video_info = get_video_info_by_youtube_api(youtube_id)
    if video_info is None:
        return None

    video_data = {"youtube_id" : youtube_id,
                  "title"      : video_info["snippet"]["title"],
                  "url"        : "https://www.youtube.com/watch?v=" + youtube_id,
                  "duration"   : convert_iso8601_to_seconds(video_info["contentDetails"]["duration"])}

    if video_info.get("statistics", False):
        video_data["views"] = int(video_info["statistics"]["viewCount"])

    video_data["description"] = video_info["snippet"]["description"]
    if video_info["snippet"].get("tags", False):
        video_data["keywords"] = u", ".join(video_info["snippet"]["tags"])
    else:
        video_data["keywords"] = u""

    potential_id = re.sub('[^a-z0-9]', '-', video_data["title"].lower())
    potential_id = re.sub('-+$', '', potential_id)  # remove any trailing dashes (see issue 1140)
    potential_id = re.sub('^-+', '', potential_id)  # remove any leading dashes (see issue 1526)

    number_to_add = 0
    current_id = youtube_id #EH: force to use youtube id

    while True:
        videos = video.get_for_readable_id(current_id)
        if videos is None: #id is unique so use it and break out
            video_data["readable_id"] = current_id
            break
        else: # id is not unique so will have to go through loop again
            number_to_add+=1
            current_id = potential_id + '-' + str(number_to_add)

    return video_data


# ref: http://stackoverflow.com/questions/16742381/how-to-convert-youtube-api-duration-to-seconds
def convert_iso8601_to_seconds(duration):
    """
    duration - ISO 8601 time format
    examples :
        'P1W2DT6H21M32S' - 1 week, 2 days, 6 hours, 21 mins, 32 secs,
        'PT7M15S' - 7 mins, 15 secs
    """
    split   = duration.split('T')
    period  = split[0]
    time    = split[1]
    timeD   = {}

    # days & weeks
    if len(period) > 1:
        timeD['days']  = int(period[-2:-1])
    if len(period) > 3:
        timeD['weeks'] = int(period[:-3].replace('P', ''))

    # hours, minutes & seconds
    if len(time.split('H')) > 1:
        timeD['hours'] = int(time.split('H')[0])
        time = time.split('H')[1]
    if len(time.split('M')) > 1:
        timeD['minutes'] = int(time.split('M')[0])
        time = time.split('M')[1]    
    if len(time.split('S')) > 1:
        timeD['seconds'] = int(time.split('S')[0])

    # convert to seconds
    timeS = timeD.get('weeks', 0)   * (7*24*60*60) + \
            timeD.get('days', 0)    * (24*60*60) + \
            timeD.get('hours', 0)   * (60*60) + \
            timeD.get('minutes', 0) * (60) + \
            timeD.get('seconds', 0)

    return timeS


def get_video_info_by_youtube_api(youtube_id):
    # API doc, see https://developers.google.com/youtube/v3/docs/videos/list
    url = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails,snippet,statistics,status&id=%s&key=%s"\
          % (youtube_id, API_KEY)

    try:
        resp = urllib.request.urlopen(url)
    except urllib.error.HTTPError as e:
        if e.code != 404:
            logging.error("Youtube API error: url [%s] status_code [%d], error [%s]",
                          url, e.code, str(e))
        return None

    result = json.loads(resp.read())
    return result["items"][0] if result["pageInfo"]["totalResults"] > 0 else None
