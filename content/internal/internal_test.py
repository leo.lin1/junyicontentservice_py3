# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch

from . import internal
import content.exception

class TestGetById(unittest.TestCase):
    def test_get_default_tree_category(self):
        default_tree_category = internal.get_default_tree_category()
        self.assertIsNotNone(default_tree_category)
        self.assertRegex(default_tree_category, '^default_tree_')

    def test_default_arg(self):
        version = internal.get_version()
        self.assertIsNotNone(version)
        self.assertTrue(version['default'])

    @patch('instance_cache.set_cache')
    @patch('instance_cache.get_cache')
    def test_default_arg_cached(self, get_cache_patch, set_cache_patch):
        set_cache_patch.return_value = None
        get_cache_patch.return_value = None
        version = internal.get_version()
        self.assertIsNotNone(version)
        self.assertTrue(version['default'])

    @patch('instance_cache.set_cache')
    @patch('instance_cache.get_cache')
    @patch('content.internal.internal.get_setting')
    def test_default_arg_fail(self, get_setting_patch, get_cache_patch, set_cache_patch):
        """
        模擬 setting 中 topic_tree_version 的值錯誤
        """
        set_cache_patch.return_value = None
        get_cache_patch.return_value = None
        get_setting_patch.return_value = -123
        self.assertRaises(internal.InvalidContentTree, internal.get_version)
        self.assertTrue(get_setting_patch.called)

    def test_default_version(self):
        version = internal.get_version(version_id='default')
        self.assertIsNotNone(version)
        self.assertTrue(version['default'])

    def test_edit_version(self):
        version = internal.get_version(version_id='edit')
        self.assertIsNotNone(version)
        self.assertTrue(version['edit'])

    def test_invalid_arg(self):
        self.assertRaisesRegex(content.exception.InvalidFormat, 'Invalid version_id',
                               internal.get_version, version_id='DEFAULT')

    def test_digit(self):
        version = internal.get_version(version_id=1)
        self.assertEqual(version['number'], 1)
