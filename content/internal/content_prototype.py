# -*- coding: utf-8 -*-


class Content:
    _property_from_entity = []
    _entity = None
    _parent_topic = None

    @property
    def is_live(self):
        raise NotImplementedError

    @property
    def is_teaching_material(self):
        raise NotImplementedError

    def __init__(self, entity, parent_topic=None):
        self._entity = entity
        for prop in self._property_from_entity:
            setattr(self, prop, self._entity.get(prop))
        self._parent_topic = parent_topic

    def info_to_topic_page(self):
        raise NotImplementedError
