#!/usr/bin/python
# -*- coding: utf-8 -*-

class CoolEnglishBasicLearning:
    ContentRightsObject = {
        'author_link': 'https://www.coolenglish.edu.tw/',
        'author_name': '酷英 Cool English',
        'content_link': 'https://www.coolenglish.edu.tw/course/view.php?id=109',
        'content_name': '酷英基本學習',
        'image_link': 'https://www.coolenglish.edu.tw/',
        'image_url': 'https://www.coolenglish.edu.tw/pluginfile.php/1/core_admin/logocompact/0x70/1574721714/logo_s.png',
        'rights_type': 'JY'
    }

    TopicData = {
        'ce-bl-grade-3': {
            'title': '三年級',
            'items': [
                'ce-bl-g3-le-u1c', 'ce-bl-g3-le-u1q',
                'ce-bl-g3-le-u2c', 'ce-bl-g3-le-u2q',
                'ce-bl-g3-le-u3c', 'ce-bl-g3-le-u3q',
                'ce-bl-g3-le-u4c', 'ce-bl-g3-le-u4q',
                'ce-bl-g3-le-u5c', 'ce-bl-g3-le-u5q',
                'ce-bl-g3-le-u6c', 'ce-bl-g3-le-u6q',
                'ce-bl-g3-exp-u1c', 'ce-bl-g3-exp-u1q',
                'ce-bl-g3-exp-u2c', 'ce-bl-g3-exp-u2q',
            ],
            'mapping': {
                'ce-bl-g3-le-u1c': {'title': '1. 學習字母 : Aa-Ff', 'url': 'grade-3/le/u1c/res/index.html'},
                'ce-bl-g3-le-u1q': {'title': 'Quiz 1: 學習字母 Aa-Ff', 'url': 'grade-3/le/u1q/res/index.html'},
                'ce-bl-g3-le-u2c': {'title': '2. 學習字母: Gg-Kk', 'url': 'grade-3/le/u2c/res/index.html'},
                'ce-bl-g3-le-u2q': {'title': 'Quiz 2: 學習字母 Gg-Kk', 'url': 'grade-3/le/u2q/res/index.html'},
                'ce-bl-g3-le-u3c': {'title': '3. 學習字母: Ll-Pp', 'url': 'grade-3/le/u3c/res/index.html'},
                'ce-bl-g3-le-u3q': {'title': 'Quiz 3: 學習字母: Ll-Pp', 'url': 'grade-3/le/u3q/res/index.html'},
                'ce-bl-g3-le-u4c': {'title': '4. 學習字母: Qq-Tt', 'url': 'grade-3/le/u4c/res/index.html'},
                'ce-bl-g3-le-u4q': {'title': 'Quiz 4: 學習字母 Qq-Tt', 'url': 'grade-3/le/u4q/res/index.html'},
                'ce-bl-g3-le-u5c': {'title': '5. 學習字母: Uu-Zz', 'url': 'grade-3/le/u5c/res/index.html'},
                'ce-bl-g3-le-u5q': {'title': 'Quiz 5: 學習字母 Uu-Zz', 'url': 'grade-3/le/u5q/res/index.html'},
                'ce-bl-g3-le-u6c': {'title': '6. 學習字母: Aa-Zz', 'url': 'grade-3/le/u6c/res/index.html'},
                'ce-bl-g3-le-u6q': {'title': 'Quiz 6: 學習字母 Aa-Zz', 'url': 'grade-3/le/u6q/res/index.html'},
                'ce-bl-g3-exp-u1c': {'title': '7. 常用語 (一)', 'url': 'grade-3/exp/u1c/res/index.html'},
                'ce-bl-g3-exp-u1q': {'title': 'Quiz 7: 常用語(一)', 'url': 'grade-3/exp/u1q/res/index.html'},
                'ce-bl-g3-exp-u2c': {'title': '8. 常用語 (二)', 'url': 'grade-3/exp/u2c/res/index.html'},
                'ce-bl-g3-exp-u2q': {'title': 'Quiz 8 : 常用語 (二)', 'url': 'grade-3/exp/u2q/res/index.html'},
            },
        },
        'ce-bl-grade-4': {
            'title': '四年級',
            'items': [
                'ce-bl-g4-ws-u1c', 'ce-bl-g4-ws-u1q',
                'ce-bl-g4-ws-u2c', 'ce-bl-g4-ws-u2q',
                'ce-bl-g4-ws-u3c', 'ce-bl-g4-ws-u3q',
                'ce-bl-g4-ws-u4c', 'ce-bl-g4-ws-u4q',
                'ce-bl-g4-ws-u5c', 'ce-bl-g4-ws-u5q',
                'ce-bl-g4-ws-u6c', 'ce-bl-g4-ws-u6q',
                'ce-bl-g4-ph-u1c', 'ce-bl-g4-ph-u1q',
                'ce-bl-g4-ph-u2c', 'ce-bl-g4-ph-u2q',
                'ce-bl-g4-ph-u3c', 'ce-bl-g4-ph-u3q',
                'ce-bl-g4-ph-u4c', 'ce-bl-g4-ph-u4q',
                'ce-bl-g4-ph-u5c', 'ce-bl-g4-ph-u5q',
                'ce-bl-g4-exp-u1c', 'ce-bl-g4-exp-u1q',
                'ce-bl-g4-exp-u2c', 'ce-bl-g4-exp-u2q',
            ],
            'mapping': {
                'ce-bl-g4-ws-u1c': {'title': 'Words and Structures: 1. What\'s Your Name?', 'url': 'grade-4/ws/u1c/G4U1.html'},
                'ce-bl-g4-ws-u1q': {'title': 'Quiz 1: What\'s Your Name?', 'url': 'grade-4/ws/u1q/res/index.html'},
                'ce-bl-g4-ws-u2c': {'title': 'Words and Structures: 2. What\'s This?', 'url': 'grade-4/ws/u2c/G4U2.html'},
                'ce-bl-g4-ws-u2q': {'title': 'Quiz 2: What\'s This?', 'url': 'grade-4/ws/u2q/res/index.html'},
                'ce-bl-g4-ws-u3c': {'title': 'Words and Structures: 3. What\'s That?', 'url': 'grade-4/ws/u3c/G4U3.html'},
                'ce-bl-g4-ws-u3q': {'title': 'Quiz 3: What\'s That?', 'url': 'grade-4/ws/u3q/res/index.html'},
                'ce-bl-g4-ws-u4c': {'title': 'Words and Structures: 4. What Color Is It?', 'url': 'grade-4/ws/u4c/G4U4.html'},
                'ce-bl-g4-ws-u4q': {'title': 'Quiz 4: What Color Is It?', 'url': 'grade-4/ws/u4q/res/index.html'},
                'ce-bl-g4-ws-u5c': {'title': 'Words and Structures: 5. Is That A Pen?', 'url': 'grade-4/ws/u5c/G4U5.html'},
                'ce-bl-g4-ws-u5q': {'title': 'Quiz 5: Is That A Pen?', 'url': 'grade-4/ws/u5q/res/index.html'},
                'ce-bl-g4-ws-u6c': {'title': 'Words and Structures: 6. Is It A Bird?', 'url': 'grade-4/ws/u6c/G4U6.html'},
                'ce-bl-g4-ws-u6q': {'title': 'Quiz 6: Is It A Bird?', 'url': 'grade-4/ws/u6q/res/index.html'},
                'ce-bl-g4-ph-u1c': {'title': 'Phonics: 1. a, b, c, d, t', 'url': 'grade-4/ph/u1c/G4U1.html'},
                'ce-bl-g4-ph-u1q': {'title': 'Quiz 1: a, b, c, d, t', 'url': 'grade-4/ph/u1q/res/index.html'},
                'ce-bl-g4-ph-u2c': {'title': 'Phonics: 2. e, h, j, n, v, w', 'url': 'grade-4/ph/u2c/G4U2.html'},
                'ce-bl-g4-ph-u2q': {'title': 'Quiz 2: e, h, j, n, v, w', 'url': 'grade-4/ph/u2q/res/index.html'},
                'ce-bl-g4-ph-u3c': {'title': 'Phonics: 3. i, g, k, l, p, z', 'url': 'grade-4/ph/u3c/G4U3.html'},
                'ce-bl-g4-ph-u3q': {'title': 'Quiz 3: i, g, k, l, p, z', 'url': 'grade-4/ph/u3q/res/index.html'},
                'ce-bl-g4-ph-u4c': {'title': 'Phonics: 4. o, f, m, x', 'url': 'grade-4/ph/u4c/G4U4.html'},
                'ce-bl-g4-ph-u4q': {'title': 'Quiz 4: o, f, m, x', 'url': 'grade-4/ph/u4q/res/index.html'},
                'ce-bl-g4-ph-u5c': {'title': 'Phonics: 5. u, r, s, y', 'url': 'grade-4/ph/u5c/G4U5.html'},
                'ce-bl-g4-ph-u5q': {'title': 'Quiz 5: u, r, s, y', 'url': 'grade-4/ph/u5q/res/index.html'},
                'ce-bl-g4-exp-u1c': {'title': 'Common Expressions: 1. How Are You?', 'url': 'grade-4/exp/u1c/G4U1.html'},
                'ce-bl-g4-exp-u1q': {'title': 'Quiz 1: How Are You?', 'url': 'grade-4/exp/u1q/res/index.html'},
                'ce-bl-g4-exp-u2c': {'title': 'Common Expressions: 2. Come Here', 'url': 'grade-4/exp/u2c/G4U2.html'},
                'ce-bl-g4-exp-u2q': {'title': 'Quiz 2: Come Here', 'url': 'grade-4/exp/u2q/res/index.html'},
            },
        },
        'ce-bl-grade-5': {
            'title': '五年級',
            'items': [
                'ce-bl-g5-ws-u1c', 'ce-bl-g5-ws-u1q',
                'ce-bl-g5-ws-u2c', 'ce-bl-g5-ws-u2q',
                'ce-bl-g5-ws-u3c', 'ce-bl-g5-ws-u3q',
                'ce-bl-g5-ws-u4c', 'ce-bl-g5-ws-u4q',
                'ce-bl-g5-ws-u5c', 'ce-bl-g5-ws-u5q',
                'ce-bl-g5-ws-u6c', 'ce-bl-g5-ws-u6q',
                'ce-bl-g5-ws-u7c', 'ce-bl-g5-ws-u7q',
                'ce-bl-g5-ws-u8c', 'ce-bl-g5-ws-u8q',
                'ce-bl-g5-ph-u1c', 'ce-bl-g5-ph-u1q',
                'ce-bl-g5-ph-u2c', 'ce-bl-g5-ph-u2q',
                'ce-bl-g5-ph-u3c', 'ce-bl-g5-ph-u3q',
                'ce-bl-g5-ph-u4c', 'ce-bl-g5-ph-u4q',
                'ce-bl-g5-exp-u1c', 'ce-bl-g5-exp-u1q',
                'ce-bl-g5-exp-u2c', 'ce-bl-g5-exp-u2q',
            ],
            'mapping': {
                'ce-bl-g5-ws-u1c': {'title': 'Words and Structures: 1. Who Is She?', 'url': 'grade-5/ws/u1c/G8U7flip new.html'},
                'ce-bl-g5-ws-u1q': {'title': 'Quiz 1: Who Is She?', 'url': 'grade-5/ws/u1q/res/index.html'},
                'ce-bl-g5-ws-u2c': {'title': 'Words and Structures: 2. How Old Are You?', 'url': 'grade-5/ws/u2c/Unit 2 new.html'},
                'ce-bl-g5-ws-u2q': {'title': 'Quiz 2: How Old Are You?', 'url': 'grade-5/ws/u2q/res/index.html'},
                'ce-bl-g5-ws-u3c': {'title': 'Words and Structures: 3. What Can You Do?', 'url': 'grade-5/ws/u3c/Unit 3 new.html'},
                'ce-bl-g5-ws-u3q': {'title': 'Quiz 3: What Can You Do?', 'url': 'grade-5/ws/u3q/res/index.html'},
                'ce-bl-g5-ws-u4c': {'title': 'Words and Structures: 4. I Am Tired.', 'url': 'grade-5/ws/u4c/Unit 4 new.html'},
                'ce-bl-g5-ws-u4q': {'title': 'Quiz 4: I Am Tired.', 'url': 'grade-5/ws/u4q/res/index.html'},
                'ce-bl-g5-ws-u5c': {'title': 'Words and Structures: 5. What Do You Want?', 'url': 'grade-5/ws/u5c/Unit 5 new.html'},
                'ce-bl-g5-ws-u5q': {'title': 'Quiz 5: What Do You Want?', 'url': 'grade-5/ws/u5q/res/index.html'},
                'ce-bl-g5-ws-u6c': {'title': 'Words and Structures: 6. What Time Is It?', 'url': 'grade-5/ws/u6c/Unit 6 new.html'},
                'ce-bl-g5-ws-u6q': {'title': 'Quiz 6: What Time Is It?', 'url': 'grade-5/ws/u6q/res/index.html'},
                'ce-bl-g5-ws-u7c': {'title': 'Words and Structures: 7. How\'s The Weather?', 'url': 'grade-5/ws/u7c/Unit 7 new.html'},
                'ce-bl-g5-ws-u7q': {'title': 'Quiz 7: How\'s The Weather?', 'url': 'grade-5/ws/u7q/res/index.html'},
                'ce-bl-g5-ws-u8c': {'title': 'Words and Structures: 8. The Boy Is Tall.', 'url': 'grade-5/ws/u8c/Unit 8 new.html'},
                'ce-bl-g5-ws-u8q': {'title': 'Quiz 8: The Boy Is Tall.', 'url': 'grade-5/ws/u8q/res/index.html'},
                'ce-bl-g5-ph-u1c': {'title': 'Phonics: 1. sh, ch', 'url': 'grade-5/ph/u1c/Unit 1 new.html'},
                'ce-bl-g5-ph-u1q': {'title': 'Quiz 1: sh, ch', 'url': 'grade-5/ph/u1q/res/index.html'},
                'ce-bl-g5-ph-u2c': {'title': 'Phonics: 2. th', 'url': 'grade-5/ph/u2c/Unit 2 new.html'},
                'ce-bl-g5-ph-u2q': {'title': 'Quiz 2: th', 'url': 'grade-5/ph/u2q/res/index.html'},
                'ce-bl-g5-ph-u3c': {'title': 'Phonics: 3. bl, br', 'url': 'grade-5/ph/u3c/Unit 3 new.html'},
                'ce-bl-g5-ph-u3q': {'title': 'Quiz 3: bl, br', 'url': 'grade-5/ph/u3q/res/index.html'},
                'ce-bl-g5-ph-u4c': {'title': 'Phonics: 4. st, sk', 'url': 'grade-5/ph/u4c/Unit 4 new.html'},
                'ce-bl-g5-ph-u4q': {'title': 'Quiz 4: st, sk', 'url': 'grade-5/ph/u4q/res/index.html'},
                'ce-bl-g5-exp-u1c': {'title': 'Common Expressions: 1. Are You OK?', 'url': 'grade-5/exp/u1c/Unit 1 new.html'},
                'ce-bl-g5-exp-u1q': {'title': 'Quiz 1: Are You OK?', 'url': 'grade-5/exp/u1q/res/index.html'},
                'ce-bl-g5-exp-u2c': {'title': 'Common Expressions: 2. Hurry Up!', 'url': 'grade-5/exp/u2c/Unit 2 new.html'},
                'ce-bl-g5-exp-u2q': {'title': 'Quiz 2: Hurry Up!', 'url': 'grade-5/exp/u2q/res/index.html'},
            },
        },
        'ce-bl-grade-6': {
            'title': '六年級',
            'items': [
                'ce-bl-g6-ws-u1c', 'ce-bl-g6-ws-u1q',
                'ce-bl-g6-ws-u2c', 'ce-bl-g6-ws-u2q',
                'ce-bl-g6-ws-u3c', 'ce-bl-g6-ws-u3q',
                'ce-bl-g6-ws-u4c', 'ce-bl-g6-ws-u4q',
                'ce-bl-g6-ws-u5c', 'ce-bl-g6-ws-u5q',
                'ce-bl-g6-ws-u6c', 'ce-bl-g6-ws-u6q',
                'ce-bl-g6-ws-u7c', 'ce-bl-g6-ws-u7q',
                'ce-bl-g6-ws-u8c', 'ce-bl-g6-ws-u8q',
                'ce-bl-g6-ph-u1c', 'ce-bl-g6-ph-u1q',
                'ce-bl-g6-ph-u2c', 'ce-bl-g6-ph-u2q',
                'ce-bl-g6-exp-u1c', 'ce-bl-g6-exp-u1q',
                'ce-bl-g6-exp-u2c', 'ce-bl-g6-exp-u2q',
            ],
            'mapping': {
                'ce-bl-g6-ws-u1c': {'title': 'Words and Structures: 1. Are You A Student?', 'url': 'grade-6/ws/u1c/Everything Looks New!.html'},
                'ce-bl-g6-ws-u1q': {'title': 'Quiz 1: Are You A Student?', 'url': 'grade-6/ws/u1q/res/index.html'},
                'ce-bl-g6-ws-u2c': {'title': 'Words and Structures: 2. What Are You Doing?', 'url': 'grade-6/ws/u2c/U2 content.html'},
                'ce-bl-g6-ws-u2q': {'title': 'Quiz 2: What Are You Doing?', 'url': 'grade-6/ws/u2q/res/index.html'},
                'ce-bl-g6-ws-u3c': {'title': 'Words and Structures: 3. Do You Like Art?', 'url': 'grade-6/ws/u3c/U3 content.html'},
                'ce-bl-g6-ws-u3q': {'title': 'Quiz 3: Do You Like Art?', 'url': 'grade-6/ws/u3q/res/index.html'},
                'ce-bl-g6-ws-u4c': {'title': 'Words and Structures: 4. I\'m Drawing A Car', 'url': 'grade-6/ws/u4c/U4 content.html'},
                'ce-bl-g6-ws-u4q': {'title': 'Quiz 4: I\'m Drawing A Car', 'url': 'grade-6/ws/u4q/res/index.html'},
                'ce-bl-g6-ws-u5c': {'title': 'Words and Structures: 5. Where Are You?', 'url': 'grade-6/ws/u5c/U5 content.html'},
                'ce-bl-g6-ws-u5q': {'title': 'Quiz 5: Where Are You?', 'url': 'grade-6/ws/u5q/res/index.html'},
                'ce-bl-g6-ws-u6c': {'title': 'Words and Structures: 6. Where Are You Going?', 'url': 'grade-6/ws/u6c/U6 content.html'},
                'ce-bl-g6-ws-u6q': {'title': 'Quiz 6: Where Are You Going?', 'url': 'grade-6/ws/u6q/res/index.html'},
                'ce-bl-g6-ws-u7c': {'title': 'Words and Structures: 7. Where Is My Book?', 'url': 'grade-6/ws/u7c/U7 content.html'},
                'ce-bl-g6-ws-u7q': {'title': 'Quiz 7: Where Is My Book?', 'url': 'grade-6/ws/u7q/res/index.html'},
                'ce-bl-g6-ws-u8c': {'title': 'Words and Structures: 8. I Have A Long T-shirt', 'url': 'grade-6/ws/u8c/U8 content.html'},
                'ce-bl-g6-ws-u8q': {'title': 'Quiz 8: I Have A Long T-shirt', 'url': 'grade-6/ws/u8q/res/index.html'},
                'ce-bl-g6-ph-u1c': {'title': 'Phonics: 1. a_e, e_e, i_e, o_e, u_e', 'url': 'grade-6/ph/u1c/Phonics U1 content.html'},
                'ce-bl-g6-ph-u1q': {'title': 'Quiz 1: a_e, e_e, i_e, o_e, u_e', 'url': 'grade-6/ph/u1q/res/index.html'},
                'ce-bl-g6-ph-u2c': {'title': 'Phonics: 2. ee, ea, ay', 'url': 'grade-6/ph/u2c/Phonics U2 content.html'},
                'ce-bl-g6-ph-u2q': {'title': 'Quiz 2: ee, ea, ay', 'url': 'grade-6/ph/u2q/res/index.html'},
                'ce-bl-g6-exp-u1c': {'title': 'Common Expressions: 1. Excuse Me', 'url': 'grade-6/exp/u1c/Expression U1 content.html'},
                'ce-bl-g6-exp-u1q': {'title': 'Quiz 1: Excuse Me', 'url': 'grade-6/exp/u1q/res/index.html'},
                'ce-bl-g6-exp-u2c': {'title': 'Common Expressions: 2. Watch Out!', 'url': 'grade-6/exp/u2c/Expression U2 content.html'},
                'ce-bl-g6-exp-u2q': {'title': 'Quiz 2: Watch Out!', 'url': 'grade-6/exp/u2q/res/index.html'},
            },
        },
        'ce-bl-grade-7': {
            'title': '七年級',
            'items': [
                'ce-bl-g7-le-u1c', 'ce-bl-g7-le-u1q',
                'ce-bl-g7-le-u2c', 'ce-bl-g7-le-u2q',
                'ce-bl-g7-le-u3c', 'ce-bl-g7-le-u3q',
                'ce-bl-g7-le-u4c', 'ce-bl-g7-le-u4q',
                'ce-bl-g7-le-u5c', 'ce-bl-g7-le-u5q',
                'ce-bl-g7-le-u6c', 'ce-bl-g7-le-u6q',
                'ce-bl-g7-le-u7c', 'ce-bl-g7-le-u7q',
                'ce-bl-g7-le-u8c', 'ce-bl-g7-le-u8q',
            ],
            'mapping': {
                'ce-bl-g7-le-u1c': {'title': '1. What Is His Phone Number?', 'url': 'grade-7/le/u1c/G7U1.html'},
                'ce-bl-g7-le-u1q': {'title': 'Quiz 1: What Is His Phone Number?', 'url': 'grade-7/le/u1q/res/index.html'},
                'ce-bl-g7-le-u2c': {'title': '2. Please Spell Your Name', 'url': 'grade-7/le/u2c/G7U2.html'},
                'ce-bl-g7-le-u2q': {'title': 'Quiz 2: Please Spell Your Name', 'url': 'grade-7/le/u2q/res/index.html'},
                'ce-bl-g7-le-u3c': {'title': '3. My Aunt Is A Singer', 'url': 'grade-7/le/u3c/G7U3.html'},
                'ce-bl-g7-le-u3q': {'title': 'Quiz 3: My Aunt Is A Singer', 'url': 'grade-7/le/u3q/res/index.html'},
                'ce-bl-g7-le-u4c': {'title': '4. Where Are They?', 'url': 'grade-7/le/u4c/G7U4.html'},
                'ce-bl-g7-le-u4q': {'title': 'Quiz 4: Where Are They?', 'url': 'grade-7/le/u4q/res/index.html'},
                'ce-bl-g7-le-u5c': {'title': '5. What\'s Mr. Lin Doing?', 'url': 'grade-7/le/u5c/G7U5.html'},
                'ce-bl-g7-le-u5q': {'title': 'Quiz 5: What\'s Mr. Lin Doing?', 'url': 'grade-7/le/u5q/res/index.html'},
                'ce-bl-g7-le-u6c': {'title': '6. There Are Monkeys In The Picture', 'url': 'grade-7/le/u6c/G7U6.html'},
                'ce-bl-g7-le-u6q': {'title': 'Quiz 6: There Are Monkeys In The Picture', 'url': 'grade-7/le/u6q/res/index.html'},
                'ce-bl-g7-le-u7c': {'title': '7. Do You Have Art Class On Tuesday?', 'url': 'grade-7/le/u7c/G7U7.html'},
                'ce-bl-g7-le-u7q': {'title': 'Quiz 7: Do You Have Art Class On Tuesday?', 'url': 'grade-7/le/u7q/res/index.html'},
                'ce-bl-g7-le-u8c': {'title': '8. Does It Have Big Feet?', 'url': 'grade-7/le/u8c/G7U8.html'},
                'ce-bl-g7-le-u8q': {'title': 'Quiz 8: Does It Have Big Feet?', 'url': 'grade-7/le/u8q/res/index.html'},
            },
        },
        'ce-bl-grade-8': {
            'title': '八年級',
            'items': [
                'ce-bl-g8-le-u1c', 'ce-bl-g8-le-u1q',
                'ce-bl-g8-le-u2c', 'ce-bl-g8-le-u2q',
                'ce-bl-g8-le-u3c', 'ce-bl-g8-le-u3q',
                'ce-bl-g8-le-u4c', 'ce-bl-g8-le-u4q',
                'ce-bl-g8-le-u5c', 'ce-bl-g8-le-u5q',
                'ce-bl-g8-le-u6c', 'ce-bl-g8-le-u6q',
                'ce-bl-g8-le-u7c', 'ce-bl-g8-le-u7q',
            ],
            'mapping': {
                'ce-bl-g8-le-u1c': {'title': '1. Which Do You Like, Coffee Or Tea?', 'url': 'grade-8/le/u1c/G8U1ce.html'},
                'ce-bl-g8-le-u1q': {'title': 'Quiz 1: Which Do You Like, Coffee Or Tea?', 'url': 'grade-8/le/u1q/res/index.html'},
                'ce-bl-g8-le-u2c': {'title': '2. How Many Oranges Do We Need?', 'url': 'grade-8/le/u2c/G8U2 flip.html'},
                'ce-bl-g8-le-u2q': {'title': 'Quiz 2: How Many Oranges Do We Need?', 'url': 'grade-8/le/u2q/res/index.html'},
                'ce-bl-g8-le-u3c': {'title': '3. It Was Warm Last Spring', 'url': 'grade-8/le/u3c/G8U3flip.html'},
                'ce-bl-g8-le-u3q': {'title': 'Quiz 3: It Was Warm Last Spring', 'url': 'grade-8/le/u3q/res/index.html'},
                'ce-bl-g8-le-u4c': {'title': '4. Tim Invited Many People To His Party', 'url': 'grade-8/le/u4c/G8U4flip.html'},
                'ce-bl-g8-le-u4q': {'title': 'Quiz 4: Tim Invited Many People To His Party', 'url': 'grade-8/le/u4q/res/index.html'},
                'ce-bl-g8-le-u5c': {'title': '5. What Did You Do Last Weekend?', 'url': 'grade-8/le/u5c/G8U5flip.html'},
                'ce-bl-g8-le-u5q': {'title': 'Quiz 5: What Did You Do Last Weekend?', 'url': 'grade-8/le/u5q/res/index.html'},
                'ce-bl-g8-le-u6c': {'title': '6. I Will Wear My New Jacket', 'url': 'grade-8/le/u6c/G8U6flip.html'},
                'ce-bl-g8-le-u6q': {'title': 'Quiz 6: I Will Wear My New Jacket', 'url': 'grade-8/le/u6q/res/index.html'},
                'ce-bl-g8-le-u7c': {'title': '7. How Do I Get To The Train Station?', 'url': 'grade-8/le/u7c/G8U7flip.html'},
                'ce-bl-g8-le-u7q': {'title': 'Quiz 7: How Do I Get To The Train Station?', 'url': 'grade-8/le/u7q/res/index.html'},
            },
        },
        'ce-bl-grade-9': {
            'title': '九年級',
            'items': [
                'ce-bl-g9-le-u1c', 'ce-bl-g9-le-u1q',
                'ce-bl-g9-le-u2c', 'ce-bl-g9-le-u2q',
                'ce-bl-g9-le-u3c', 'ce-bl-g9-le-u3q',
                'ce-bl-g9-le-u4c', 'ce-bl-g9-le-u4q',
                'ce-bl-g9-le-u5c', 'ce-bl-g9-le-u5q',
            ],
            'mapping': {
                'ce-bl-g9-le-u1c': {'title': '1. He Feels Sick', 'url': 'grade-9/le/u1c/G9U1.html'},
                'ce-bl-g9-le-u1q': {'title': 'Quiz 1: He Feels Sick', 'url': 'grade-9/le/u1q/res/index.html'},
                'ce-bl-g9-le-u2c': {'title': '2. Who Is Bigger?', 'url': 'grade-9/le/u2c/G9U2(new).html'},
                'ce-bl-g9-le-u2q': {'title': 'Quiz 2: Who Is Bigger?', 'url': 'grade-9/le/u2q/res/index.html'},
                'ce-bl-g9-le-u3c': {'title': '3. Peter Is The Best Student In Our School', 'url': 'grade-9/le/u3c/G9U3(new).html'},
                'ce-bl-g9-le-u3q': {'title': 'Quiz 3: Peter Is The Best Student In Our School', 'url': 'grade-9/le/u3q/res/index.html'},
                'ce-bl-g9-le-u4c': {'title': '4. Andy Has Done The Dishes', 'url': 'grade-9/le/u4c/G9U4(new).html'},
                'ce-bl-g9-le-u4q': {'title': 'Quiz 4: Andy Has Done The Dishes', 'url': 'grade-9/le/u4q/res/index.html'},
                'ce-bl-g9-le-u5c': {'title': '5. It Was Made By My Mom', 'url': 'grade-9/le/u5c/G9U5(new).html'},
                'ce-bl-g9-le-u5q': {'title': 'Quiz 5: It Was Made By My Mom', 'url': 'grade-9/le/u5q/res/index.html'},
            },
        },
    }
