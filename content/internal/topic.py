# -*- coding: utf-8 -*-
import base64
import datetime
import json
import logging
import os
import pickle
import re
from typing import Callable

from google.cloud import datastore

import content.exception
import instance_cache
from api import request_cache
from . import internal
from .content_prototype import Content
from .cool_english import CoolEnglishBasicLearning

# properties of topic model that should be excluded from index
EXCLUDE_FROM_INDEXES = (
    'extended_slug',
    'description',
    'init_custom_stack',
    'icon_src',
    'created_on',
    'updated_on',
    'last_edited_by',
    'logo_src',
    'banner_src',
    'intro',
    'links',
)

SPECIAL_TAGS = [
    'mission-add-exam-tab',
    'has-pre-exam',
    'has-post-exam',
    'has-topic-exam',
    'grouping_eleme',
    'grouping_junio',
    'grouping_senio',
    'grouping_unive',
    'grouping_grade_1',
    'grouping_topic',
    'grouping_grade',
    'publisher_jun',
    'publisher_han',
    'publisher_nan',
    'publisher_kan',
]


class Topic(Content):
    # === 基本欄位: Topic 資料庫欄位的子集合 ===
    title: str
    standalone_title: str
    id: str
    extended_slug: str  # #2975 fix 之後，all topic in published versions has extended_slug
    description: str
    tags: [str]
    hide: bool
    icon_src: str
    logo_src: str
    banner_src: str
    intro: str
    links: [] = []
    # === 關聯欄位：需要額外操作才能填入的欄位，使用前請檢查是否填入 ===
    children: [] = None
    ancestors: [] = None  # from parent to root, ancestors[0] is parent Topic, ancestors[-1] is root Topic

    # === Dependency injection 欄位 ===
    content_factory: Callable = None

    # === Private property ===
    # _property_from_entity 列舉的欄位會轉存到 Topic, 其餘還有
    # child_keys
    # parent_keys
    _property_from_entity = ['title', 'standalone_title', 'id', 'extended_slug', 'description',
                             'tags', 'hide', 'icon_src', 'logo_src', 'banner_src', 'intro']

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def has_children(self):
        return True if len(self._entity['child_keys']) > 0 else False

    @property
    def contains_teaching_material(self):
        # 目前酷英的資料不在 DB 之中, 為避免在無 child 時直接呼叫開此特例
        if self.id in CoolEnglishBasicLearning.TopicData:
            return True
        return not all(child_key.kind == 'Topic' for child_key in self._entity['child_keys'])

    @property
    def is_live(self):
        return not self.hide

    @property
    def is_teaching_material(self):
        return False

    @property
    def is_editable(self):
        edit_version = None
        try:
            edit_version = internal.get_edit_version()
        except internal.InvalidContentTree:
            # edit version 不存在，因此一定是無法編輯
            return False
        if self._entity['version'] == edit_version.key:
            return True
        return False

    @property
    def topic_page_url(self):
        return '/%s' % self.extended_slug

    def __init__(self, *, entity, parent_topic=None):
        # live content tree 存在沒有 child_keys 的 Topic (不是分隔線)
        if 'child_keys' not in entity:
            entity['child_keys'] = []

        super().__init__(entity, parent_topic)
        if not self.tags:
            self.tags = []
        if 'links' in self._entity:
            self.links = json.loads(self._entity['links']) or []

    @classmethod
    def from_id(cls, *, topic_id: str, version_id=None, current_user=None):
        is_moderator = current_user and current_user.is_moderator
        # 目前僅先開放 edit version，除此之外都抓 default version
        # 僅開放 moderator 讀取 edit version
        if version_id == 'edit':
            if is_moderator:
                version = internal.get_edit_version()
            else:
                raise PermissionError('Only moderators are allowed to get edit tree.')
        else:
            version = internal.get_default_version()
        entity = _get_by_id(topic_id, version)
        if entity is None:
            raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)
        assert ('is_section' not in entity or not entity['is_section'])
        return cls(entity=entity)

    def _get_cool_eng_children(self):
        entities = []
        for child_key, child_info in CoolEnglishBasicLearning.TopicData[self.id]['mapping'].items():
            key = datastore.Key('Iframe', child_key, project='junyiacademy')
            self._entity['child_keys'].append(key)
            entity = datastore.Entity(key)
            entity['id'] = child_key
            entity['title'] = child_info['title']
            entities.append(entity)
        return entities

    def _get_children(self, kind=None, limit=-1):
        if self.content_factory is None:
            raise AttributeError("Require content_factory to initialize children.")

        if self.id in CoolEnglishBasicLearning.TopicData:
            entities = self._get_cool_eng_children()
        else:
            if kind == None:
                child_keys = self._entity['child_keys']
            else:
                # if kind is given (not `None`), filter out keys whose kind is not matched
                child_keys = [k for k in self._entity['child_keys'] if k.kind == kind]
            if limit > 0:
                child_keys = child_keys[:limit]
            entities = internal.datastore_ordered_get_multi(child_keys)
            if self.is_editable:
                apply_content_change(entities)
        return [self.content_factory(entity=entity,
                                     parent_topic=self,
                                     _treat_topic_as_divider=self.contains_teaching_material)
                for entity in entities]

    def _init_children(self):
        self.children = self._get_children()

    def _init_ancestors(self):
        entities = internal.datastore_ordered_get_multi(self._entity['ancestor_keys'])
        self.ancestors = [Topic(entity=entity) for entity in entities]

    def info_to_topic_page(self, leading_topic=False, visible_content_selector=None):
        ret = {
            'topic_id': self.id,
            'title': self.presented_title,
            'description': self.description,
            'intro': self.intro,
            'tags': self.tags,
            'icon_src': self.icon_src,
            'child': [],
            'is_content_topic': self.contains_teaching_material
        }
        assert visible_content_selector is not None
        if leading_topic:
            if self.children is None:
                self._init_children()
            ret['breadcrumb'] = self.get_breadcrumb_data(visible_content_selector)
            ret['extended_slug'] = self.extended_slug
            ret['logo_src'] = self.get_logo_data(visible_content_selector)
            ret['banner_src'] = self.banner_src
            ret['links'] = self.links

            video_count = 0
            exercise_count = 0

            def add_child(child):
                nonlocal ret
                nonlocal video_count
                nonlocal exercise_count

                if not visible_content_selector(child):
                    return
                child.content_factory = self.content_factory
                if child.__class__.__name__ == 'Topic':
                    child_info = child.info_to_topic_page(visible_content_selector=visible_content_selector)
                else:
                    child_info = child.info_to_topic_page()
                ret['child'].append(child_info)
                if child.__class__.__name__ == 'Video':
                    video_count += 1
                if child.__class__.__name__ == 'Exercise':
                    exercise_count += 1

            for topic_child in self.children:
                add_child(topic_child)
                if topic_child.__class__.__name__ == 'Section':
                    topic_child.init_children(self.content_factory)
                    ret['is_content_topic'] |= topic_child.contains_teaching_material
                    for section_child in topic_child.children:
                        add_child(section_child)

            if ret['is_content_topic']:
                ret['child_video_count'] = video_count
                ret['child_exercise_count'] = exercise_count
        else:
            # 不是 leading topic，所以是 leading topic 的 child
            # 除了基本資訊之外，另外加入前六個 child topic 的 title
            # https://gitlab.com/junyiacademy/junyiacademy/-/issues/3902
            # it's not leading topic, we only need 'Topic' kind children
            first_topics = self._get_children(kind='Topic', limit=6)
            for topic_child in first_topics:
                if visible_content_selector(topic_child):
                    ret['child'].append(topic_child.presented_title)

        return ret

    def dump(self):
        return {
            'kind': self.__class__.__name__,
            'id': self.id,
            'title': self.title,
            'hide': not self.hide,
        }

    def topic_to_json(self, version):
        ret = {
            'kind': 'Topic',
            'entity_key_name': self._entity.key.name,
            'id': self.id,
            'title': self.title,
            'standalone_title': self.standalone_title,
            'version': version,
            'children': [{'kind': child.kind, 'name': child['name'],
                          'entity_key_id': child.key.id}
                         for child in self.children]
        }
        return ret

    def get_children_entities_of_kind(self, kind):
        child_keys_match_kind = filter(lambda child: child.kind == kind,
                                       self._entity['child_keys'])
        return internal.datastore_ordered_get_multi(list(child_keys_match_kind))

    def subtree(self):
        if not self.has_children:
            return None
        if self.id == 'new-and-noteworthy' or (not self.is_live and self.id != 'root'):
            return None

        def odd_is_content_topic(t: Topic):
            return any(child_key.kind in ("Video", "Exercise", "Exam", "Article")
                       for child_key in t._entity['child_keys'])

        child_topic_entities = self.get_children_entities_of_kind('Topic')
        child_topic_subtrees = []
        for child_topic_entity in child_topic_entities:
            child_topic_subtree = Topic(entity=child_topic_entity).subtree()
            if child_topic_subtree is not None:
                child_topic_subtrees.append(child_topic_subtree)

        return {
            'id': self.id,
            'key': self._entity.key.to_legacy_urlsafe(location_prefix='s~').decode('utf-8'),
            'title': self.title,
            'tags': self.tags,
            'isContentTopic': odd_is_content_topic(self),
            'childTopics': child_topic_subtrees
        }

    def get_parent(self):
        """
        不建議直接取用 self._parent_topic, self._parent_topic 可能未賦值
        :return: parent Topic
        """
        if self._parent_topic:
            return self._parent_topic

        # root has no parent
        if self.id == 'root':
            return None

        parent_entity = internal.get_client().get(self._entity['parent_keys'][0])
        self._parent_topic = Topic(entity=parent_entity)
        return self._parent_topic

    def get_breadcrumb_data(self, visible_content_selector):
        """
        =若整串路徑都可見=
        breadcrumb 是從 root 往下第一層 (e.g. 主題式）起算，不含 self 的路徑

        假設階層關係為：主題式 > 國小-數與量 > 整數 ，則
        1. self = 主題式: breadcrumb = []
        2. self = 國小-數與量: breadcrumb = [主題式]
        3. self = 整數: breadcrumb = [主題式, 國小-數與量]

        =若階層關係中有主題不可見=
        先展開可見版 breadcrumb．若 breadcrumb[N] 不可見，則回傳 breadcrumb[N+1:]
        假設階層關係為：主題式 > 國中-數學三百問 (不可見) > 數與量 > 整數與數線
        1. self = 主題式: breadcrumb = []
        2. self = 國中-數學三百問 (不可見): breadcrumb = [主題式]  （此案例應該不會發生，應該在外面擋住）
        3. self = 數與量: breadcrumb = []
        4. self = 整數與數線: breadcrumb = [數與量]

        :param visible_content_selector: 判定 Topic 是否可見的 function
        """
        parent = self.get_parent()
        if not parent or not visible_content_selector(parent):
            return []

        if parent.id == 'root':
            return []

        my_piece = {
            "topic_id": parent.id,
            "title": parent.title,
            "tags": parent.tags,
        }
        breadcrumb = parent.get_breadcrumb_data(visible_content_selector)
        breadcrumb.append(my_piece)
        return breadcrumb

    def get_logo_data(self, visible_content_selector):
        """
        取自己或是離自己最近的 ancestor 的 logo
        :param visible_content_selector: 判定 Topic 是否可見的 function
        """
        if self.logo_src:
            return self.logo_src
        parent = self.get_parent()
        if not parent or not visible_content_selector(parent):
            return None
        return parent.get_logo_data(visible_content_selector)

    # Generates the slug path of this topic, including parents,
    # i.e. math/arithmetic/fractions
    def generate_extended_slug(self):
        self._init_ancestors()
        parent_ids = [topic.id for topic in self.ancestors]
        parent_ids.reverse()
        if len(parent_ids) > 1:
            if parent_ids[0] != "root":
                logging.error("first ancestor is not root. id: %s, parent_ids[0]: %s" % (self.id, parent_ids[0]))
            slug = "%s/%s" % ('/'.join(parent_ids[1:]), self.id)
        else:
            slug = self.id
        return slug

    def update_extended_slug(self, skip_put=False):
        self.extended_slug = self.generate_extended_slug()
        # FIXME: put
        # if not skip_put:
        #     self.put()
        return self.extended_slug


class Divider:
    """
    跟 content 同層的 Topic 在 topic page 顯示為分隔線，後台顯示為資料夾
    """
    _topic: Topic

    def __init__(self, entity, parent_topic=None):
        self._topic = Topic(entity=entity, parent_topic=parent_topic)

    @property
    def is_live(self):
        return self._topic.is_live

    @property
    def presented_title(self):
        if self.is_live:
            return self._topic.title
        return self._topic.title + ' [hidden]'

    def info_to_topic_page(self):
        return {
            'url': self._topic.topic_page_url,
            'type': 'Topic',
            'title': self.presented_title,
            'description': self._topic.description,
            'intro': self._topic.intro,
            'progress_id': None,
            'id': None,
            'is_content': self._topic.is_teaching_material,
            'has_children': self._topic.has_children,
            'topic_id': self._topic.id,
            'tags': self._topic.tags
        }


class Section(Content):
    """
    小節，在 topic page 顯示為分隔線，底層可以看作特殊且功能較少的 Topic，
    跟 Topic 一樣存在 datastore Topic kind
    在同一個 TopicVersion 中, Section id 不可以和 Section 或 Topic 重複
    """
    title: str
    standalone_title: str
    id: str
    extended_slug: str  # #2975 fix 之後，all topic in published versions has extended_slug
    children: [] = None
    ancestors: [] = None  # from parent to root, ancestors[0] is parent Topic, ancestors[-1] is root Topic
    version: datastore.entity.Entity = None

    # === Dependency injection 欄位 ===
    content_factory: Callable = None

    # === Private property ===
    _property_from_entity = ['title', 'standalone_title', 'id', 'extended_slug']

    def __init__(self, *, entity, parent_topic=None, version=None):
        if 'child_keys' not in entity:
            entity['child_keys'] = []
        super().__init__(entity, parent_topic)
        self.version = version

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def is_live(self):
        return True

    @property
    def is_teaching_material(self):
        return False

    @property
    def is_editable(self):
        if not self.version:
            raise NotImplementedError()
        if self.version['edit'] is True:
            return True
        return False

    @property
    def contains_teaching_material(self):
        return not all(child_key.kind == 'Topic' for child_key in self._entity['child_keys'])

    @classmethod
    def from_id(cls, *, section_id: str, version_id=None):
        version = internal.get_version(version_id)
        root_key = get_root_key(version=version)
        entity = _get_by_id(section_id, version, ancestor=root_key)
        if entity is None:
            raise content.exception.ContentNotExistsError('No Section for section_id [%s] in version [%s]' %
                                                          (section_id, version_id))
        assert (entity['is_section'] == True)
        return cls(entity=entity, version=version)

    def init_children(self, content_factory):
        self.content_factory = content_factory
        self._init_children()

    def _init_children(self):
        if self.content_factory is None:
            raise AttributeError("Require content_factory to initialize children.")

        entities = internal.datastore_ordered_get_multi(self._entity['child_keys'])

        self.children = [self.content_factory(entity=entity,
                                              parent_topic=self)
                         for entity in entities]

    def dump(self):
        if self.children is None:
            self._init_children()

        return {
            'kind': 'Section',
            'id': self.id,
            'title': self.title,
            'standalone_title': self.standalone_title,
            'children': [c.dump() for c in self.children],  # TODO check required info
        }

    def info_to_topic_page(self):
        """
        為了維持 /api/content/topicpage 的外觀，真小節仿造假小節（Divider) 輸出
        """
        return {
            'url': '/%s' % self.extended_slug,
            'type': 'Topic',
            'title': self.title,
            'description': '',
            'intro': '',
            'progress_id': None,
            'id': None,
            'is_content': self.is_teaching_material,
            'has_children': False,
            'topic_id': self.id,
            'tags': []
        }

    def add_children(self, child_keys):
        self._entity['child_keys'].extend(child_keys)
        client = internal.get_client()
        client.put(self._entity)


def get_new_key_name():
    return base64.urlsafe_b64encode(os.urandom(30)).decode()


def get_key(key_name, parent):
    client = internal.get_client()
    return client.key('Topic', key_name, parent=parent)


def _get_by_id(topic_id, version, ancestor=None):
    query = internal.get_query('Topic', ancestor)
    query.add_filter('id', '=', topic_id)
    query.add_filter('version', '=', version.key)
    fetched = list(query.fetch(1))
    if len(fetched) == 0:
        return None
    return fetched[0]


def get_root(version_id):
    version = internal.get_version(version_id)
    root_key = get_root_key(version)
    client = internal.get_client()
    root_entity = client.get(root_key)
    if not root_entity:
        raise content.exception.ContentNotExistsError(u"root of version [%i] not exists"
                                                      % version['number'])
    return Topic(entity=root_entity)


@instance_cache.cache_with_key_fxn(
    lambda version: "get_root_key_%d" % version['number'],
    available_seconds=0)
def get_root_key(version):
    return _get_by_id('root', version).key


def get_all_topics(version):
    datastore_client = internal.get_client()
    query = datastore_client.query(kind='Topic')
    query.add_filter('version', '=', version.key)
    return list(query.fetch())


def get_content_topics(version=None):
    topics = get_all_topics(version)
    content_topics = []
    for topic in topics:
        if 'child_keys' not in topic:
            continue
        for child_key in topic['child_keys']:
            if child_key.kind != 'Topic':
                content_topics.append(topic)
                break

    content_topics.sort(key=lambda topic: topic['standalone_title'])
    return content_topics


def get_filled_content_topics(types=[], version=None):
    topics = get_content_topics(version)

    child_dict = {}
    for topic in topics:
        for key in topic['child_keys']:
            if key.kind in types or (len(types) == 0 and key.kind != 'Topic'):
                child_dict[key] = True

    child_dict = dict((e.key, e) for e in internal.datastore_unordered_get_multi(
        list(child_dict.keys())))

    filled_topics = []
    for topic in topics:
        filled_topics.append(Topic(entity=topic))
        filled_topics[-1].children = [
            child_dict[key] for key in topic['child_keys'] if key in child_dict]
    return filled_topics


def get_new_id(parent_id, title, version):
    if title:
        potential_id = title.lower()
        potential_id = re.sub('[^a-z0-9]', '-', potential_id)
        # remove any trailing dashes (see issue 1140)
        potential_id = re.sub('-+$', '', potential_id)
        # remove any leading dashes (see issue 1526)
        potential_id = re.sub('^-+', '', potential_id)

        if potential_id[0].isdigit():
            potential_id = parent_id + "-" + potential_id
    else:
        potential_id = '1'

    potential_id = "v" + str(version['number']) + "-" + potential_id

    number_to_add = 0
    current_id = potential_id
    root = get_root_key(version=version)
    while True:
        matching_topic = _get_by_id(current_id, version, root)
        if matching_topic is None:  # id is unique so use it and break out
            return current_id
        else:  # id is not unique so will have to go through loop again
            number_to_add += 1
            current_id = '%s-%s' % (potential_id, number_to_add)


def create_topic(title, parent_id, topic_id=None, **kwargs):
    if not parent_id:
        raise content.exception.ContentNotExistsError(u"預計擺放的母資料夾 [%s] 不存在" % parent_id)

    version = internal.get_edit_version()

    # generate new topic id if not available
    if not topic_id:
        topic_id = get_new_id(parent_id, title, version)
        logging.info("created a new id %s for %s", topic_id, title)

    validate_id_format(topic_id)
    validate_title(title)

    # setting the root to be the parent so that inserts and deletes can be done in a transaction
    key_name = get_new_key_name()
    root_key = get_root_key(version=version)
    new_topic_key = get_key(key_name, root_key)

    # TODO: should do this in Topic class?
    new_topic_args = {
        'title': title,
        'standalone_title': title,
        'id': topic_id,
        'extended_slug': None,  # will be updated after we got parent entity in txn
        'description': None,
        'parent_keys': [],  # will be updated after we got parent entity in txn
        'ancestor_keys': [],  # will be updated after we got parent entity in txn
        'init_custom_stack': None,  # TODO: remove this?
        'version': version.key,
        'hide': False,
        'created_on': datetime.datetime.utcnow(),
        'updated_on': datetime.datetime.utcnow(),
        'last_edited_by': None,  # FIXME: current user
        'backup_timestamp': datetime.datetime.utcnow(),
    }
    if 'standalone_title' in kwargs:
        new_topic_args['standalone_title'] = kwargs['standalone_title']
    if 'description' in kwargs:
        new_topic_args['description'] = kwargs['description']
    if 'is_section' in kwargs:
        new_topic_args['is_section'] = True

    with internal.get_client().transaction():
        new_topic_entity = _create_topic_txn(root_key, version, parent_id, new_topic_key,
                                             new_topic_args)

    return new_topic_entity


def _create_topic_txn(root_key, version, parent_id, new_topic_key, new_topic_args):
    client = internal.get_client()
    topic_id = new_topic_args['id']

    # check parent
    parent_topic_entity = _get_by_id(parent_id, version, ancestor=root_key)
    if not parent_topic_entity:
        raise content.exception.ContentNotExistsError(u"預計擺放的母資料夾 [%s] 不存在" % parent_id)

    # 小節下不能建立 Topic and Section
    if 'is_section' in parent_topic_entity and parent_topic_entity['is_section']:
        raise content.exception.InvalidParent(u'不合法的操作：小節下不能建立主題或小節\n'
                                              'Details: parent id [%s], new %s id [%s]' %
                                              (parent_id,
                                               'section' if 'is_section' in new_topic_args else 'topic',
                                               new_topic_args['id']))

    # check if topic already exists
    existing_topic_entity = _get_by_id(topic_id, version, ancestor=root_key)
    if existing_topic_entity:
        raise content.exception.TopicExistsError(u"已存在代號是 %s 的資料夾，該資料夾的標題是 [%s]" % (topic_id, existing_topic_entity['title']))

    # check if key already exists
    topic = client.get(new_topic_key)
    if topic is not None:
        raise content.exception.TopicKeyExistsError("auto generated random key name '%s' already exists. try it again." % new_topic_key.name)

    # create new entity
    new_topic_entity = datastore.Entity(key=new_topic_key, exclude_from_indexes=EXCLUDE_FROM_INDEXES)
    new_topic_entity.update(new_topic_args)

    # update ancestors
    parent_keys = [parent_topic_entity.key]
    ancestor_keys = parent_keys[:]
    ancestor_keys.extend(parent_topic_entity['ancestor_keys'])
    if len(parent_topic_entity['ancestor_keys']):
        extended_slug = "%s/%s" % (parent_topic_entity['extended_slug'], topic_id)
    else:
        extended_slug = topic_id
    new_topic_entity['parent_keys'] = parent_keys
    new_topic_entity['ancestor_keys'] = ancestor_keys
    new_topic_entity['extended_slug'] = extended_slug

    client.put(new_topic_entity)

    parents = client.get_multi(new_topic_entity['parent_keys'])
    for parent in parents:
        if 'child_keys' not in parent:
            parent['child_keys'] = []
        parent['child_keys'].append(new_topic_entity.key)
        client.put(parent)

    # FIXME: update version info
    # version.update()

    return new_topic_entity


def delete_topic(topic_id):
    version = internal.get_edit_version()
    root_key = get_root_key(version=version)
    with internal.get_client().transaction():
        _delete_topic_txn(root_key, version, topic_id)
    return topic_id


def _delete_topic_txn(root_key, version, topic_id):
    client = internal.get_client()
    target_topic_entity = _get_by_id(topic_id, version, ancestor=root_key)
    if not target_topic_entity:
        raise content.exception.ContentNotExistsError(u"預計刪除的資料夾 [%s] 不存在" % topic_id)
    parents = client.get_multi(target_topic_entity['parent_keys'])
    for parent in parents:
        parent['child_keys'].remove(target_topic_entity.key)
        client.put(parent)
    client.delete(target_topic_entity.key)


def update_topic(validation_only, topic_id, **kwargs):
    version = internal.get_edit_version()

    new_topic_args = {}
    if 'title' in kwargs:
        new_topic_args['title'] = kwargs['title']
        validate_title(new_topic_args['title'])
    if 'standalone_title' in kwargs:
        new_topic_args['standalone_title'] = kwargs['standalone_title']
    if 'description' in kwargs:
        new_topic_args['description'] = kwargs['description']
    if 'id' in kwargs:
        new_topic_args['id'] = kwargs['id']
        validate_id_format(new_topic_args['id'])

    root_key = get_root_key(version=version)
    with internal.get_client().transaction():
        topic_entity = _update_topic_txn(validation_only, root_key, version, topic_id,
                                         new_topic_args)

    return topic_entity


def _update_topic_txn(validation_only, root_key, version, topic_id, new_topic_args):
    # check if topic exists
    topic_entity = _get_by_id(topic_id, version, ancestor=root_key)
    if not topic_entity:
        raise content.exception.ContentNotExistsError(u"這個代號的資料夾不存在 %s" % topic_id)

    if validation_only:
        return None

    # update entity
    changed = False
    if "id" in new_topic_args and new_topic_args["id"] != topic_id:
        # check if topic already exists
        existing_topic_entity = _get_by_id(new_topic_args["id"], version, ancestor=root_key)
        if existing_topic_entity:
            raise content.exception.TopicExistsError(u"已存在代號是 %s 的資料夾，該資料夾的標題是 [%s]" % (new_topic_args["id"], existing_topic_entity['title']))
        changed = True

    # 後端要擋 tag 數量上限
    if "tags" in new_topic_args:
        if type(new_topic_args["tags"]) == list:
            keep_tags = list(
                set(new_topic_args["tags"]).intersection(SPECIAL_TAGS))
            custom_tags = list(set(new_topic_args["tags"]) - set(SPECIAL_TAGS))
            if len(custom_tags) > 4:
                logging.warning(u'自訂標籤數量大於四個，這些 tags 將被捨棄：%s' % (', '.join(custom_tags[4:])))
            all_tags = keep_tags + custom_tags[:4]  # 只保留前 4 個
            if topic_entity["tags"] != all_tags:
                topic_entity["tags"] = all_tags
                changed = True
        del new_topic_args["tags"]

    for attr, value in new_topic_args.items():
        if topic_entity[attr] != value:
            topic_entity[attr] = value
            changed = True

    if changed:
        topic_entity['updated_on'] = datetime.datetime.utcnow()
        topic_entity['last_edited_by'] = None  # FIXME: current user
        topic_entity['backup_timestamp'] = datetime.datetime.utcnow()
        client = internal.get_client()
        client.put(topic_entity)
        # FIXME: update version info
        # version.update()

    return topic_entity


def validate_id_format(topic_id):
    if not topic_id:
        raise content.exception.InvalidFormat(topic_id, u"資料夾的代號不能空白")
    if len(topic_id) > 500:
        raise content.exception.InvalidFormat(topic_id, u"代號不能超過 500 個字, %s" % topic_id)
    if not re.match(r'^[a-z0-9_-]+$', topic_id):
        raise content.exception.InvalidFormat(topic_id, u"代號 %s 存在不合法字元" % topic_id)


def validate_title(title):
    if not title:
        raise content.exception.InvalidFormat(title, u"資料夾的標題不能空白")
    if len(title) > 100:
        raise content.exception.InvalidFormat(title, u"標題不能超過 100 個字, %s" % title)


@request_cache.cache()
def fetch_content_changes():
    edit_version = internal.get_edit_version()
    query = internal.get_query(kind='VersionContentChange', ancestor=edit_version.key)
    query.add_filter('version', '=', edit_version.key)
    content_change_dict = {}
    for change in iter(query.fetch()):
        content_change_dict[change['content']] = change
    return content_change_dict


def apply_content_change(content_list):
    """
    將 edit version 上的 content change 直接 apply 回 content_list
    """
    content_change_dict = fetch_content_changes()
    for content_base in content_list:
        if content_base.key not in content_change_dict:
            continue
        change = content_change_dict[content_base.key]
        content_base.update(pickle.loads(change['content_changes']))


def add_child(child_entity, parent_id, version):
    root_key = get_root_key(version)

    with internal.get_client().transaction():
        topic_entity = _get_by_id(parent_id, version, ancestor=root_key)
        if topic_entity is None:
            raise content.exception.ContentNotExistsError(u"這個代號的資料夾不存在 %s" % parent_id)
        if 'child_keys' not in topic_entity:
            topic_entity['child_keys'] = []
        topic_entity['child_keys'].append(child_entity.key)
        topic_entity['updated_on'] = datetime.datetime.utcnow()
        topic_entity['last_edited_by'] = None  # FIXME: current user
        topic_entity['backup_timestamp'] = datetime.datetime.utcnow()
        client = internal.get_client()
        client.put(topic_entity)

        return topic_entity
