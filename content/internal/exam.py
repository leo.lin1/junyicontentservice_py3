# -*- coding: utf-8 -*-
from .content_prototype import Content


class Exam(Content):
    name: str  # e.g. “【測驗】認識分數”
    content_list: []
    public: bool
    tags: [str]
    large_screen_only: bool

    _property_from_entity = ['name', 'content_list', 'public', 'tags', 'large_screen_only']

    @property
    def is_live(self):
        return self.public

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        if self.is_live:
            return self.name
        return self.name + ' [hidden]'

    @property
    def url(self):
        return "/exam/%s" % self.id

    @property
    def id(self):
        return self._entity.key.name

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity=entity)
        if parent_topic:
            self._parent_topic = parent_topic

    def info_to_topic_page(self):
        return {
            'url': self.url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'id': self.id,
            'is_content': self.is_teaching_material,
            'count': len(self.content_list),
            'tags': self.tags if self.tags else [],
            'large_screen_only': self.large_screen_only
        }

    def dump(self):
        return {
            'kind': self.__class__.__name__,
            'id': self.id,
            'title': self.name,
        }
