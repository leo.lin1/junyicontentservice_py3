# -*- coding: utf-8 -*-
import copy
import json
import pickle
import unittest
from unittest.mock import patch, Mock

from google.cloud import datastore

import content.exception
from content import exception
from content.factory import get_content_factory
from . import internal
from . import mock_entity
from . import topic

CHILDREN_TOPIC_PROPS = sorted(['topic_id', 'title', 'description', 'intro', 'tags', 'icon_src', 'child', 'is_content_topic'])
LEADING_TOPIC_PROPS = sorted(CHILDREN_TOPIC_PROPS + ['breadcrumb', 'extended_slug', 'logo_src', 'banner_src', 'links'])
CONTENT_TOPIC_PROPS = sorted(LEADING_TOPIC_PROPS + ['child_video_count', 'child_exercise_count'])


class TestTopic(unittest.TestCase):
    # TODO: cool english topic

    def test_init_str_prop(self):
        e = topic._get_by_id('math', internal.get_edit_version())
        t1 = topic.Topic(entity=e)
        self.assertEqual(t1.banner_src, None)
        self.assertEqual(t1.icon_src, None)
        self.assertEqual(t1.logo_src, None)
        self.assertEqual(t1.intro, None)
        e['banner_src'] = 'test_banner_src'
        e['icon_src'] = 'test_icon_src'
        e['logo_src'] = 'test_logo_src'
        e['intro'] = 'test_intro'
        t2 = topic.Topic(entity=e)
        self.assertEqual(t2.banner_src, 'test_banner_src')
        self.assertEqual(t2.icon_src, 'test_icon_src')
        self.assertEqual(t2.logo_src, 'test_logo_src')
        self.assertEqual(t2.intro, 'test_intro')

    def test_init_links(self):
        # test 'links' attr not in entity
        e, _, _ = mock_entity.get_topic_entity()
        t = topic.Topic(entity=e)
        self.assertEqual(type(t.links), list)
        self.assertEqual(len(t.links), 0)

        # test 'links' attr is null in entity
        e, _, _ = mock_entity.get_topic_entity()
        e['links'] = 'null'
        t = topic.Topic(entity=e)
        self.assertEqual(type(t.links), list)
        self.assertEqual(len(t.links), 0)

        # test 'links' attr is array with one object in entity
        links = [{"url": "/profile", "color": "brown", "icon": "fa-smile-o", "title": "test"}]
        e, _, _ = mock_entity.get_topic_entity()
        e['links'] = json.dumps(links)
        t = topic.Topic(entity=e)
        self.assertEqual(type(t.links), list)
        self.assertEqual(len(t.links), 1)
        self.assertEqual(t.links[0]['url'], links[0]['url'])

    @patch('logging.error')
    def test_update_extended_slug(self, logging_error):
        version = internal.get_default_version()
        # level 1 (child of root)
        e = topic._get_by_id('math', version=version)
        t = topic.Topic(entity=e)
        self.assertEqual(t.update_extended_slug(), 'math')
        # level 2
        e = topic._get_by_id('arithmetic', version=version)
        t = topic.Topic(entity=e)
        self.assertEqual(t.update_extended_slug(), 'math/arithmetic')
        # level 3
        e = topic._get_by_id('addition-subtraction', version=version)
        t = topic.Topic(entity=e)
        self.assertEqual(t.update_extended_slug(), 'math/arithmetic/addition-subtraction')
        # 測試錯誤的 ancestors 順序 (第一個不是 root)
        e = topic._get_by_id('arithmetic', version=version)
        e['ancestor_keys'].reverse()
        t = topic.Topic(entity=e)
        self.assertFalse(logging_error.called)
        t.update_extended_slug()
        self.assertTrue(logging_error.called)

    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_invalid_topic_id(self, default_version_patch, get_topic_patch):
        """
        模擬 topic_id 取不到 Topic entity
        """
        default_version_patch.return_value = None
        get_topic_patch.return_value = None
        self.assertRaises(exception.ContentNotExistsError, topic.Topic.from_id, topic_id='no-such-topic')
        self.assertTrue(default_version_patch.called)
        self.assertTrue(get_topic_patch.called)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    def test_init_by_topic_id_with_no_children(self, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個沒有 child_keys 的 Topic
        """
        get_topic_patch.return_value, get_children_patch.return_value, _ = \
            mock_entity.get_topic_entity(is_live=False)
        del get_topic_patch.return_value['child_keys']
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        self.assertTrue(get_topic_patch.called)
        self.assertIsNotNone(t._entity)
        self.assertFalse(t.contains_teaching_material)

        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        # 必須先設定 factory 才能呼叫 info_to_topic_page()
        self.assertRaises(AttributeError, t.info_to_topic_page, leading_topic=True,
                          visible_content_selector=lambda c: c.is_live)
        t.content_factory = get_content_factory()

        # 僅顯示 live 的 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(len(t.children), 0)
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), LEADING_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 0)

        # 顯示全部 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), LEADING_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 0)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    def test_init_by_topic_id_contains_12_topics(self, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 12 個子 topic (6 live) 的 Topic
        """
        get_topic_patch.return_value, get_children_patch_return_value, _ = \
            mock_entity.get_topic_entity(num_child_topic=12, child_is_live=[True, False])
        get_children_patch.side_effect = [get_children_patch_return_value] + [[]] * 18  # 6 + 12 child topics without grand child
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        self.assertTrue(get_topic_patch.called)
        self.assertIsNotNone(t._entity)
        self.assertFalse(t.contains_teaching_material)

        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        # 必須先設定 factory 才能呼叫 info_to_topic_page()
        self.assertRaises(AttributeError, t.info_to_topic_page, leading_topic=True,
                          visible_content_selector=lambda c: c.is_live)
        t.content_factory = get_content_factory()

        # 僅顯示 live 的 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(len(t.children), 12)
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), LEADING_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 6)
        for i in info['child']:
            self.assertEqual(sorted(list(i.keys())), CHILDREN_TOPIC_PROPS)

        # 顯示全部 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), LEADING_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 12)
        for i in info['child']:
            self.assertEqual(sorted(list(i.keys())), CHILDREN_TOPIC_PROPS)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    def test_init_by_topic_id_contains_5_topics_and_grand_child(self, get_topic_patch, get_multi_patch):
        """
        模擬透過 topic_id 取到一個含有 5 個子 topic 的 topic，每個子 topic 有不同數量的孫 topic
        """
        # 每個 child 分別有 0 ~ 8 個 grandchildren
        num_grand_child_topic = [0, 2, 4, 6, 8]
        get_topic_patch.return_value, children, grand_children = \
            mock_entity.get_topic_entity(num_child_topic=5, child_is_live=[True],
                                         num_grand_child_topic=num_grand_child_topic)
        # get_multi_patch takes only first 6 grandchildren
        for c in children:
            if c.key == get_topic_patch.return_value['child_keys'][4]:
                first_six_keys = c['child_keys'][:6]
                grand_children[4] = [a for a in grand_children[4] if a.key in first_six_keys]
        # get_multi_patch returns:
        # 1. leading topic children
        # 2. for each children, their first 6 grandchildren
        get_multi_patch.side_effect = [children] + grand_children
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(len(info['child']), 5)
        for idx, i in enumerate(info['child']):
            self.assertEqual(sorted(list(i.keys())), CHILDREN_TOPIC_PROPS)
            self.assertEqual(len(i['child']), min(num_grand_child_topic[idx], 6))

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    def test_init_by_topic_id_contains_10_contents(self, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 10 個 content (3 hide) 的 Topic
        """
        get_topic_patch.return_value, get_children_patch.return_value, _ = \
            mock_entity.get_topic_entity(num_child_each_content=2, child_is_live=[True, False])
        t = topic.Topic.from_id(topic_id='mock_topic_id')
        self.assertTrue(t.contains_teaching_material)
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(t.children[0].__class__.__name__, 'Exam')
        self.assertEqual(t.children[1].__class__.__name__, 'Article')
        self.assertEqual(t.children[2].__class__.__name__, 'Url')
        self.assertEqual(t.children[3].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[4].__class__.__name__, 'Video')
        self.assertEqual(t.children[5].__class__.__name__, 'Exam')
        self.assertEqual(t.children[6].__class__.__name__, 'Article')
        self.assertEqual(t.children[7].__class__.__name__, 'Url')
        self.assertEqual(t.children[8].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[9].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertTrue(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertFalse(t.children[5].is_live)
        self.assertFalse(t.children[6].is_live)
        self.assertTrue(t.children[7].is_live)  # Url is always live
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())),
                             CONTENT_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 7)

    @patch.object(datastore.Client, 'get_multi')
    def test_init_by_entity_contains_10_contents_and_2_dividers(self, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 10 個 content (3 hide) + 2 Divider(空 Topic) 的 Topic
        """
        topic_entity, children, _ = \
            mock_entity.get_topic_entity(num_child_topic=2, num_child_each_content=2, child_is_live=[True, False])
        get_children_patch.return_value = children
        t = topic.Topic(entity=topic_entity)
        self.assertTrue(t.contains_teaching_material)
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(t.children[0].__class__.__name__, 'Divider')
        self.assertEqual(t.children[1].__class__.__name__, 'Divider')
        self.assertEqual(t.children[2].__class__.__name__, 'Exam')
        self.assertEqual(t.children[3].__class__.__name__, 'Article')
        self.assertEqual(t.children[4].__class__.__name__, 'Url')
        self.assertEqual(t.children[5].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[6].__class__.__name__, 'Video')
        self.assertEqual(t.children[7].__class__.__name__, 'Exam')
        self.assertEqual(t.children[8].__class__.__name__, 'Article')
        self.assertEqual(t.children[9].__class__.__name__, 'Url')
        self.assertEqual(t.children[10].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[11].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertFalse(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertTrue(t.children[5].is_live)
        self.assertTrue(t.children[6].is_live)
        self.assertFalse(t.children[7].is_live)
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Url is always live
        self.assertFalse(t.children[10].is_live)
        self.assertTrue(t.children[11].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), CONTENT_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 8)

        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertEqual(t.children[0].__class__.__name__, 'Divider')
        self.assertEqual(t.children[1].__class__.__name__, 'Divider')
        self.assertEqual(t.children[2].__class__.__name__, 'Exam')
        self.assertEqual(t.children[3].__class__.__name__, 'Article')
        self.assertEqual(t.children[4].__class__.__name__, 'Url')
        self.assertEqual(t.children[5].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[6].__class__.__name__, 'Video')
        self.assertEqual(t.children[7].__class__.__name__, 'Exam')
        self.assertEqual(t.children[8].__class__.__name__, 'Article')
        self.assertEqual(t.children[9].__class__.__name__, 'Url')
        self.assertEqual(t.children[10].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[11].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertFalse(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertTrue(t.children[5].is_live)
        self.assertTrue(t.children[6].is_live)
        self.assertFalse(t.children[7].is_live)
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Url is always live
        self.assertFalse(t.children[10].is_live)
        self.assertTrue(t.children[11].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), CONTENT_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 12)

    @patch.object(datastore.Client, 'get_multi')
    def test_init_by_entity_contains_grand_child_contents_and_dividers(self, get_multi_patch):
        """
        模擬透過 topic_id 取到一個含有 2 個 child topic 的 topic，每個 child topic 各有 child content 及 divider
        """
        num_grand_child_topic = [2, 8]
        topic_entity, children, grand_children = \
            mock_entity.get_topic_entity(num_child_topic=2, child_is_live=[True],
                                         num_grand_child_topic=num_grand_child_topic, num_grand_child_each_content=1)
        grand_children_topic = []
        for g in grand_children:
            # keep only 'Topic' kind grand children
            grand_children_topic.append([c for c in g if c.kind == "Topic"])
        for c in children:
            if c.key == topic_entity['child_keys'][1]:
                # get_multi_patch takes only first 6 grandchildren
                first_six_keys = c['child_keys'][:6]
                grand_children_topic[1] = [a for a in grand_children_topic[1] if a.key in first_six_keys]
        # get_multi_patch returns:
        # 1. leading topic children
        # 2. for each children, their first 6 grandchildren which is 'Topic' kind
        get_multi_patch.side_effect = [children] + grand_children_topic
        t = topic.Topic(entity=topic_entity)
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(t.children[0].__class__.__name__, 'Topic')
        self.assertEqual(t.children[1].__class__.__name__, 'Topic')
        self.assertIsNotNone(info)
        self.assertListEqual(sorted(list(info.keys())), LEADING_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 2)
        for idx, i in enumerate(info['child']):
            self.assertEqual(sorted(list(i.keys())), CHILDREN_TOPIC_PROPS)
            self.assertEqual(len(i['child']), min(num_grand_child_topic[idx], 6))

    def test_init_by_entity_cool_english_topic(self):
        """
        模擬 酷英 三年級 知識點頁
        """
        topic_entity, _, _ = mock_entity.get_topic_entity()
        topic_entity['id'] = 'ce-bl-grade-3'
        t = topic.Topic(entity=topic_entity)
        t.content_factory = get_content_factory()
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertTrue(t.contains_teaching_material)
        for i in range(12):
            self.assertEqual(t.children[i].__class__.__name__, 'Iframe')
        self.assertListEqual(sorted(list(info.keys())), CONTENT_TOPIC_PROPS)
        self.assertEqual(len(info['child']), 16)

    @patch('content.internal.internal.get_edit_version')
    @patch('content.internal.topic._get_by_id')
    def test_init_by_topic_id_without_edit_version(self, get_topic_patch, get_edit_version_patch):
        """
        模擬在沒有 edit version 的時候，透過 topic_id 取到一個 Topic
        在 publish 過程中，會有一小段時間沒有 edit version/edit tree
        在那段期間內也要能夠正常取得 default version 的 topic data
        """
        get_topic_patch.return_value, _, _ = \
            mock_entity.get_topic_entity(is_live=False)
        get_edit_version_patch.side_effect = internal.InvalidContentTree
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.get_logo_data = Mock()
        t.get_logo_data.return_value = None
        t.content_factory = get_content_factory()
        self.assertFalse(t.is_editable)
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertIsNotNone(info)

    parent_id = 'math'
    new_id = 'new_topic_id'
    new_title = 'New Topic Title'
    new_desc = 'description of new topic'
    update_id = 'test_update_topic'
    update_title = 'Test Update Topic Title'
    update_desc = 'a fake topic to test update function'
    created_topic_ids = []

    def setUp(self) -> None:
        super().setUp()
        self.created_topic_ids = []

    def tearDown(self):
        super().tearDown()
        # revert 建出的 topics
        for topic_id in self.created_topic_ids:
            e = topic._get_by_id(topic_id, internal.get_edit_version())
            if e:
                client = internal.get_client()
                parents = client.get_multi(e['parent_keys'])
                for parent in parents:
                    parent['child_keys'].remove(e.key)
                    client.put(parent)
                client.delete(e.key)

    def _update_topic_ok(self, topic_id, new_topic_id, new_topic_title, new_topic_description):
        updated_topic = topic.update_topic(False, topic_id,
                                           id=new_topic_id,
                                           title=new_topic_title,
                                           standalone_title=new_topic_title,
                                           description=new_topic_description)
        self.assertEqual(updated_topic['id'], new_topic_id)
        self.assertEqual(updated_topic['title'], new_topic_title)
        self.assertEqual(updated_topic['description'], new_topic_description)

    @patch('google.cloud.datastore.Client.put')
    def test_update_topic_ok(self, put_patch):
        # validation only
        updated_topic = topic.update_topic(True, self.update_id)
        self.assertIsNone(updated_topic)
        self.assertFalse(put_patch.called)
        # not changed title and desc
        self._update_topic_ok(self.update_id, self.update_id,
                              self.update_title, self.update_desc)
        self.assertFalse(put_patch.called)
        # changed title and desc
        self._update_topic_ok(self.update_id, self.update_id + 'x',
                              self.update_title + 'x', self.update_desc + 'x')
        self.assertTrue(put_patch.called)

    def test_update_topic_fail_topic_id(self):
        # 測試不存在 topic
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '這個代號的資料夾不存在 x',
                               topic.update_topic, True, 'x')
        # 測試已存在 id
        self.assertRaisesRegex(content.exception.TopicExistsError, '已存在代號是 math 的資料夾，該資料夾的標題是',
                               topic.update_topic, False, self.update_id, id='math')
        # 測試不合法 id
        self.assertRaisesRegex(content.exception.InvalidFormat, '代號 id\+ 存在不合法字元',
                               topic.update_topic, False, self.update_id, id='id+')

    @patch('content.internal.topic.get_all_topics')
    def test_get_content_topics_ok(self, get_all_topics):
        topic_entity, _, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=1, num_child_each_content=1, child_is_live=[True])
        get_all_topics.return_value = [topic_entity]
        content_topics = topic.get_content_topics(350)
        self.assertTrue(get_all_topics.called)
        self.assertEqual(len(content_topics), 1)
        self.assertEqual(len(content_topics[0]['child_keys']), 6)

    @patch('content.internal.topic.get_all_topics')
    def test_get_content_topics_all_topic_children(self, get_all_topics):
        topic_entity, _, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=2, child_is_live=[True, False])
        get_all_topics.return_value = [topic_entity]
        content_topics = topic.get_content_topics(350)
        self.assertTrue(get_all_topics.called)
        self.assertEqual(len(content_topics), 0)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic.get_content_topics')
    def test_get_filled_content_topics_one_kind(self, get_content_topics,
                                                get_children_patch):
        kind = 'Article'
        topic_entity, children, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=2, num_child_each_content=2,
                child_is_live=[True, False])
        get_children_patch.return_value = \
            [child for child in children if child.kind == kind]
        get_content_topics.return_value = [topic_entity]
        result = topic.get_filled_content_topics([kind], version=350)
        self.assertTrue(get_content_topics.called)
        self.assertTrue(get_children_patch.called)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].id, topic_entity['id'])
        self.assertEqual(len(result[0].children), 2)
        for child in result[0].children:
            self.assertEqual(child.kind, kind)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic.get_content_topics')
    def test_get_filled_content_topics_no_kind_assigned(
            self, get_content_topics, get_children_patch):
        topic_entity, children, _ = \
            mock_entity.get_topic_entity(
                num_child_topic=2, num_child_each_content=1,
                child_is_live=[True])
        get_children_patch.return_value = \
            [child for child in children if child.kind != 'Topic']
        get_content_topics.return_value = [topic_entity]
        result = topic.get_filled_content_topics(version=350)
        self.assertTrue(get_content_topics.called)
        self.assertTrue(get_children_patch.called)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].id, topic_entity['id'])
        self.assertEqual(len(result[0].children), 5)
        for child in result[0].children:
            self.assertNotEqual(child.kind, 'Topic')

    def _create_topic_ok(self, new_topic_title, parent_id, new_topic_id, new_topic_description):
        created_topic = topic.create_topic(new_topic_title, parent_id, new_topic_id,
                                           standalone_title=new_topic_title,
                                           description=new_topic_description)
        self.assertEqual(created_topic['title'], new_topic_title)
        if new_topic_id:
            self.assertEqual(created_topic['id'], new_topic_id)
        else:
            self.assertIsNotNone(created_topic['id'])
            new_topic_id = created_topic['id']
        self.assertEqual(created_topic['description'], new_topic_description)
        self.assertEqual(created_topic['version'], internal.get_edit_version().key)
        # check parent
        client = internal.get_client()
        parent_topic_key = created_topic['parent_keys'][0]
        parent_topic = client.get(parent_topic_key)
        child_keys = parent_topic['child_keys']
        child_ids = [t['id'] for t in client.get_multi(child_keys)]
        self.assertIn(new_topic_id, child_ids)
        self.assertEqual(created_topic['extended_slug'], '%s/%s' % (parent_topic['extended_slug'], new_topic_id))
        return new_topic_id

    def test_create_topic_ok(self):
        # happy path
        topic_id = self._create_topic_ok(self.new_title, self.parent_id, self.new_id, self.new_desc)
        self.created_topic_ids.append(topic_id)
        # get new topic id
        topic_id = self._create_topic_ok(self.new_title, self.parent_id, None, self.new_desc)
        self.created_topic_ids.append(topic_id)
        # create first child (the just created topic `topic_id` is empty)
        topic_id = self._create_topic_ok(self.new_title, topic_id, None, self.new_desc)
        self.created_topic_ids.append(topic_id)

    def test_create_topic_fail_topic_id(self):
        # 測試不存在 parent
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '預計擺放的母資料夾 \[None] 不存在',
                               topic.create_topic, self.new_title, None, self.new_id)
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '預計擺放的母資料夾 \[] 不存在',
                               topic.create_topic, self.new_title, '', self.new_id)
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '預計擺放的母資料夾 \[x] 不存在',
                               topic.create_topic, self.new_title, 'x', self.new_id)
        # 測試已存在 id
        self.assertRaisesRegex(content.exception.TopicExistsError, '已存在代號是 math 的資料夾，該資料夾的標題是',
                               topic.create_topic, self.new_title, self.parent_id, 'math')
        # 測試不合法 id
        self.assertRaisesRegex(content.exception.InvalidFormat, '代號 id\+ 存在不合法字元',
                               topic.create_topic, self.new_title, self.parent_id, 'id+')

    @patch('content.internal.topic.get_key')
    def test_create_topic_fail_key(self, get_key):
        get_key.return_value = topic.get_root_key(version=internal.get_default_version())
        # 測試 new key name 碰撞
        self.assertRaisesRegex(content.exception.TopicKeyExistsError,
                               'auto generated random key name .* already exists. try it again.',
                               topic.create_topic, self.new_title, self.parent_id, self.new_id)

    @patch('content.internal.internal.get_default_version')
    def test_get_breadcrumb_data(self, get_default_version):
        normal_user_selector = lambda c: c.is_live
        super_user_selector = lambda c: True
        get_default_version.return_value = internal.get_version(version_id=350)
        under_hidden_parent_topic = topic.Topic.from_id(topic_id='khan-fractions')
        self.assertListEqual([], under_hidden_parent_topic.get_breadcrumb_data(normal_user_selector))
        self.assertListEqual([{'topic_id': 'khan-videos', 'title': 'khan videos', 'tags': []}],
                             under_hidden_parent_topic.get_breadcrumb_data(super_user_selector))

        root = topic.Topic.from_id(topic_id='root')
        self.assertListEqual(root.get_breadcrumb_data(super_user_selector), [])
        self.assertListEqual(root.get_breadcrumb_data(normal_user_selector), [])

    @patch('content.internal.internal.get_default_version')
    def test_get_logo_data(self, get_default_version):
        get_default_version.return_value = internal.get_version(version_id=350)
        t = topic.Topic.from_id(topic_id='junyi-addition-and-subtraction')
        self.assertEqual(t.get_logo_data(lambda c: c.is_live), '/images/logo_large.png')
        t = topic.Topic.from_id(topic_id='khan-fractions')
        self.assertIsNone(t.get_logo_data(lambda c: c.is_live))

    def test_get_topic_from_id(self):
        client = internal.get_client()

        # get default version topic
        t = topic.Topic.from_id(topic_id='math')
        topicVersion = client.get(t._entity['version'])
        self.assertFalse(topicVersion['edit'])
        self.assertTrue(topicVersion['default'])

        # get edit version topic (normal user should get error)
        self.assertRaisesRegex(PermissionError, 'Only moderators are allowed to get edit tree.',
                               topic.Topic.from_id, topic_id='math', version_id='edit')

        # get edit version topic (moderator)
        user = self.create_user(moderator=True)
        t = topic.Topic.from_id(topic_id='math', version_id='edit', current_user=user)
        topicVersion = client.get(t._entity['version'])
        self.assertTrue(topicVersion['edit'])
        self.assertFalse(topicVersion['default'])

        # get other version topic
        t = topic.Topic.from_id(topic_id='math', version_id='x')
        topicVersion = client.get(t._entity['version'])
        self.assertFalse(topicVersion['edit'])
        self.assertTrue(topicVersion['default'])

    def create_user(self, moderator=False):
        from testutil.authutil import create_jaid
        from auth.user_util import User
        jaid = create_jaid(moderator=moderator)
        return User(jaid)

    def test_get_children_entities_of_kind(self):
        e = topic._get_by_id('basic-exponents', internal.get_edit_version())
        parent_topic = topic.Topic(entity=e)
        video_entities_list = parent_topic.get_children_entities_of_kind('Video')
        self.assertNotEqual(0, len(video_entities_list))
        for video_entity in video_entities_list:
            self.assertEqual(video_entity.kind, 'Video')


class TestTopicUtil(unittest.TestCase):
    @patch.object(datastore.Query, 'fetch')
    def test_get_by_id_no_result(self, fetch_patch):
        fetch_patch.return_value = []
        self.assertIsNone(topic._get_by_id('123', mock_entity.get_topic_version_entity()))

    @patch('content.internal.topic.fetch_content_changes')
    def test_vcc_apply_ok(self, fetch_content_changes_patch):
        ori_article_entity = mock_entity.get_article_entity()
        new_article_entity = copy.copy(ori_article_entity)
        version = mock_entity.get_topic_version_entity()
        article_vcc_entity = mock_entity.get_content_change_entity(
            version.key, ori_article_entity.key, ['name'])
        fetch_content_changes_patch.return_value = {ori_article_entity.key: article_vcc_entity}

        topic.apply_content_change([mock_entity.get_exercise_entity(),
                                    new_article_entity,
                                    mock_entity.get_video_entity()])
        changes = pickle.loads(article_vcc_entity['content_changes'])
        self.assertEqual(new_article_entity['name'], changes['name'])

    @patch('content.internal.topic._get_by_id')
    def test_topic_add_child(self, get_topic_patch):
        version = internal.get_default_version()
        mock_topic, _, _ = mock_entity.get_topic_entity()
        mock_child, _, _ = mock_entity.get_topic_entity()
        get_topic_patch.return_value = mock_topic

        mock_topic['child_keys'] = []
        ret = topic.add_child(mock_child, mock_topic['id'], version)
        self.assertEqual(mock_topic['child_keys'], [mock_child.key])

        del mock_topic['child_keys']
        ret = topic.add_child(mock_child, mock_topic['id'], version)
        self.assertEqual(mock_topic['child_keys'], [mock_child.key])
