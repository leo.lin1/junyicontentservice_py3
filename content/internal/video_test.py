# -*- coding: utf-8 -*-
import pickle
import unittest
from unittest import mock

from . import internal
from . import mock_entity
from . import video as video_lib


class TestGetForReadableId(unittest.TestCase):

    def test_empty_readable_id(self):
        video = video_lib.get_for_readable_id('')
        self.assertIsNone(video)

    def test_non_exist_readable_id(self):
        video = video_lib.get_for_readable_id('non-exist-readable-id')
        self.assertIsNone(video)

    def test_existed_readable_id(self):
        video = video_lib.get_for_readable_id('the-why-of-the-3-divisibility-rule')
        self.assertIsNotNone(video)

    @mock.patch('content.internal.video._get_by_readable_id')
    def test_multiple_same_readable_id(self, get_by_readable_id_patch):
        obj1 = mock.MagicMock(**{'key.id': 100})
        obj1.__getitem__.side_effect = {'youtube_id': 'non_player_suffix'}.__getitem__
        obj2 = mock.MagicMock(**{'key.id': 200})
        obj2.__getitem__.side_effect = {'youtube_id': 'non_player_suffix'}.__getitem__
        obj3 = mock.MagicMock(**{'key.id': 300})
        obj3.__getitem__.side_effect = {'youtube_id': 'a_player'}.__getitem__
        get_by_readable_id_patch.return_value = [obj1, obj2, obj3]

        video = video_lib.get_for_readable_id('mock')
        self.assertEqual(video, obj2)


class VideoTestSuite(unittest.TestCase):

    def tearDown(self):
        client = internal.get_client()
        entities = video_lib._get_by_readable_id(self.readable_id)
        for entity in entities:
            client.delete(entity.key)

    def check_entity_value(self, video_data):
        compared_keys = ['readable_id', 'title', 'description', 'youtube_id',
                         'duration', 'views', 'url', 'keywords']

        entities = video_lib._get_by_readable_id(self.readable_id)
        self.assertEqual(len(entities), 1)
        video_entity = entities[0]

        version = internal.get_version('edit')
        existing_change = video_lib.get_content_change(video_entity, version)
        if existing_change:
            video_entity.update(pickle.loads(existing_change['content_changes']))

        for key in compared_keys:
            self.assertEqual(video_data[key], video_entity[key])


class TestCreateVideoEntity(VideoTestSuite):

    def test_create_video(self):
        video_data = mock_entity.get_video_entity()
        self.readable_id = video_data['readable_id']
        video_entity = video_lib.create_video(self.readable_id, video_data)

        self.assertIsNotNone(video_entity)
        self.check_entity_value(video_data)


class TestVideo(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entity = mock_entity.get_video_entity()
        self._test_extended_slug = 'math/arithmetic/basic-exponents'
        self._mock_parent_topic = mock.MagicMock()
        self._mock_parent_topic.extended_slug = self._test_extended_slug

    def test_dump_video_data(self):
        test_video = video_lib.Video(entity=self._test_video_entity, parent_topic=self._mock_parent_topic)
        video_data = test_video.dump_video_data()
        self.assertEqual({
            'youtube_id': self._test_video_entity['youtube_id'],
            'url': self._test_video_entity['url'],
            'title': self._test_video_entity['title'],
            'description': self._test_video_entity['description'],
            'keywords': self._test_video_entity['keywords'],
            'duration': self._test_video_entity['duration'],
            'readable_id': self._test_video_entity['readable_id'],
            'video_path': f"//junyivideo2.oss-cn-hangzhou.aliyuncs.com/{self._test_video_entity['youtube_id']}.mp4",
            'canonical_url': f"/{self._test_extended_slug}/v/{self._test_video_entity['readable_id']}",
            'content_rights_object': None,
        }, video_data)

    def test_dump_video_data_with_same_title_description(self):
        fake_tittle = 'same_fake_tittle'
        self._test_video_entity['description'] = fake_tittle
        self._test_video_entity['title'] = fake_tittle
        test_video = video_lib.Video(entity=self._test_video_entity, parent_topic=self._mock_parent_topic)
        video_data = test_video.dump_video_data()
        self.assertEqual({
            'youtube_id': self._test_video_entity['youtube_id'],
            'url': self._test_video_entity['url'],
            'title': fake_tittle,
            'description': None,
            'keywords': self._test_video_entity['keywords'],
            'duration': self._test_video_entity['duration'],
            'readable_id': self._test_video_entity['readable_id'],
            'video_path': f"//junyivideo2.oss-cn-hangzhou.aliyuncs.com/{self._test_video_entity['youtube_id']}.mp4",
            'canonical_url': f"/{self._test_extended_slug}/v/{self._test_video_entity['readable_id']}",
            'content_rights_object': None,
        }, video_data)

    def test_dump_video_data_without_parent(self):
        test_video = video_lib.Video(entity=self._test_video_entity)
        with self.assertRaises(AttributeError):
            test_video.dump_video_data()


class TestVideoSubtitles(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_sub_titles_entity = \
            mock_entity.get_video_subtitles_entity()

    def test_to_description(self):
        test_video_sub_titles = video_lib.VideoSubtitles(
            entity=self._test_video_sub_titles_entity)
        description = test_video_sub_titles.to_description()
        self.assertEqual('fake_json1 fake_json2', description)

    def test_to_description_with_long_text(self):
        long_text = '0123456789' * 10
        self._test_video_sub_titles_entity['json'] = \
            '[{"text": "' + long_text + '"}, {"text": "' + long_text + '"}]'
        test_video_sub_titles = video_lib.VideoSubtitles(
            entity=self._test_video_sub_titles_entity)
        description = test_video_sub_titles.to_description()
        self.assertEqual(' '.join((long_text, long_text))[:150], description)


class TestGetVideoSubtitles(unittest.TestCase):

    def test_empty_key(self):
        video_subtitles = video_lib._get_video_subtitles('')
        self.assertEqual(0, len(video_subtitles))

    def test_non_exist_key(self):
        video_subtitles = video_lib._get_video_subtitles('non-exist-key')
        self.assertEqual(0, len(video_subtitles))

    def test_exist_key(self):
        # There is no video subtitles in test data store
        video_subtitles = video_lib._get_video_subtitles('???')
        self.assertEqual(0, len(video_subtitles))


class TestGetVideoSubtitlesDescription(unittest.TestCase):

    @mock.patch.object(video_lib.VideoSubtitles, 'to_description',
                       return_value='fake_description')
    @mock.patch('content.internal.video._get_video_subtitles')
    def test_has_zh_tw_description(self, get_video_subtitles_patch, mock_to_description):
        get_video_subtitles_patch.return_value = mock_to_description

        description = video_lib._get_video_subtitles_description('fake_id')
        get_video_subtitles_patch.assert_called_once_with('zh-TW:fake_id')
        self.assertEqual('fake_description', description)

    @mock.patch.object(video_lib.VideoSubtitles, 'to_description',
                       return_value='fake_description')
    @mock.patch('content.internal.video._get_video_subtitles')
    def test_no_zh_tw_description(self, get_video_subtitles_patch, mock_to_description):
        get_video_subtitles_patch.side_effect = ([], mock_to_description)

        description = video_lib._get_video_subtitles_description('fake_id')
        get_video_subtitles_patch.assert_called_with('en:fake_id')
        self.assertEqual('fake_description', description)

    @mock.patch('content.internal.video._get_video_subtitles')
    def test_no_description(self, get_video_subtitles_patch):
        get_video_subtitles_patch.side_effect = ([], [])

        description = video_lib._get_video_subtitles_description('fake_id')
        self.assertIsNone(description)


class TestDumpNeighborVideoData(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entity = mock_entity.get_video_entity()

    def test_dump_data(self):
        video_data = video_lib._dump_neighbor_video_data(self._test_video_entity)
        self.assertEqual({
            "key_id": self._test_video_entity.key.id,
            "readable_id": self._test_video_entity['readable_id'],
            "title": self._test_video_entity['title'],
        }, video_data)


class TestGetPlayData(unittest.TestCase):

    def setUp(self) -> None:
        self._test_video_entities_list = [
            mock_entity.get_video_entity(),
            mock_entity.get_video_entity(),
            mock_entity.get_video_entity(),
        ]
        self._mock_parent_topic = mock.MagicMock(**{
            'extended_slug': 'mock_slug'
        })

    def test_no_match(self):
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            'not_exist_readable_id')
        self.assertIsNone(play_data)
        self.assertIsNone(video)

    def _assert_video_and_play_data(self,
        matched_video_entity,
        play_data,
        video,
        previous_video_entity=None,
        next_video_entity=None):
        self.assertEqual(video.selected, 'selected')
        self.assertEqual(video.readable_id,
                         matched_video_entity['readable_id'])
        self.assertEqual({
            'previous_video': {
                'key_id': previous_video_entity.key.id,
                'readable_id': previous_video_entity['readable_id'],
                'title': previous_video_entity['title'],
            } if previous_video_entity else None,
            'next_video': {
                'key_id': next_video_entity.key.id,
                'readable_id': next_video_entity['readable_id'],
                'title': next_video_entity['title'],
            } if next_video_entity else None,
            'key':
                matched_video_entity.key.id,
            'youtube_id':
                matched_video_entity['youtube_id'],
            'url':
                matched_video_entity['url'],
            'title':
                matched_video_entity['title'],
            'description':
                matched_video_entity['description'],
            'keywords':
                matched_video_entity['keywords'],
            'duration':
                matched_video_entity['duration'],
            'readable_id':
                matched_video_entity['readable_id'],
            'video_path':
                f"//junyivideo2.oss-cn-hangzhou.aliyuncs.com/{matched_video_entity['youtube_id']}.mp4",
            'canonical_url':
                f"/mock_slug/v/{matched_video_entity['readable_id']}",
            'long_description': None,
            'related_exercises': [],
            'selected_nav_link': 'watch',
            'issue_labels':
                f"Component-Videos,Video-{matched_video_entity['readable_id']}",
            'author_profile':
                'https://plus.google.com/103970106103092409324',
            'content_rights_object': None,
        }, play_data)

    def test_match_first(self):
        matched_video_entity = self._test_video_entities_list[0]
        next_video_entity = self._test_video_entities_list[1]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         next_video_entity=next_video_entity)

    def test_match_middle(self):
        matched_video_entity = self._test_video_entities_list[1]
        prv_video_entity = self._test_video_entities_list[0]
        next_video_entity = self._test_video_entities_list[2]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         previous_video_entity=prv_video_entity,
                                         next_video_entity=next_video_entity)

    def test_match_last(self):
        matched_video_entity = self._test_video_entities_list[2]
        prv_video_entity = self._test_video_entities_list[1]
        play_data, video = video_lib.get_play_data(
            self._mock_parent_topic, self._test_video_entities_list,
            matched_video_entity['readable_id'])

        self._assert_video_and_play_data(matched_video_entity, play_data, video,
                                         previous_video_entity=prv_video_entity)