# -*- coding: utf-8 -*-
import copy
import datetime
import json
import logging
import pickle
import urllib.parse

from google.cloud import datastore

from . import internal
from .content_prototype import Content


class Video(Content):
    youtube_id: str
    url: str
    title: str
    description: str
    keywords: str
    duration: int
    readable_id: str
    content_rights_object: dict

    _property_from_entity = ['youtube_id', 'url', 'title', 'description', 'keywords', 'duration', 'readable_id']

    @property
    def is_live(self):
        return True

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def progress_id(self):
        return 'v' + str(self._entity.key.id)

    @property
    def canonical_url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return "/%s/v/%s" % (self._parent_topic.extended_slug, urllib.parse.quote(self.readable_id))

    @property
    def content_rights_object(self):
        try:
            return json.loads(
                self._entity['content_rights_object']
            )
        except Exception:
            pass
        return None

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.canonical_url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'description': self.description,
            'progress_id': self.progress_id,
            'id': self.readable_id,
            'is_content': self.is_teaching_material,
            'duration': self.duration,
            'duration_format': "%d:%02d" % (self.duration / 60, self.duration % 60)
        }

    def dump(self):
        return {
            'kind': self.__class__.__name__,
            'id': self.readable_id,
            'title': self.title,
        }

    def dump_video_data(self):
        video_data = {}
        for attr_name in self._property_from_entity:
          video_data[attr_name] = getattr(self, attr_name)
        if self.description == self.title:
          video_data['description'] = None
        video_data.update({
            'video_path': f'//junyivideo2.oss-cn-hangzhou.aliyuncs.com/{self.youtube_id}.mp4',
            'canonical_url': self.canonical_url,
            'content_rights_object': self.content_rights_object,
        })
        return video_data


class VideoSubtitles(object):
    """Subtitles for a YouTube video

    This is a cache of the content from Universal Subtitles for a video. A job
    runs periodically to keep these up-to-date.

    Store with a key name of "LANG:YOUTUBEID", e.g., "en:9Ek61w1LxSc".
    """
    modified: str
    youtube_id: str
    language: str
    json: str

    _property_from_entity = ['modified', 'youtube_id', 'language', 'json']

    def __init__(self, entity):
        self._entity = entity
        for prop in self._property_from_entity:
            setattr(self, prop, self._entity.get(prop))

    def to_json(self):
        """Return subtitles JSON as a Python object

        If there is an issue parsing the JSON, None is returned.
        """
        try:
            return json.loads(self.json)
        except ValueError:
            logging.warning('VideoSubtitles.parse_json: json decode error')

    def to_description(self):
        subtitles_json = self.to_json()
        if subtitles_json:
            text_list = [s.get('text', '') for s in subtitles_json]
            return ' '.join(text_list)[:150]
        return ''


def get_for_readable_id(readable_id):
    if readable_id == "":
        return None

    video = None
    query_results = _get_by_readable_id(readable_id)
    # The database currently contains multiple Video objects for a particular
    # video.  Some are old.  Some are due to a YouTube sync where the youtube urls
    # changed and our code was producing youtube_ids that ended with '_player'.
    # This hack gets the most recent valid Video object.
    key_id = 0
    for res in query_results:
        if res.key.id > key_id and not res['youtube_id'].endswith('_player'):
            video = res
            key_id = res.key.id
    # End of hack

    return video


def _get_by_readable_id(readable_id):
    datastore_client = internal.get_client()
    query = datastore_client.query(kind='Video')
    query.add_filter('readable_id', '=', readable_id)

    return list(query.fetch())


# Copy from content/internal/exercise.py
# TODO(kerker): Move to a common place
def get_content_change(content, version):
    query = internal.get_query(kind='VersionContentChange', ancestor=version.key)
    query.add_filter('version', '=', version.key)
    query.add_filter('content', '=', content.key)
    return next(iter(query.fetch(1)), None)


# Copy from content/internal/exercise.py
# TODO(kerker): Move to a common place
def add_content_change(content, version, data):
    client = internal.get_client()

    change = get_content_change(content, version)
    diff = pickle.loads(change['content_changes']) if change is not None else {}
    orig_data = copy.copy(dict(content))
    orig_data.update(diff)

    updated = False
    for prop, value in data.items():
        if value is None:
            continue
        if prop in orig_data and orig_data[prop] == value:
            continue
        diff[prop] = value
        updated = True

    # only put the change if we have actually changed any props
    if updated:
        if change is None:
            change = datastore.Entity(
                key=client.key('VersionContentChange', parent=version.key),
                exclude_from_indexes=('content_changes',)
            )
            change['version'] = version.key
            change['content'] = content.key
        change['content_changes'] = pickle.dumps(diff, protocol=2)
        change['updated_on'] = datetime.datetime.utcnow()
        change['last_edited_by'] = None  # FIXME: current user
        client.put(change)


def create_video(readable_id=None, video_data=None):
    new_video_args = {
        'access_control': '',
        'backup_timestamp': datetime.datetime.utcnow(),
        'date_added': datetime.datetime.utcnow(),
        'description': video_data['description'],
        'duration': video_data['duration'],
        'extra_properties': pickle.dumps(video_data.get('extra_properties', {}), protocol=2),
        'keywords': video_data['keywords'],
        'readable_id': readable_id,
        'title': video_data['title'],
        'topic_string_keys': '',
        'url': video_data['url'],
        'views': video_data['views'],
        'youtube_id': video_data['youtube_id']
    }

    with internal.get_client().transaction():
        new_video_entity = _create_video_txn(new_video_args)

    return new_video_entity


EXCLUDE_FROM_INDEXES = (
    'description',
    'extra_properties',
    'topic_string_keys',
)


def _create_video_txn(new_video_args):
    client = internal.get_client()
    new_video_entity = datastore.Entity(key=client.key('Video'),
                                        exclude_from_indexes=EXCLUDE_FROM_INDEXES)
    new_video_entity.update(new_video_args)

    client.put(new_video_entity)
    return new_video_entity


def _get_video_subtitles(key_name):
    datastore_client = internal.get_client()
    query = datastore_client.query(kind='VideoSubtitles')
    query.add_filter('key', '=', key_name)
    return list(query.fetch())


def _get_video_subtitles_description(youtube_id):
    for lang in ('zh-TW', 'en'):
        subtitle = _get_video_subtitles(f'{lang}:{youtube_id}')
        if subtitle:
            return VideoSubtitles(entity=subtitle[0]).to_description()
    return None


def _dump_neighbor_video_data(neighbor_video_entity):
    neighbor_video = Video(entity=neighbor_video_entity)
    return {
        "key_id": neighbor_video_entity.key.id,
        "readable_id": neighbor_video.readable_id,
        "title": neighbor_video.title
    }


def get_play_data(parent_topic, video_entities_list, readable_id):
    ret_play_data = {
        'previous_video': None,
        'next_video': None,
    }

    for idx, video_entity in enumerate(video_entities_list):
        video = Video(entity=video_entity, parent_topic=parent_topic)
        if video.readable_id == readable_id:
            this_video = video
            this_video.selected = 'selected'
            ret_play_data['key'] = video_entity.key.id
            ret_play_data.update(this_video.dump_video_data())
            if idx != 0:
                previous_video_entity = video_entities_list[idx - 1]
                ret_play_data['previous_video'] = _dump_neighbor_video_data(previous_video_entity)
            if idx + 1 != len(video_entities_list):
                next_video_entity = video_entities_list[idx + 1]
                ret_play_data['next_video'] = _dump_neighbor_video_data(next_video_entity)
            break
    else:
        return None, None

    ret_play_data['long_description'] = _get_video_subtitles_description(this_video.youtube_id)
    if ret_play_data['description'] and ret_play_data['long_description']:
        ret_play_data['long_description'] = ' '.join((
            ret_play_data['description'],
            ret_play_data['long_description'],
        ))

    # related_exercises = this_video.related_exercises()
    # button_top_exercise = None
    # if related_exercises:
    #     def ex_to_dict(exercise):
    #         return {
    #             'name': exercise.display_name,
    #             'url': exercise.relative_url,
    #         }
    #     button_top_exercise = ex_to_dict(related_exercises[0])
    # This is ugly; we would rather have these templates client-side.
    # subtitles_html = shared_jinja.get().render_template('videosubtitles.html',
    #                                                     subtitles_json=subtitles_json)
    # # check this video had been removed or not
    # video_first_topic = this_video.first_topic()
    # if video_first_topic is None:
    #     raise MissingVideoException("This video '%s' had been removed, sorry!" %
    #                                 readable_id)

    ret_play_data.update({
        # extra_properties not exist now
        # 'extra_properties': this_video.extra_properties or {},
        # 'button_top_exercise': button_top_exercise,
        # 'subtitles_html': subtitles_html,
        'related_exercises': [],  # disabled for now
        'selected_nav_link': 'watch',
        'issue_labels': f'Component-Videos,Video-{readable_id}',
        'author_profile': 'https://plus.google.com/103970106103092409324',
    })
    return ret_play_data, this_video
