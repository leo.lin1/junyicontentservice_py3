# -*- coding: utf-8 -*-
import datetime
import json
import pickle
import random
from random import shuffle

from google.cloud import datastore

from . import internal


def _get_content_children(num_child_each_content, child_is_live=None):
    children = []
    if num_child_each_content == 0:
        return children
    if child_is_live and len(child_is_live) > num_child_each_content:
        raise ValueError('len(child_is_live)[%d] should not be larger than num_child_each_content [%d]' %
                         (len(child_is_live), num_child_each_content))
    if child_is_live is None:
        child_is_live = [True]
    for i in range(num_child_each_content):
        is_live = child_is_live[i % len(child_is_live)]
        children.append(get_exam_entity(is_live))
        children.append(get_article_entity(is_live))
        children.append(get_url_entity())
        children.append(get_exercise_entity(is_live))
        children.append(get_video_entity())

    return children


def get_topic_entity(is_live=True,
                     num_child_topic=0,
                     num_child_each_content=0,
                     child_is_live=None,
                     num_grand_child_topic=None,
                     num_grand_child_each_content=0,
                     ):
    ret, _ = _get_topic_entity(is_live)
    children = []
    grand_children_list = []
    if child_is_live and num_child_topic != 0 and len(child_is_live) > num_child_topic:
        raise ValueError('len(child_is_live)[%d] should not be larger than num_child_topic [%d]' %
                         (len(child_is_live), num_child_topic))
    for i in range(num_child_topic):
        is_live = child_is_live[i % len(child_is_live)]
        child_num_child_topic = num_grand_child_topic[i % len(num_grand_child_topic)] if num_grand_child_topic else 0
        child, grand_children = _get_topic_entity(
            is_live, num_child_topic=child_num_child_topic, num_child_each_content=num_grand_child_each_content)
        children.append(child)
        if is_live:
            grand_children_list.append(grand_children)

    children.extend(_get_content_children(num_child_each_content, child_is_live))

    ret['child_keys'] = [child.key for child in children]
    shuffle(children)
    return ret, children, grand_children_list


def _get_topic_entity(is_live=True, num_child_topic=0, num_child_each_content=0):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Topic', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['title', 'standalone_title', 'id', 'extended_slug', 'description']:
        entity[item] = item + '_' + unique_suffix
    entity['tags'] = []
    entity['hide'] = not is_live
    entity['version'] = internal.get_default_version().key

    children = []
    for i in range(num_child_topic):
        child, _ = _get_topic_entity(True)
        children.append(child)
    children.extend(_get_content_children(num_child_each_content))
    entity['child_keys'] = [child.key for child in children]
    shuffle(children)

    return entity, children


def get_section_entity(num_child_each_content, child_is_live=None):
    ret = _get_section_entity()
    children = _get_content_children(num_child_each_content, child_is_live)
    ret['child_keys'] = [child.key for child in children]
    shuffle(children)
    return ret, children


def _get_section_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Topic', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['title', 'standalone_title', 'id', 'extended_slug']:
        entity[item] = item + '_' + unique_suffix
    entity['child_keys'] = []
    return entity


def get_exam_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Exam', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    entity['name'] = 'name_' + unique_suffix
    entity['content_list'] = ['dummy_content1', 'dummy_content2']
    entity['public'] = is_live
    entity['tags'] = []
    entity['large_screen_only'] = True
    return entity


def get_article_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Article', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    entity['name'] = 'name_' + unique_suffix
    entity['content_list'] = ['dummy_content1', 'dummy_content2']
    entity['public'] = is_live
    entity['tags'] = []
    entity['large_screen_only'] = True
    return entity


def get_url_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Url', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['url', 'title', 'description']:
        entity[item] = item + '_' + unique_suffix
    return entity


def get_exercise_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Exercise', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    # Matching the default properties in content.internal.create_exercise().
    for item in ['name', 'pretty_display_name', 'description']:
        entity[item] = item + '_' + unique_suffix
    entity.update({
        'cover_range': json.dumps([[entity['name']]]),
        'covers': [],
        'h_position': 0,
        'is_quiz_exercise': True,
        'large_screen_only': True,
        'live': is_live,
        'prerequisites': [],
        'seconds_per_fast_problem': 0.0,
        'short_display_name': entity['pretty_display_name'][:11],
        'tags': [],
        'v_position': 0,
    })
    return entity


def get_video_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Video', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['youtube_id', 'url', 'title', 'description', 'keywords', 'readable_id']:
        entity[item] = item + '_' + unique_suffix
    entity['duration'] = 100
    entity['views'] = 666
    return entity


def get_video_subtitles_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('VideoSubtitles', 'en:9Ek61w1LxSc', project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['modified', 'youtube_id', 'language']:
        entity[item] = item + '_' + unique_suffix
    entity['json'] = '[{"text": "fake_json1"}, {"text": "fake_json2"}]'
    return entity


def get_topic_version_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('TopicVersion', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    return datastore.Entity(key)


def get_content_change_entity(version_key, content_key, changed_props):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('VersionContentChange', unique_suffix, parent=version_key)
    entity = datastore.Entity(key)
    entity['version'] = version_key
    entity['content'] = content_key
    changes = {prop: prop + '_' + unique_suffix for prop in changed_props}
    entity['content_changes'] = pickle.dumps(changes, protocol=2)
    entity['updated_on'] = datetime.datetime.utcnow()
    entity['last_edited_by'] = None
    return entity
