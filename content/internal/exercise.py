# -*- coding: utf-8 -*-
import copy
import datetime
import json
import pickle
import re
import urllib.parse

from google.cloud import datastore

from content import exception
from content.internal import internal, topic
from content.internal.content_prototype import Content


EXCLUDE_FROM_INDEXES = (
    'description',
)


class Exercise(Content):
    name: str
    pretty_display_name: str
    live: bool
    description: str
    is_quiz_exercise: bool
    large_screen_only: bool
    _property_from_entity = ['name', 'pretty_display_name', 'live', 'description', 'is_quiz_exercise',
                             'large_screen_only']

    @property
    def is_live(self):
        return self.live

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        pretty_display_name = self.pretty_display_name
        if not pretty_display_name:
            import logging
            logging.error("exercise has no pretty_display_name. name: %s", self.name)
            # fallback to use its name (id)
            pretty_display_name = self.name
        if self.is_live:
            return pretty_display_name
        return pretty_display_name + ' [hidden]'

    @property
    def progress_id(self):
        return self.name

    @property
    def canonical_url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return "/%s/e/%s" % (self._parent_topic.extended_slug, urllib.parse.quote(self.name))

    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.canonical_url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'description': self.description,
            'progress_id': self.progress_id,
            'id': self.name,
            'is_content': self.is_teaching_material,
            'is_quiz_exercise': self.is_quiz_exercise,
            'large_screen_only': self.large_screen_only
        }

    def dump(self):
        return {
            'kind': self.__class__.__name__,
            'id': self.name,
            'title': self.pretty_display_name or '',
        }


def get_by_name(exercise_name):
    client = internal.get_client()
    query = client.query(kind='Exercise')
    query.add_filter('name', '=', exercise_name)
    return next(iter(query.fetch(1)), None)


def add_new_exercise(data):
    client = internal.get_client()
    exercise = datastore.Entity(key=client.key('Exercise'), exclude_from_indexes=EXCLUDE_FROM_INDEXES)
    exercise.update(data)
    exercise['backup_timestamp'] = datetime.datetime.utcnow()
    exercise['creation_date'] = datetime.datetime.utcnow()
    client.put(exercise)
    return exercise


# TODO(kamesan): move to a common place to share with video api impl
def get_content_change(content, version):
    query = internal.get_query('VersionContentChange', ancestor=version.key)
    query.add_filter('version', '=', version.key)
    query.add_filter('content', '=', content.key)
    return next(iter(query.fetch(1)), None)


# TODO(kamesan): move to a common place to share with video api impl
def add_content_change(content, version, data):
    client = internal.get_client()

    change = get_content_change(content, version)
    diff = pickle.loads(change['content_changes']) if change is not None else {}
    orig_data = copy.copy(dict(content))
    orig_data.update(diff)

    updated = False
    for prop, value in data.items():
        if value is None:
            continue
        if prop in orig_data and orig_data[prop] == value:
            continue
        diff[prop] = value
        updated = True

    # only put the change if we have actually changed any props
    if updated:
        if change is None:
            change = datastore.Entity(
                key=client.key('VersionContentChange', parent=version.key),
                exclude_from_indexes=('content_changes',)
            )
            change['version'] = version.key
            change['content'] = content.key
        change['content_changes'] = pickle.dumps(diff, protocol=2)
        change['updated_on'] = datetime.datetime.utcnow()
        change['last_edited_by'] = None  # FIXME: current user
        client.put(change)


def validate_name_format(exercise_name):
    if not exercise_name:
        raise exception.InvalidFormat(exercise_name, "代號不能空白")
    if len(exercise_name) > 500:
        raise exception.InvalidFormat(exercise_name, "代號不能超過 500 個字, %s" % exercise_name)
    if not re.match(r'^[a-z0-9_-]+$', exercise_name):
        raise exception.InvalidFormat(exercise_name, "代號 %s 存在不合法字元" % exercise_name)


def validate_title_format(content_title):
    if not content_title:
        raise exception.InvalidFormat(content_title, "標題不能空白")
    if len(content_title) > 100:
        raise exception.InvalidFormat(content_title, "標題不能超過 100 個字, %s" % content_title)


def create_exercise(exercise_name, content_title, description, parent_id):
    new_exercise_data = {
        'cover_range': json.dumps([[exercise_name]]),
        'covers': [],
        'description': description,
        'h_position': 0,
        'is_quiz_exercise': True,
        'live': True,
        'name': exercise_name,
        'prerequisites': [],
        'pretty_display_name': content_title,
        'seconds_per_fast_problem': 0.0,
        'short_display_name': content_title[:11],
        'tags': [],
        'v_position': 0,
    }

    # Get the root entities here since only ancestor queries are allowed inside
    # transactions.
    client = internal.get_client()
    version = internal.get_edit_version()
    root_key = topic.get_root_key(version)
    existing_exercise = get_by_name(exercise_name)

    with client.transaction():
        exercise, parent_topic = _create_exercise_txn(
                parent_id, version, root_key, existing_exercise, new_exercise_data)

    if existing_exercise is None:
        # Add exercise to parent topic here since the key is incomplete inside
        # the transaction.
        if 'child_keys' not in parent_topic:
            parent_topic['child_keys'] = []
        parent_topic['child_keys'].append(exercise.key)
        client.put(parent_topic)


def update_exercise(exercise_name, content_title, description):
    new_exercise_data = {
        'description': description,
        'pretty_display_name': content_title,
        'short_display_name': content_title[:11],
    }

    exercise = get_by_name(exercise_name)
    if exercise is None:
        raise exception.ExerciseUpdateError("這個代號的知識點不存在")

    client = internal.get_client()
    version = internal.get_edit_version()

    with client.transaction():
        add_content_change(exercise, version, new_exercise_data)


def _create_exercise_txn(parent_id, version, root_key, existing_exercise, new_exercise_data):
    # TODO(kamesan): rename _get_by_id to get_by_id
    parent_topic = topic._get_by_id(parent_id, version, ancestor=root_key)
    if parent_topic is None:
        raise exception.ExerciseCreateError("預計擺放的母資料夾 [%s] 不存在" % parent_id)

    if existing_exercise is None:
        exercise = add_new_exercise(new_exercise_data)
        # Adding the exercise to parent topic needs to defer until the
        # transaction is committed to obtain a complete key.
        return exercise, parent_topic

    # Validate the new data is consistent with the existing exercise.
    exercise = copy.copy(existing_exercise)
    existing_change = get_content_change(existing_exercise, version)
    if existing_change is not None:
        exercise.update(pickle.loads(existing_change['content_changes']))

    if 'child_keys' in parent_topic and exercise.key in parent_topic['child_keys']:
        raise exception.ExerciseCreateError(
                "母資料夾中，已存在相同標題的知識點 [%s]" % exercise['pretty_display_name'])
    if new_exercise_data['pretty_display_name'] != exercise['pretty_display_name']:
        raise exception.ExerciseCreateError(
                "此知識點已存在，且已存在的知識點標題 [%s] 和預計上架的標題不一樣" % exercise['pretty_display_name'])
    if new_exercise_data['cover_range'] != exercise['cover_range']:
        raise exception.ExerciseCreateError(
                "此知識點已存在，且已存在的知識點 cover range [%s] 和預計上架的 cover range 不一樣" % exercise['cover_range'])

    add_content_change(existing_exercise, version, new_exercise_data)

    # Since the exercise exists and has a complete key, we can add it to the
    # parent topic here.
    client = internal.get_client()
    if 'child_keys' not in parent_topic:
        parent_topic['child_keys'] = []
    parent_topic['child_keys'].append(existing_exercise.key)
    client.put(parent_topic)

    return existing_exercise, parent_topic
