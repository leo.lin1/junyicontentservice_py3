# -*- coding: utf-8 -*-
import auth.user_util
import content.exercise
import content.section
import content.topic
import content.video
import instance_cache


def get_topic_page_json_impl(topic_id, version_id=None):
    current_user = auth.user_util.User.current()
    include_hidden = current_user.is_moderator if current_user else False
    if version_id == 'edit':
        # 目前僅先開放 edit version，除此之外都抓 default version
        return content.topic.get_topic_page(topic_id=topic_id, include_hidden=include_hidden,
                                            version_id=version_id, current_user=current_user,
                                            skip_cache=True)
    else:
        return content.topic.get_topic_page(topic_id=topic_id, include_hidden=include_hidden)


def get_topic_list_impl(version_id):
    return content.topic.get_exercise_topics(version_id)


def validate_or_update_topic_by_edu_sheet(topic_id, title, description, validation_only, version_id="edit"):
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本修改 Topic']
        }

    err_response, updated_topic = content.topic.update_topic(topic_id, title, description, validation_only, version_id)
    return {
        'topic_id': topic_id,
        'errors': err_response
    }


def validate_or_create_topic_by_edu_sheet(topic_id, title, description, parent_id, validation_only, version_id="edit"):
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本建立 Topic']
        }

    err_response, created_topic = content.topic.create_topic(topic_id, title, description, parent_id, validation_only, version_id)
    return {
        'topic_id': topic_id,
        'errors': err_response
    }


def validate_or_create_exercise_by_edu_sheet(
        exercise_name, content_title, description, parent_id, validation_only):
    err_response = content.exercise.create_exercise(
        exercise_name, content_title, description, parent_id, validation_only)
    return {
        'content_id': exercise_name,
        'errors': err_response
    }


def validate_or_update_exercise_by_edu_sheet(
        exercise_name, content_title, description, validation_only):
    err_response = content.exercise.update_exercise(
        exercise_name, content_title, description, validation_only)
    return {
        'content_id': exercise_name,
        'errors': err_response
    }


def create_section(parent_id, section_id, title, standalone_title):
    created_section_entity = content.section.create_section(parent_id, section_id, title, standalone_title)
    return {
        'id': created_section_entity['id'],
        'title': created_section_entity['title'],
        'standalone_title': created_section_entity['standalone_title'],
    }


def read_section(version_id, section_id):
    return content.section.read_section(section_id=section_id, version_id=version_id)


def update_section(section_id, new_section_id, new_title, new_standalone_title):
    content.section.update_section(section_id, new_section_id, new_title, new_standalone_title)


def validate_or_create_video_by_edu_sheet(readable_id, content_title, description, parent_id, validation_only=True, version_id="edit"):
    errors = content.video.create_video(readable_id, content_title, description, parent_id, validation_only, version_id)

    return {
        'readable_id': readable_id,
        'errors': errors
    }


def validate_or_update_video_by_edu_sheet(readable_id, content_title, description, validation_only=True, version_id="edit"):
    errors = content.video.update_video(readable_id, content_title, description, validation_only, version_id)

    return {
        'readable_id': readable_id,
        'errors': errors
    }


def video_play_data_impl(topic_id, video_id):
    return {"video": content.video.get_video(topic_id, video_id)}


def flush_instance_cache(category):
    if instance_cache._debug:
        instance_cache.flush(category=category)
        return 'ok'
    return 'fail'


def warm_up():
    auth.user_util.get_cached_developer_list()
    auth.user_util.get_cached_moderator_list()
    return '', 200, {}
