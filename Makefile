.PHONY: run-ds test

# Run datastore emulator
run-ds:
	gcloud beta emulators datastore start --data-dir=/work_resource --host-port=0.0.0.0:8085
	# Then you shoud do
	# $(gcloud beta emulators datastore env-init --data-dir=/work_resource)

# Run tests.  If COVERAGE is set (e.g. COVERAGE=true), run them in coverage mode.  If
# MAX_TEST_SIZE is set, only tests of this size or smaller are run.
MAX_TEST_SIZE = medium
test:
	if test -n "$(COVERAGE)"; then  \
	   coverage3 run tools/runtests.py --max-size="$(MAX_TEST_SIZE)" && \
	   coverage3 report --skip-covered; \
	else \
	   python3 tools/runtests.py --max-size="$(MAX_TEST_SIZE)"; \
	fi
