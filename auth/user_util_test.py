# -*- coding: utf-8 -*-
from testutil import testcase
from unittest.mock import patch
from . import user_util
from flask import Flask
from werkzeug.http import dump_cookie
import pdb  #FIXME
from testutil.authutil import create_jaid


_normal_user_jaid = 'eyJ0eXAiOiAiSldUIiwgImFsZyI6ICJSUzI1NiJ9.eyJpYXQiOiAxNTgwNzA3MzQ2LCAic3ViIjogIkZBS0VfVVNFUl9EQVRBX0tFWSIsICJleHAiOiAxNTgxOTE2OTQ2fQ.hh3DrZahAs8pABp0hURBiLT-qf_8nuVDtQTcSBTmGuGIjhM_1Z-LOXJfRRb2590S2bQkOzCgrxCz_9iRfx2fYR2mFWnp6YVMOz-8j_ynrms8IcsZJfXfI77ggFne8ugvivU6PHnl0TK-Z1R2HsN0cGJZJuEi5HwOwIAbv0_AonO8uZ60v2B-LugkeGmj_dmjRMjHSUzkgvTV-LTW0bjszZEksXsDotXNI1UP10dQAyiJPQ_ztDBwLNbglw9yPmLgG6HjcIi2X4WLif_8GIK8P0YxV1TzJC90VkCU1acsOaisUjyYER2DqX5qyRMaRkAJKCDQj8g4sIYYzmcYC9aGTQ'
_normal_user_jaid_iat = 1580707346
_developer_jaid = 'eyJ0eXAiOiAiSldUIiwgImFsZyI6ICJSUzI1NiJ9.eyJtb2RlcmF0b3IiOiB0cnVlLCAiaWF0IjogMTU4MDc0MDY0MywgInN1YiI6ICJGQUtFX1VTRVJfREFUQV9LRVkiLCAiZXhwIjogMTU4MTk1MDI0MywgImRldmVsb3BlciI6IHRydWV9.C7ydHgULFAyvruYkTI7dTBpWJweoeuYhH9VVC-H1sJ7Lj4aDuuI8gCL-8H-VCbG5bogxk8Q8BIkJIdRuyZGf5kST2vy3KfGdtZIBb8MEU2TLZ8OBJ-JIxPmU2SmgUp01_Uy3d8RxXQkqdMdRl-JupjTPggTIP5obtU5Nf8oPRTBKkT-ou7afBmS6_wU6bsyHGRWVei2rivNI9k_JxAKUNEWSY4iKIvmlKC5btvQAx0QIon8R5oys3nbbMNPIt1Ko9VxrDU9DeCbCFJ40AnnwOqEMWsAhlJnxHuzmpI3n1aLwE5KS2thIqA-ZV7zYySo7LI-sXIEnnju8YlBTLe3-wA'
_developer_jaid_iat = 1580740643

class TestUser(testcase.NoCacheTestCase):
    def setUp(self):
        self.app = Flask(__name__)
        super().setUp()

    def test_valid_developer(self):
        super_user_jaid = create_jaid(developer=True, moderator=True)
        header = dump_cookie(user_util.JA_AUTH_COOKIE_NAME, super_user_jaid)
        with self.app.test_request_context(environ_base={'HTTP_COOKIE': header}):
            user = user_util.User.current()
        self.assertIsNotNone(user)
        self.assertEqual(user._key, 'FAKE_USER_DATA_KEY')
        self.assertTrue(user.is_developer)
        self.assertTrue(user.is_moderator)

    def test_valid_normal_user(self):
        normal_user_jaid = create_jaid()
        header = dump_cookie(user_util.JA_AUTH_COOKIE_NAME, normal_user_jaid)
        with self.app.test_request_context(environ_base={'HTTP_COOKIE': header}):
            user = user_util.User.current()
        self.assertIsNotNone(user)
        self.assertEqual(user._key, 'FAKE_USER_DATA_KEY')
        self.assertFalse(user.is_developer)
        self.assertFalse(user.is_moderator)

    def test_expired_jaid(self):
        expired_jaid = create_jaid(expired=True)
        self.assertRaisesRegex(ValueError, '^Token expired,', user_util.User, expired_jaid)

        header = dump_cookie(user_util.JA_AUTH_COOKIE_NAME, expired_jaid)
        with self.app.test_request_context(environ_base={'HTTP_COOKIE': header}):
            current_user = user_util.User.current()
        self.assertIsNone(current_user)

    def test_future_jaid(self):
        future_jaid = create_jaid(future=True)
        self.assertRaisesRegex(ValueError, '^Token used too early,', user_util.User, future_jaid)

        header = dump_cookie(user_util.JA_AUTH_COOKIE_NAME, future_jaid)
        with self.app.test_request_context(environ_base={'HTTP_COOKIE': header}):
            current_user = user_util.User.current()
        self.assertIsNone(current_user)

    def test_invalid_jaid(self):
        invalid_jaid = create_jaid().decode() + 'xx'
        self.assertRaisesRegex(ValueError, '^Could not verify token signature.', user_util.User, invalid_jaid)
        header = dump_cookie(user_util.JA_AUTH_COOKIE_NAME, invalid_jaid)
        with self.app.test_request_context(environ_base={'HTTP_COOKIE': header}):
            current_user = user_util.User.current()
        self.assertIsNone(current_user)

