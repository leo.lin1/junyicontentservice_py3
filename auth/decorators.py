# -*- coding: utf-8 -*-
from functools import wraps
from flask import abort
from . import user_util

def moderator_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        current_user = user_util.User.current()
        if not current_user:
            abort(401)
        if not current_user.is_moderator:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function
