# -*- coding: utf-8 -*-
import logging
import time
from flask import request
import api_client.fetch
from api import request_cache
import instance_cache
from google.cloud import datastore
from google.auth import jwt


JA_AUTH_COOKIE_NAME = 'JAID'
_LIGHT_AUTH_PUBLIC_KEY = 'api_client/default_public.pem'


class CookieExpired(BaseException):
    pass


class User:
    _key: datastore.Key = None
    _is_developer: bool = None
    _is_moderator: bool = None
    def __init__(self, jaid):
        self._from_jaid(jaid)

    @property
    def is_moderator(self):
        if self.is_developer:
            return True
        if self._is_moderator is not None:
            return self._is_moderator
        try:
            if self._key in get_cached_moderator_list():
                return True
        except ValueError as e:
            logging.error('Get moderator list failed. %r', e)
        return False

    @property
    def is_developer(self):
        if self._is_developer is not None:
            return self._is_developer
        try:
            if self._key in get_cached_developer_list():
                return True
        except ValueError as e:
            logging.error('Get developer list failed. %r', e)
        return False

    def _from_jaid(self, jaid):
        payload = _verify_jaid(jaid)
        self._key = payload['sub']
        self._is_developer = payload['developer'] if 'developer' in payload else False
        self._is_moderator = payload['moderator'] if 'moderator' in payload else False

    @staticmethod
    @request_cache.cache()
    def current():
        jaid = request.cookies.get(JA_AUTH_COOKIE_NAME)
        if not jaid:
            return None
        try:
            user = User(jaid)
        except ValueError as e:
            logging.warning('Invalid JAID [%r]' % e)
            return None
        return user

def _check_expiration(jaid_payload):
    now = time.time()
    if now > jaid_payload['exp']:
        raise CookieExpired()
    if jaid_payload['iat'] > now:
        raise ValueError('Cookie generate timestamp is in the future %s, now is %d' %
                         (jaid_payload['iat'], now))

def _verify_jaid(jaid):
    with open(_LIGHT_AUTH_PUBLIC_KEY) as fd:
        cert = fd.read()
    claims = jwt.decode(jaid, certs=cert)

    _check_expiration(claims)
    return claims


@instance_cache.cache(available_seconds=3600)
def _get_moderator_list():
    return api_client.fetch.fetch('/api/v1/auth/privileged_users/moderator')

def get_cached_moderator_list():
    return [datastore.Key.from_legacy_urlsafe(urlsafe_key) for urlsafe_key in _get_moderator_list()]

@instance_cache.cache(available_seconds=3600)
def _get_developer_list():
    return api_client.fetch.fetch('/api/v1/auth/privileged_users/developer')

def get_cached_developer_list():
    return [datastore.Key.from_legacy_urlsafe(urlsafe_key) for urlsafe_key in _get_developer_list()]
