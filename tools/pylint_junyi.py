"""
Patch Junyi data types to the pylint AST.
This is a pylint plugin. it was included by pylintrc.
"""


import sys

import re
import functools
import astroid
from astroid import scoped_nodes
from astroid import node_classes

class patcher(object):
    def __init__(self, node_type, **kwarg):
        self.node_type = node_type
        self.kwarg = kwarg
    def __call__(self, fn):
        @functools.wraps(fn)
        def wrapper(node):
            location = self.location(node)
            return fn(node, location, **self.kwarg)
        astroid.MANAGER.register_transform(self.node_type, wrapper)
        return wrapper

    @staticmethod
    def location(node, depth=4):
        path, pnode, name = [], node, 'dummy'
        while pnode and name and depth:
            try:
                name = pnode.name
            except AttributeError:
                # some node do not have a name
                name = None
            if name is not None:
                path.insert(0, name)
            pnode = pnode.parent
            depth -= 1
        return '.'.join(path)


@patcher(scoped_nodes.Class,
        LIST_ATTRS=set((
            ('goals.models.Goal', 'objectives'),
            ('exercise_models.Exercise', 'topic_string_keys'),
            ('mission_models.IMission', 'tasks'),
            ('exercise_models.Exercise', 'cover_range'),
        )),
        DICT_LIKE_TYPE_RE=re.compile(r'object_property\.'
            + r'(JsonProperty|JsonStrProperty|ObjectProperty|UnvalidatedObjectProperty)\([^)]*\)'),
        LIST_LIKE_TYPE_RE=re.compile(r'object_property\.'
            + r'TsvProperty\([^)]*\)')
        )
def patch_db_model_dynamic_objects(node, location,
        LIST_ATTRS, DICT_LIKE_TYPE_RE, LIST_LIKE_TYPE_RE):
    """ replace dynamic types to dict or list

        * if attr described in TYPE_HINT_MAP, use the specified type
        * JsonProperty, ObjectProperty, UnvalidatedObjectProperty => dict
        * TsvProperty => list
    """
    for attr, value in node.locals.items():
        if len(value) != 1:
            continue
        if not isinstance(value[0], node_classes.AssignName):
            continue

        try:
            assign_value = value[0].assign_type().value.as_string()
        except AttributeError:
            continue

        if (location, attr) in LIST_ATTRS:
            attr_cls = scoped_nodes.ListComp
        elif DICT_LIKE_TYPE_RE.match(assign_value):
            attr_cls = scoped_nodes.DictComp
        elif LIST_LIKE_TYPE_RE.match(assign_value):
            attr_cls = scoped_nodes.ListComp
        else:
            continue

        node.locals[attr] = [attr_cls(attr, None)]


@patcher(scoped_nodes.Class)
def patch_reference_attributes(node, location):
    """ patch reference attributes in db.Model classes """
    ADD_ATTR_MAP = {
        ('user_models.UserData'): [
            ('customized_nickname_targeted_by_owner', 'user_models.CustomizedNickname'),
            ('userresidencehistory_set', 'user_models.UserResidenceHistory'),
            ('userstatehistory_set', 'user_models.UserStateHistory'),
            ('educationlevel_set', 'user_models.EducationLevel'),
        ],
    }
    if location in ADD_ATTR_MAP:
        for attr, value in ADD_ATTR_MAP[location]:
            node.locals[attr] = [astroid.extract_node(value)]


@patcher(scoped_nodes.FunctionDef)
def add_bust_cache_arg(node, location):
    """ add arg bust_cache to layer_cache or reqeust_cache decorator """
    try:
        decorate_name = node.decorators.as_string()
    except AttributeError:
        return

    if '@layer_cache.' in decorate_name or '@request_cache.' in decorate_name:
        args, lineno = node.args, node.lineno
        args.args.append(node_classes.AssignName('bust_cache',
                    lineno=lineno, col_offset=1, parent=args))
        args.defaults.append(node_classes.Const(False,
                    lineno=lineno, col_offset=1, parent=args))


@patcher(scoped_nodes.Module)
def patch_raven_client(node, location):
    if location == 'third_party.raven' and 'Client' not in node.locals:
        node.locals['Client'] = [astroid.extract_node('third_party.raven.base.Client')]


def register(_linter):
    """pylint plugin register"""
