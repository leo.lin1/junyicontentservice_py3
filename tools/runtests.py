#!/usr/bin/env python3

import optparse
import os
import sys
import logging

# For python2.5 install the unittest2 package
try:   # Work under either python2.5 or python2.7
    import unittest2 as unittest
except ImportError:
    import unittest

import logging
import appengine_tool_setup

USAGE = """%prog [options] [TEST_SPEC] ...

Run unit tests for App Engine apps.

This script will set up the Python path and environment. Test files
are expected to be named with a _test.py suffix.

TEST_SPEC   Specify tests by directory, file, or dotted name. Omit to
            use the current directory.

            Directory name: recursively search for files named *_test.py

            File name: find tests in the file.

            Dotted name: find tests specified by the name, e.g.,
            auth.tokens_test.TimestampTests.test_timestamp_creation,
            importer.autonow_test
"""


TEST_FILE_RE = '*_test.py'


def file_path_to_module(path):
    return path.replace('.py', '').replace(os.sep, '.')


def turn_off_warmup_config():
    origin_pattern = "- warmup\n"
    substitution =  "#- warmup\n"
    lines = []
    target_line_num = 0
    with open("app.yaml", "r") as f:
        lines = f.readlines()
        for line_num, line in enumerate(lines, 1):
            if  line == origin_pattern:
                target_line_num = line_num
                break
    if target_line_num in [len(lines), 0]:
        print(repr("[Update Warmup Config] Can not find '%s' in app.yaml => skip to comment it" % origin_pattern), "\n")
    else:
        with open("app.yaml", "w") as f:
            lines[target_line_num-1] = substitution
            f.writelines(lines)
        print(repr("[Update Warmup Config] Change '%s' to '%s' in app.yaml" % (origin_pattern, substitution)), "\n")


def main(test_specs, max_size, appengine_sdk_dir=None, no_warmup=False):
    if max_size == "large" and no_warmup:
        turn_off_warmup_config()
    appengine_tool_setup.fix_sys_path(appengine_sdk_dir)

    # This import needs to happen after fix_sys_path is run.
    from testutil import testsize
    testsize.set_max_size(max_size)

    from flask import Flask
    app = Flask(__name__)

    num_errors = 0

    with app.app_context():
        for test_spec in test_specs:
            loader = unittest.loader.TestLoader()
            if not os.path.exists(test_spec):
                suite = loader.loadTestsFromName(test_spec)
            elif test_spec.endswith('.py'):
                suite = loader.loadTestsFromName(file_path_to_module(test_spec))
            else:
                suite = loader.discover(test_spec,
                                        pattern=TEST_FILE_RE,
                                        top_level_dir=os.getcwd())

            runner = unittest.TextTestRunner(verbosity=2)
            result = runner.run(suite)
            if not result.wasSuccessful():
                num_errors += 1

    return num_errors


if __name__ == '__main__':
    parser = optparse.OptionParser(USAGE)
    # for example: --sdk=/Users/your_id/google-cloud-sdk/platform/google_appengine
    #              --sdk=/usr/local/google_appengine
    parser.add_option('--sdk', dest='sdk', metavar='SDK_PATH',
                      help='path to the App Engine SDK')
    parser.add_option('--max-size', dest='max_size', metavar='SIZE',
                      choices=['small', 'medium', 'large'],
                      default='medium',
                      help='run tests this size and smaller ("small", '
                           '"medium", "large")')
    parser.add_option('--no-warmup', dest='no_warmup', action='store_true',
                      help='disable warmup gae service (by dynamically modifying app.yaml)')

    options, args = parser.parse_args()

    if not args:
        test_specs = [os.getcwd()]
    else:
        test_specs = args

    logger = logging.getLogger()
    logger.level = logging.DEBUG
    stream_handler = logging.StreamHandler(sys.stdout)
    logger.addHandler(stream_handler)

    num_errors = main(test_specs, options.max_size, options.sdk, options.no_warmup)
    sys.exit(min(num_errors, 127))    # exitcode of 128+ means 'signal'
