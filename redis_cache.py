import os

import redis

import instance_cache
from base_cache import BaseCache


class RedisCache(BaseCache):
    @staticmethod
    @instance_cache.cache(available_seconds=0)
    def get_client():
        redis_host = os.environ.get('REDISHOST', 'localhost')
        redis_port = int(os.environ.get('REDISPORT', 6379))
        redis_client = redis.Redis(host=redis_host, port=redis_port)
        return redis_client

    @classmethod
    def _get_value(cls, key):
        return cls.get_client().get(key)

    @classmethod
    def _set_value(cls, key, value):
        cls.get_client().set(key, value)

    @classmethod
    def delete(cls, key, seconds=0, namespace=None):
        cls.get_client().delete(key)

    @classmethod
    def flush(cls):
        cls.get_client().flushdb()
