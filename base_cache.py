import functools
import pickle

# 100000 is max for memory cache
MAX_SIZE_OF_CACHE = 1000 * 1000
# max for memory cache - ChunkedResult overhead
MAX_SIZE_OF_CACHE_CHUNKS = MAX_SIZE_OF_CACHE - 100


class Debug():
    DBG = False

    @staticmethod
    def print(param):
        if Debug.DBG:
            print(param)

    @staticmethod
    def trim(a):
        s = str(a)
        if len(s) > 64:
            s = s[:64] + '...'
        return s

    @staticmethod
    def debug(target):
        @functools.wraps(target)
        def wrapper(*args, **kwargs):
            args_ = [Debug.trim(a) for a in args[1:]]
            Debug.print('    >>>> [%s] called with args=%s, kwargs=%s'
                        % (target.__qualname__, args_, kwargs))
            return target(*args, **kwargs)

        return wrapper


class BaseCache():
    @classmethod
    @Debug.debug
    def get(cls, key, namespace=None, for_cas=False):
        value = cls._get_value(key)
        if value is None:
            return None
        else:
            return pickle.loads(value)

    @classmethod
    @Debug.debug
    def get_multi(cls, keys, key_prefix='', namespace=None, for_cas=False):
        values = {}
        for key in keys:
            value = cls.get(key)
            if value is not None:
                values[key] = value
        return values

    @classmethod
    @Debug.debug
    def set(cls, key, value, time=0, min_compress_len=0, namespace=None):
        value = pickle.dumps(value)
        if len(value) > MAX_SIZE_OF_CACHE:
            raise ValueError("Values may not be more than %s: %s" % (MAX_SIZE_OF_CACHE, len(value)))
        cls._set_value(key, value)
        return True

    @classmethod
    @Debug.debug
    def set_multi(cls,
                  mapping,
                  time=0,
                  key_prefix='',
                  min_compress_len=0,
                  namespace=None):
        for key, value in mapping.items():
            cls.set(key, value)

    @classmethod
    def delete(cls, key, seconds=0, namespace=None):
        raise NotImplementedError

    @classmethod
    def delete_multi(cls, keys, namespace):
        for key in keys:
            cls.delete(key)

    @classmethod
    def flush(cls):
        raise NotImplementedError

    @classmethod
    def _get_value(cls, key):
        raise NotImplementedError

    @classmethod
    def _set_value(cls, key, value):
        raise NotImplementedError
