# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch

import instance_cache


def test_cache_inner_func():
    return 0


@instance_cache.cache_with_key_fxn(lambda a: 'test_cache_func_key_%s' % a, available_seconds=0)
def test_cache_func_key(a):
    return test_cache_inner_func()


@instance_cache.cache(available_seconds=1)
def test_cache_func_exp():
    return test_cache_inner_func()


@instance_cache.cache(available_seconds=0, category='a')
def test_cache_func_cat():
    return test_cache_inner_func()


@instance_cache.cache(available_seconds=0)
def test_cache_func():
    return test_cache_inner_func()


class TestInstanceCache(unittest.TestCase):

    def tearDown(self):
        super().tearDown()
        instance_cache.flush()

    @patch('instance_cache_test.test_cache_inner_func')
    def test_instance_cache_skip(self, func_patch):
        # skip cache, cache won't be set
        test_cache_func(skip_cache=True)
        self.assertEqual(func_patch.call_count, 1)

        # called without cache
        test_cache_func()
        self.assertEqual(func_patch.call_count, 2)

        # called with cache
        test_cache_func()
        self.assertEqual(func_patch.call_count, 2)

        # skip cache, existing cache is ignored
        test_cache_func(skip_cache=True)
        self.assertEqual(func_patch.call_count, 3)

    @patch('instance_cache_test.test_cache_inner_func')
    def test_instance_cache_cat(self, func_patch):
        # no cat, called without cache
        test_cache_func()
        self.assertEqual(func_patch.call_count, 1)
        # called with cache
        test_cache_func()
        self.assertEqual(func_patch.call_count, 1)

        # cat 'a', called without cache
        test_cache_func_cat()
        self.assertEqual(func_patch.call_count, 2)
        # called with cache
        test_cache_func_cat()
        self.assertEqual(func_patch.call_count, 2)

        # flush cat 'a'
        instance_cache.flush('a')

        # no cat, called with cache
        test_cache_func()
        self.assertEqual(func_patch.call_count, 2)

        # cat 'a', called without cache
        test_cache_func_cat()
        self.assertEqual(func_patch.call_count, 3)
        # called with cache
        test_cache_func_cat()
        self.assertEqual(func_patch.call_count, 3)

    @patch('instance_cache_test.test_cache_inner_func')
    def test_instance_cache_key(self, func_patch):
        # arg 1, called without cache
        test_cache_func_key(1)
        self.assertEqual(func_patch.call_count, 1)
        # called with cache
        test_cache_func_key(1)
        self.assertEqual(func_patch.call_count, 1)

        # arg 2, called without cache
        test_cache_func_key(2)
        self.assertEqual(func_patch.call_count, 2)
        # called with cache
        test_cache_func_key(2)
        self.assertEqual(func_patch.call_count, 2)

    @patch('instance_cache_test.test_cache_inner_func')
    def test_instance_cache_exp(self, func_patch):
        # called without cache
        test_cache_func_exp()
        self.assertEqual(func_patch.call_count, 1)
        # called with cache
        test_cache_func_exp()
        self.assertEqual(func_patch.call_count, 1)
        import time
        time.sleep(1)
        # called with expired cache
        test_cache_func_exp()
        self.assertEqual(func_patch.call_count, 2)
