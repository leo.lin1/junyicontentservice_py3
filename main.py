# -*- coding: utf-8 -*-
import logging
import os

import google.cloud.logging
from flask import Flask
from flask import request, jsonify, abort, make_response
from flask_cors import CORS
from google.cloud.logging.handlers import AppEngineHandler, setup_logging

import api_implement
import content.exception
import content.topic
from api import wsgi_compat, request_cache, api_util
from auth.decorators import moderator_required

app = Flask(__name__)
CORS_ORIGINS = ['http://localhost:8080',
        'https://junyiacademytest1.appspot.com',
        ]
CORS(app, resources={ r'/*': {'origins': CORS_ORIGINS}}, supports_credentials=True)
app.wsgi_app = wsgi_compat.WSGICompatHeaderMiddleware(app.wsgi_app)

FIX_CONTENT_MAX_CACHE_AGE = 60 * 60 * 24 * 3

def init_logging():
    # Instantiates a client
    client = google.cloud.logging.Client()
    handler = AppEngineHandler(client, name='stdout')
    logging.getLogger().setLevel(logging.DEBUG)
    setup_logging(handler)
    logging.info('logging initialized')


def get_request_data(data, key, default_val=None):
    if data and key in data:
        return data[key]
    return default_val


def trim_string(src, strip_crlf=True):
    if src:
        src = src.strip()
        if strip_crlf:
            src = src.replace('\r', '').replace('\n', '')
    return src


@app.before_request
def clean_request_cache():
    request_cache.flush()


XSRF_COOKIE_KEY = "fkey"
XSRF_HEADER_KEY = "X-KA-FKEY"


@app.before_request
def check_xsrf_value():
    if request.method == 'OPTIONS':
        return
    if request.path in ('/api/content/hello', '/api/content/redis_test'):
        return None

    cookie_value = request.cookies.get(XSRF_COOKIE_KEY)
    header_value = request.headers.get(XSRF_HEADER_KEY)
    if not cookie_value or not header_value:
        abort(400)
    if cookie_value != header_value:
        logging.warning('Mismatch xsrf value, possible from hacker')
        abort(403)


def check_required_args_exist(*args):
    """
    GET parameter parser
    """
    ret = []
    for required_param in args:
        param_name = required_param[0]
        cast_fn = required_param[1]
        if param_name not in request.args:
            raise ValueError("Require [%s] GET param" % param_name)
        ret.append(cast_fn(request.args[param_name]))
    return ret


@app.route('/api/content/hello')
def hello():
    """Return a friendly HTTP greeting."""
    logging.error('e: here')
    logging.warning('w: here')
    logging.info('i: here')
    logging.debug('d: here')
    return 'Hello World!'


@app.route('/api/content/redis_test')
def redis_test():
    """Test the Redis server."""
    import redis

    # create client
    redis_host = os.environ.get('REDISHOST', 'localhost')
    redis_port = int(os.environ.get('REDISPORT', 6379))
    redis_client = redis.Redis(host=redis_host, port=redis_port)

    # try to get from redis
    logging.debug(str(redis_client.get('greet')))

    # try to set to redis
    redis_client.set('greet', 'Hello Redis!')

    # try to get from redis again
    logging.debug(str(redis_client.get('greet')))

    return redis_client.get('greet')


@app.route('/api/content/topicpage')
def topicpage():
    """
    Retrieve the listing of subtopics and videos for this content.
        Used on the content page. e.g. /junyi-math/m1s/mesfl
    """
    try:
        checked_args = check_required_args_exist(('topic_id', str))
        topic_id = checked_args[0]
    except ValueError as e:
        return api_util.api_invalid_param_response(str(e))
    try:
        checked_args = check_required_args_exist(('v', str))
        version_id = checked_args[0]
    except ValueError:
        # `v` is not required
        version_id = None

    try:
        resp = api_implement.get_topic_page_json_impl(topic_id, version_id=version_id)
    except content.exception.ContentNotExistsError as e:
        return api_util.api_not_found_response(str(e))
    except PermissionError as e:
        return api_util.api_unauthorized_response(str(e))

    return jsonify(resp)


@app.route("/api/content/topictree/<version_id>/edu_sheet/topic/update", methods=["PUT"])
@moderator_required
def validate_or_update_topic_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本修改 Topic")
    request_json = request.get_json()
    topic_id = trim_string(get_request_data(request_json, 'topic_id', ''))
    topic_title = trim_string(get_request_data(request_json, 'topic_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return api_implement.validate_or_update_topic_by_edu_sheet(topic_id, topic_title, description, validation_only,
                                                               version_id)


@app.route("/api/content/topictree/<version_id>/edu_sheet/topic/create", methods=["POST"])
@moderator_required
def validate_or_create_topic_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本建立 Topic")
    if 'parent_id' not in request.get_json():
        return api_util.api_invalid_param_response('Missing required parameter: parent_id')
    request_json = request.get_json()
    topic_id = trim_string(get_request_data(request_json, 'topic_id', ''))
    topic_title = trim_string(get_request_data(request_json, 'topic_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    parent_id = trim_string(get_request_data(request_json, 'parent_id', ''))
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return jsonify(api_implement.validate_or_create_topic_by_edu_sheet(
        topic_id, topic_title, description, parent_id, validation_only, version_id))

@app.route("/api/content/topictree/<version_id>/edu_sheet/video/create", methods=["POST"])
@moderator_required
def validate_or_create_video_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本建立 Video")
    request_json = request.get_json()
    readable_id = trim_string(get_request_data(request_json, 'readable_id', ''))
    content_title = trim_string(get_request_data(request_json, 'content_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    parent_id = trim_string(get_request_data(request_json, 'parent_id', ''))
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return jsonify(api_implement.validate_or_create_video_by_edu_sheet(
        readable_id, content_title, description, parent_id, validation_only))


@app.route("/api/content/topictree/<version_id>/edu_sheet/video/update", methods=["PUT"])
@moderator_required
def validate_or_update_video_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本修改 Video")
    request_json = request.get_json()
    readable_id = trim_string(get_request_data(request_json, 'readable_id', ''))
    content_title = trim_string(get_request_data(request_json, 'content_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return jsonify(api_implement.validate_or_update_video_by_edu_sheet(
        readable_id, content_title, description, validation_only))

@app.route("/api/content/topictree/<version_id>/edu_sheet/exercise/create", methods=["POST"])
@moderator_required
def validate_or_create_exercise_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本建立 Exercise")
    request_json = request.get_json()
    exercise_name = trim_string(get_request_data(request_json, 'exercise_name', None))
    content_title = trim_string(get_request_data(request_json, 'content_title', None))
    description = trim_string(get_request_data(request_json, 'description', None))
    parent_id = trim_string(get_request_data(request_json, 'parent_id', None))
    validation_only = bool(int(request.args.get('validation_only', True)))
    return jsonify(api_implement.validate_or_create_exercise_by_edu_sheet(
            exercise_name, content_title, description, parent_id, validation_only))


@app.route("/api/content/topictree/<version_id>/edu_sheet/exercise/update", methods=["PUT"])
@moderator_required
def validate_or_update_exercise_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本修改 Exercise")
    request_json = request.get_json()
    exercise_name = trim_string(get_request_data(request_json, 'exercise_name', None))
    content_title = trim_string(get_request_data(request_json, 'content_title', None))
    description = trim_string(get_request_data(request_json, 'description', None))
    validation_only = bool(int(request.args.get('validation_only', True)))
    return jsonify(api_implement.validate_or_update_exercise_by_edu_sheet(
            exercise_name, content_title, description, validation_only))


@app.route('/api/content/section', methods=['POST'])
@moderator_required
def create_section():
    """
    在 edit version 的指定 topic 下建立小節

    :parameter
      - in: query
        name: parent_id
        required: true
        type: string
        description: 作為新增 section parent 的 topic id
      - in: query
        name: id
        required: false
        type: string
        description: 可由 client 指定或 server 發配，為 edit version, topic tree 中 unique 的 id
      - in: query
        name: title
        required: false
        type: string
        description: 顯示在 UI 的 title
      - in: query
        name: standalone_title
        required: false
        type: string
        description: standalone_title
    :return:
      - 200:
            client 未指定 id, 則回傳 id, title
      - 201 :
            Created. client 指定 id, 則無 response data
      - 400:
            query 欄位不合法
      - 404:
            topic with parent_id not found
      - 409:
            Conflict. client 指定的 id 已存在
      - 415:
            only support 'application/json' request
      - 500:
            極少發生，隨機產生的 topic name 衝突
    """
    if not request.get_json():
        return api_util.api_unsupported_media_type()
    if 'parent_id' not in request.get_json():
        return api_util.api_invalid_param_response('Missing required parameter: parent_id')
    try:
        section_info = api_implement.create_section(request.get_json()['parent_id'],
                                                    get_request_data(request.get_json(), 'id', None),
                                                    get_request_data(request.get_json(), 'title', None),
                                                    get_request_data(request.get_json(), 'standalone_title', None)
                                                    )
    except content.exception.ContentNotExistsError as e:
        return api_util.api_not_found_response(str(e))
    except (content.exception.InvalidFormat, content.exception.InvalidParent) as e:
        return api_util.api_invalid_param_response(str(e))
    except content.exception.TopicExistsError as e:
        return api_util.api_conflict(str(e))
    except content.exception.TopicKeyExistsError as e:
        return api_util.api_error_response(str(e))

    if 'id' in request.get_json():
        return '', 201
    else:
        return jsonify(section_info), 200

@app.route('/api/content/topicversion/<version_id>/section/<section_id>', methods=['GET'])
@moderator_required
def read_section(version_id, section_id):
    """
    讀取小節內容

    :parameter
      - in: path
        name: version_id
        required: true
        type: string, e.g. 'edit', 'default' or '1000'
        description: topic version id
    :parameter
      - in: path
        name: section_id
        required: true
        type: string
        description: section id
    :return:
      - 200:
            success. return kind, id, title, standalone_title and children
      - 400:
            version_id 不合法
      - 404:
            找不到此 section
    """
    try:
        is_published, payload = api_implement.read_section(version_id, section_id)
    except content.exception.InvalidFormat as e:
        # invalid version_id
        return api_util.api_invalid_param_response(str(e))
    except content.exception.ContentNotExistsError as e:
        # section not found
        return api_util.api_not_found_response(str(e))
    response = make_response(jsonify(payload))
    is_cacheable = is_published and version_id != 'default'
    if is_cacheable:
        response.cache_control.max_age = FIX_CONTENT_MAX_CACHE_AGE
    return response

@app.route('/api/content/section/<section_id>', methods=['PUT'])
@moderator_required
def update_section(section_id):
    """
    修改小節內容

    :parameter
      - in: path
        name: id
        required: true
        type: string
        description: origin id
      - in: query
        name: id
        required: false
        type: string
        description: new id, 沒傳則不會修改
      - in: query
        name: title
        required: false
        type: string
        description: new title, 沒傳則不會修改
      - in: query
        name: standalone_title
        required: false
        type: string
        description: new standalone_title, 沒傳則不會修改
    :return:
      - 204:
            success with no content
      - 400:
            query 欄位不合法
      - 404:
            section not found by given id
      - 409:
            new id 與已存在於 edit version 的 topic / section id 衝突
      - 415:
            only support 'application/json' request
    """
    if request.get_json() is None:
        return api_util.api_unsupported_media_type()
    try:
        api_implement.update_section(section_id,
                                     get_request_data(request.get_json(), 'id', None),
                                     get_request_data(request.get_json(), 'title', None),
                                     get_request_data(request.get_json(), 'standalone_title', None)
                                     )
    except content.exception.ContentNotExistsError as e:
        return api_util.api_not_found_response(str(e))
    except content.exception.InvalidFormat as e:
        return api_util.api_invalid_param_response(str(e))
    except content.exception.TopicExistsError as e:
        return api_util.api_conflict(str(e))

    return api_util.api_success_no_content_response()


# Migrates from @route("/api/v1/videos/<topic_id>/<video_id>/play")
# in junyiacademy, removed deprecated 'require_topic' parameter
@app.route('/api/content/videos/play', methods=['GET'])
def video_play_data():
    """
      取得看影片頁面，切換影片所需資訊．
      video_id 非 unique key, 需搭配 topic_id 才能找到唯一的影片
      parameter:
        - in: GET parameter
          name: topic_id
          type: string
          description: 欲查找影片所在的 topic
        - in: GET parameter
          name: video_id
          type: string
          description: 欲查找影片之 video_id
      """
    if 'topic_id' not in request.args:
        return \
            api_util.api_invalid_param_response('Require [topic_id] GET param.')
    if 'video_id' not in request.args:
        return \
            api_util.api_invalid_param_response('Require [video_id] GET param.')

    topic_id = request.args['topic_id']
    video_id = request.args['video_id']
    try:
        resp = api_implement.video_play_data_impl(topic_id, video_id)
    except content.exception.ContentNotExistsError as e:
        return api_util.api_invalid_param_response(str(e))
    return jsonify(resp)


# ths same with @route("/api/v1/topicversion/<version_id>/topictreedata") in junyiacademy
# ths same with @route("/api/v1/topictreedata") in junyiacademy
@app.route("/api/content/topictreedata", methods=["GET"])
def get_topic_tree_data():
    """
    Retrieve the listing of subtopics and videos for this topic. Used on the topic page.
    ---
    id:
      - topic
    tags:
      - Content
    parameters:
      - in: query
        name: version_id
        required: false
        type: string
        description: 可以接受以下三種格式：default/edit/版本號碼
    responses:
        200:
            description: 已成功抓取 topic tree 資料
            schema:
                $ref: '#/definitions/Topic'
        400:
            description: 找不到 version_id
    """
    if 'casing' not in request.args or request.args['casing'] != 'camel':
        logging.error('Error: Not support other casing!')
        return api_util.api_invalid_param_response('Error: Not support other casing!')

    version_id = request.args["topicversion"] if "topicversion" in request.args else None
    try:
        resp = content.topic.get_topic_tree_data(version_id=version_id)
    except (content.exception.ContentNotExistsError, content.exception.InvalidFormat) as e:
        return api_util.api_invalid_param_response(str(e))
    return jsonify(resp)

@app.route('/api/content/topic', methods=["GET"])
def get_topics():
    """
    Retrieve the public topic list of a specific version.
    For now, only support topic lists that directly contain exercise, e.g.:
        root/
            topicA/
                topicA-1/
                    exerciseA
                topicA-2/
                    videoA
    Only return topicA-1 in this case.

    parameter:
      - in: GET parameter
        name: version_id
        required: true
        type: int
        description: topic version id. e.g. 901
      - in: GET parameter
        name: topic_type
        required: true
        type: string, "exercise_topic"
        description: Only support exercise_topic for now.
    """
    try:
        checked_args = check_required_args_exist(("version_id", int), ("topic_type", str))
        version_id = checked_args[0]
        topic_type = checked_args[1]
    except ValueError as e:
        return api_util.api_invalid_param_response(
            'Require [version_id] and [topic_type] GET parameters')

    if topic_type != "exercise_topic":
        return api_util.api_invalid_param_response("Invalid topic_type")

    return jsonify(api_implement.get_topic_list_impl(version_id))


@app.route('/api/content/instance_cache/flush')
def flush():
    if 'category' not in request.args:
        return 'arg error'
    return api_implement.flush_instance_cache(request.args['category'])


@app.route('/_ah/warmup')
def warm_up():
    # init_logging()
    return '', 200, {}


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    logging.basicConfig(level=logging.DEBUG)
    app.run(host='0.0.0.0', port=8010, debug=True)

if not os.getenv('DATASTORE_EMULATOR_HOST'):
    init_logging()
