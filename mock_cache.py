from base_cache import BaseCache


class MockCache(BaseCache):
    in_mem_cache = {}

    @classmethod
    def _get_value(cls, key):
        return cls.in_mem_cache.get(key, None)

    @classmethod
    def _set_value(cls, key, value):
        cls.in_mem_cache[key] = value

    @classmethod
    def delete(cls, key, seconds=0, namespace=None):
        del cls.in_mem_cache[key]

    @classmethod
    def flush(cls):
        cls.in_mem_cache.clear()
