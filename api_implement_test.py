# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch

import api_implement


class TestGetTopicPageJson(unittest.TestCase):
    # TODO: topic_id = None
    def test_no_default_version(self):
        pass

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_update_topic_by_edu_sheet_ok(self, put_patch):
        topic_id = 'math'
        # 測試驗證
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', True)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertFalse(put_patch.called)
        # 測試修改
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertTrue(put_patch.called)

    def test_validate_or_update_topic_by_edu_sheet_fail(self):
        # 測試驗證時發生錯誤 - id 不存在
        topic_id = 'x'
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '這個代號的資料夾不存在 x')

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_create_topic_by_edu_sheet_ok(self, put_patch):
        topic_id = 'new_topic'
        # 測試驗證
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', True)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertFalse(put_patch.called)
        # 測試建立
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 0)
        self.assertTrue(put_patch.called)

    def test_validate_or_create_topic_by_edu_sheet_fail(self):
        # 測試驗證時發生錯誤 - id 空白
        topic_id = ''
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        # 測試建立時發生錯誤 - 不存在 parent
        topic_id = 'new_topic'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'x', False)
        self.assertEqual(len(ret['errors']), 1)
        # 測試建立時發生錯誤 - 已存在 id
        topic_id = 'math'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', False)
        self.assertEqual(len(ret['errors']), 1)

    def test_validate_or_create_topic_by_edu_sheet_fail_version(self):
        # 測試在 default version 變動
        topic_id = 'new_topic'
        ret = api_implement.validate_or_create_topic_by_edu_sheet(topic_id, 'title', 'description', 'math', True, 'default')
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '目前禁止於 edit 以外的版本建立 Topic')
        ret = api_implement.validate_or_update_topic_by_edu_sheet(topic_id, 'title', 'description', True, 'default')
        self.assertEqual(ret['topic_id'], topic_id)
        self.assertEqual(len(ret['errors']), 1)
        self.assertEqual(ret['errors'][0], '目前禁止於 edit 以外的版本修改 Topic')


class TestEduSheetVideoAPI(unittest.TestCase):
    @patch('pickle.dumps')
    @patch('google.cloud.datastore.Client.put')
    @patch('content.internal.youtube_sync.youtube_get_video_data_dict')
    def test_validate_or_create_video_by_edu_sheet_ok(self, youtube_get_video_data_dict_patch, put_patch, pickle_dumps_patch):
        ret = api_implement.validate_or_create_video_by_edu_sheet(
            'readable_id', 'title', 'description', 'math', False)
        self.assertEqual(ret['readable_id'], 'readable_id')
        self.assertListEqual(ret['errors'], [])
        put_patch.assert_called()

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_create_video_by_edu_sheet_invalid_version(self, put_patch):
        ret = api_implement.validate_or_create_video_by_edu_sheet(
            'readable_id', 'title', 'description', 'math', False, 'non_edit')
        self.assertListEqual(ret['errors'], ['不允許編輯此版本 [non_edit]'])
        put_patch.assert_not_called()

    @patch('google.cloud.datastore.Client.put')
    def test_validate_or_update_video_by_edu_sheet_invalid_version(self, put_patch):
        ret = api_implement.validate_or_update_video_by_edu_sheet(
            'readable_id', 'title', 'description', False, 'non_edit')
        self.assertListEqual(ret['errors'], ['不允許編輯此版本 [non_edit]'])
        put_patch.assert_not_called()
